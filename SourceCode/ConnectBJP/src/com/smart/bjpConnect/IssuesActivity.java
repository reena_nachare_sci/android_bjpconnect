package com.smart.bjpConnect;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings.Secure;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.origamilabs.library.views.StaggeredGridView;

import com.smart.bjpConnect.flipboard.loader.ImageLoader;
import com.smart.bjpConnect.flipboard.view.ScaleImageView;


public class IssuesActivity extends ActinBarActivity{

	private ProgressDialog dlgProgress;
	private ArrayList<String> issueNames;
	private ArrayList<String> image_urls;
	private ArrayList<String> issue_ids;

	private String urls[] = { 
			"http://farm9.staticflickr.com/8335/8144074340_38a4c622ab.jpg",
	};

	private String android_id;
	private ConnectionDetector cd;
	StaggeredGridView gridView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_polls);
		setupActionBar();

		android_id = Secure.getString(IssuesActivity.this.getContentResolver(),
				Secure.ANDROID_ID); 
		Log.e("android device id--"+android_id,"KKK");
		cd= new ConnectionDetector(IssuesActivity.this);
		dlgProgress= new ProgressDialog(IssuesActivity.this);
		issueNames= new ArrayList<String>();
		image_urls= new ArrayList<String>();
		issue_ids= new ArrayList<String>();

		TextView title = (TextView) actionBarLayout.findViewById(R.id.txt_title);
		title.setText("What Matters in Delhi");
		gridView = (StaggeredGridView) this.findViewById(R.id.staggeredGridView1);

		Log.e("Mainj"+issueNames.size(),"KKK");
		int margin = getResources().getDimensionPixelSize(R.dimen.margin);

		gridView.setItemMargin(margin); // set the GridView margin

		gridView.setPadding(margin, 0, margin, 0); // have the margin on the sides as well 

		if(cd.isConnectingToInternet()){
			GetIssues qs= new GetIssues();
			qs.execute();
		}
		else{
			Toast.makeText(IssuesActivity.this, "No Internet",Toast.LENGTH_SHORT).show();
			finish();
		}

	}

	/** AsyncTask to download and load an image in ListView */
	private class GetIssues extends AsyncTask<String, Void, String> {
		boolean status=false;
		@Override
		protected void onPreExecute() {
			Log.e("Pre","KK");
			dlgProgress.setMessage("Please wait");
			dlgProgress.setCancelable(false);
			dlgProgress.show();
		}

		@Override
		protected void onPostExecute(String result) {
			// Getting adapter of the listview
			Log.e("post","KK");
			dlgProgress.dismiss();
			if(status){
				Log.e("size;"+issueNames.size(),"KKK");
				if(issueNames.size()>0)
				{
					if(image_urls.size()>0){
						for(int i=0;i<image_urls.size();i++)
						{
							Log.e("URL is--"+urls[i],"KK");
							urls= new String[image_urls.size()];
							urls[i]=image_urls.get(i);
							//Log.e("URL is--"+urls[i],"KK");
						}
						StaggeredAdapter adapter = new StaggeredAdapter(IssuesActivity.this, R.id.imageView1, urls);

						gridView.setAdapter(adapter);
						adapter.notifyDataSetChanged();
					}

				}
			}
		}

		@Override
		protected String doInBackground(String... params) {
			Log.e("background","KK");
			try {
				status=getIssues();
				return null;

			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

	}

	public boolean getIssues() {
		// TODO Auto-generated method stub
		//Bitmap decodedByte = null;
		boolean isList=false;
		HttpClient httpclient = new DefaultHttpClient();
		HttpPost httppost = new HttpPost(
				"http://www.smartcloudinfo.com/BJPConnect/index.php/issues/getIssues_REST");

		try {
			// Add your data

			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);

			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

			// Execute HTTP Post Request
			HttpResponse response = httpclient.execute(httppost);

			HttpEntity entity = response.getEntity();

			InputStream inStream = entity.getContent();
			String jsonresponse = Utils.convertStreamToString(inStream);
			Log.e("result==voting qs", jsonresponse);
			JSONObject obj1 = null;

			try {
				obj1 = new JSONObject(jsonresponse);

			} catch (JSONException e) {
				isList=false;
				e.printStackTrace();
			}
			try {
				String status = obj1.getString("status");
				if (status.equals("false")) {
					isList=false;
				} else {
					isList=true;
					issue_ids.clear();
					issueNames.clear();
					image_urls.clear();
					//					option1.clear();
					//					option2.clear();
					//					option3.clear();
					JSONArray polls= obj1.getJSONArray("issues");
					for(int i=0;i<polls.length();i++)
					{
						JSONObject item= polls.getJSONObject(i);
						issueNames.add(item.getString("name"));
						image_urls.add(item.getString("icon"));
						issue_ids.add(item.getString("id"));
					}
				}

			} catch (JSONException e) {
				isList=false;
				e.printStackTrace();
			}
		} catch (ClientProtocolException ex) {
			isList=false;
			ex.printStackTrace();

		} catch (IOException ez) {
			isList=false;
			ez.printStackTrace();

		}
		return isList;
	}

	public class StaggeredAdapter extends ArrayAdapter<String> {

		private com.smart.bjpConnect.flipboard.loader.ImageLoader mLoader;
		String[] imgs;
		public StaggeredAdapter(Context context, int textViewResourceId,
				String[] objects) {
			super(context, textViewResourceId, objects);
			mLoader = new ImageLoader(context);
			imgs=objects;
		}

		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {

			ViewHolder holder;
			Log.e("Got image view url--"+position,"Name"+issueNames.get(position));
			if (convertView == null) {
				LayoutInflater layoutInflator = LayoutInflater.from(getContext());
				convertView = layoutInflator.inflate(R.layout.row_staggered_demo,
						null);
				holder = new ViewHolder();
				holder.imageView = (ScaleImageView) convertView .findViewById(R.id.imageView1);
				holder.txt_flip = (TextView) convertView .findViewById(R.id.txt_flip);
				holder.txt_flip.setText(issueNames.get(position));
				convertView.setTag(holder);
			}

			holder = (ViewHolder) convertView.getTag();
			//Log.e("Url to load--"+urls[position],"KKK"+getItem(position));
			mLoader.DisplayImage(image_urls.get(position), holder.imageView);
			mLoader.DisplayText(issueNames.get(position), holder.txt_flip, image_urls.get(position));
			convertView.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent toDetails= new Intent(IssuesActivity.this, IssueDetailsActivity.class);
					toDetails.putExtra("title", issueNames.get(position));
					toDetails.putExtra("id", issue_ids.get(position));
					startActivity(toDetails);

				}
			});

			return convertView;
		}

		class ViewHolder {
			ScaleImageView imageView;
			TextView txt_flip;
		}
	}
	/*	public class StaggeredAdapter extends ArrayAdapter<String> {

		private com.smart.bjpConnect.flipboard.loader.ImageLoader mLoader;

		public StaggeredAdapter(Context context, int textViewResourceId,
				String[] objects) {
			super(context, textViewResourceId, objects);
			mLoader = new ImageLoader(context);

		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {

			ViewHolder holder;

			if (convertView == null) {
				LayoutInflater layoutInflator = LayoutInflater.from(getContext());
				convertView = layoutInflator.inflate(R.layout.row_staggered_demo,
						null);
				holder = new ViewHolder();
				holder.imageView = (ScaleImageView) convertView .findViewById(R.id.imageView1);
				holder.txt_flip = (TextView) convertView .findViewById(R.id.txt_flip);
				holder.txt_flip.setText(issueNames.get(position));
				convertView.setTag(holder);
			}

			holder = (ViewHolder) convertView.getTag();
			Log.e("Url--"+position+getItem(position),"KKK"+issueNames.get(position));
			mLoader.DisplayImage(getItem(position), holder.imageView);

			return convertView;
		}
		 class ViewHolder {
			ScaleImageView imageView;
			TextView txt_flip;
		}

	}*/

	/*@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}*/


}
