package com.smart.bjpConnect;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings.Secure;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.origamilabs.library.views.StaggeredGridView;
import com.smart.bjpConnect.flipboard.loader.ImageLoader;
import com.smart.bjpConnect.flipboard.view.ScaleImageView;


public class KnowTheFactsActivity extends ActinBarActivity{

	private ProgressDialog dlgProgress;
	private ArrayList<String> questions;
	private ArrayList<String> image_urls;
	private ArrayList<String> question_ids;
	ArrayList<String> option1;
	ArrayList<String> option2;
	ArrayList<String> option3;
	ArrayList<String> option4;
	ArrayList<String> option5;
	static String vote_question="";
	static String txt_option_1="";
	static String txt_option_2="";
	static String txt_option_3="";
	static String txt_option_4="";
	static String txt_option_5="";
	static String question_id="";
	String display="";

	private String urls[] = { 
			"http://farm9.staticflickr.com/8335/8144074340_38a4c622ab.jpg",
	};

	private String android_id;
	private ConnectionDetector cd;
	StaggeredGridView gridView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_polls);
		setupActionBar();

		android_id = Secure.getString(KnowTheFactsActivity.this.getContentResolver(),
				Secure.ANDROID_ID); 
		Log.e("android device id--"+android_id,"KKK");
		cd= new ConnectionDetector(KnowTheFactsActivity.this);
		dlgProgress= new ProgressDialog(KnowTheFactsActivity.this);
		questions= new ArrayList<String>();
		image_urls= new ArrayList<String>();
		question_ids= new ArrayList<String>();
		option1= new ArrayList<String>();
		option2= new ArrayList<String>();
		option3= new ArrayList<String>();
		option4= new ArrayList<String>();
		option5= new ArrayList<String>();

		TextView title = (TextView) actionBarLayout.findViewById(R.id.txt_title);
		title.setText("Do you know...?");
		gridView = (StaggeredGridView) this.findViewById(R.id.staggeredGridView1);

		Log.e("Mainj"+questions.size(),"KKK");
		int margin = getResources().getDimensionPixelSize(R.dimen.margin);

		gridView.setItemMargin(margin); // set the GridView margin

		gridView.setPadding(margin, 0, margin, 0); // have the margin on the sides as well 

		if(cd.isConnectingToInternet()){
			GetFactsQs qs= new GetFactsQs();
			qs.execute();
		}
		else{
			Toast.makeText(KnowTheFactsActivity.this, "No Internet",Toast.LENGTH_SHORT).show();
			finish();
		}
	}

	/** AsyncTask to download and load an image in ListView */
	private class GetFactsQs extends AsyncTask<String, Void, String> {
		boolean status=false;
		@Override
		protected void onPreExecute() {
			Log.e("Pre","KK");
			dlgProgress.setMessage("Please wait");
			dlgProgress.setCancelable(false);
			dlgProgress.show();
		}

		@Override
		protected void onPostExecute(String result) {
			// Getting adapter of the listview
			Log.e("post","KK");
			dlgProgress.dismiss();
			if(status){
				Log.e("size;"+questions.size(),"KKK");
				if(questions.size()>0)
				{
					if(image_urls.size()>0){
						for(int i=0;i<image_urls.size();i++)
						{
							Log.e("URL is--"+urls[i],"KK");
							urls= new String[image_urls.size()];
							urls[i]=image_urls.get(i);
							//Log.e("URL is--"+urls[i],"KK");
						}
						StaggeredAdapter adapter = new StaggeredAdapter(KnowTheFactsActivity.this, R.id.imageView1, urls);

						gridView.setAdapter(adapter);
						adapter.notifyDataSetChanged();
					}

				}
			}
		}

		@Override
		protected String doInBackground(String... params) {
			Log.e("background","KK");
			try {
				status=getFactsQs();
				return null;

			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

	}

	public boolean getFactsQs() {
		// TODO Auto-generated method stub
		//Bitmap decodedByte = null;
		boolean isList=false;
		HttpClient httpclient = new DefaultHttpClient();
		HttpPost httppost = new HttpPost(
				"http://www.smartcloudinfo.com/BJPConnect/index.php/knowfacts/getKnowFacts_REST");

		try {
			// Add your data

			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
			nameValuePairs.add(new BasicNameValuePair("device_id", android_id));

			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

			// Execute HTTP Post Request
			HttpResponse response = httpclient.execute(httppost);

			HttpEntity entity = response.getEntity();

			InputStream inStream = entity.getContent();
			String jsonresponse = Utils.convertStreamToString(inStream);
			Log.e("result==voting qs", jsonresponse);
			JSONObject obj1 = null;

			try {
				obj1 = new JSONObject(jsonresponse);

			} catch (JSONException e) {
				isList=false;
				e.printStackTrace();
			}
			try {
				String status = obj1.getString("status");
				if (status.equals("false")) {
					isList=false;
				} else {
					isList=true;
					question_ids.clear();
					questions.clear();
					image_urls.clear();
					option1.clear();
					option2.clear();
					option3.clear();
					JSONArray polls= obj1.getJSONArray("knowfacts");
					for(int i=0;i<polls.length();i++)
					{
						JSONObject item= polls.getJSONObject(i);
						questions.add(item.getString("question"));
						image_urls.add(item.getString("image"));
						option1.add(item.getString("option_1"));
						option2.add(item.getString("option_2"));
						option3.add(item.getString("option_3"));
						option4.add(item.getString("option_4"));
						option5.add(item.getString("option_5"));
						question_ids.add(item.getString("id"));
					}
				}

			} catch (JSONException e) {
				isList=false;
				e.printStackTrace();
			}
		} catch (ClientProtocolException ex) {
			ex.printStackTrace();
			isList=false;
		} catch (IOException ez) {
			ez.printStackTrace();
			isList=false;
		}
		return isList;
	}

	public class StaggeredAdapter extends ArrayAdapter<String> {

		private com.smart.bjpConnect.flipboard.loader.ImageLoader mLoader;
		String[] imgs;
		public StaggeredAdapter(Context context, int textViewResourceId,
				String[] objects) {
			super(context, textViewResourceId, objects);
			mLoader = new ImageLoader(context);
			imgs=objects;
		}

		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {

			ViewHolder holder;
			Log.e("Got image view url--"+position,"Name"+questions.get(position));
			if (convertView == null) {
				LayoutInflater layoutInflator = LayoutInflater.from(getContext());
				convertView = layoutInflator.inflate(R.layout.row_staggered_demo,
						null);
				holder = new ViewHolder();
				holder.imageView = (ScaleImageView) convertView .findViewById(R.id.imageView1);
				holder.txt_flip = (TextView) convertView .findViewById(R.id.txt_flip);
				holder.txt_flip.setText(questions.get(position));
				convertView.setTag(holder);
			}

			holder = (ViewHolder) convertView.getTag();
			//Log.e("Url to load--"+urls[position],"KKK"+getItem(position));
			mLoader.DisplayImage(image_urls.get(position), holder.imageView);
			mLoader.DisplayText(questions.get(position), holder.txt_flip, image_urls.get(position));
			convertView.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Log.e("option--"+option1.get(position),"QQQ-"+questions.get(position));
					vote_question= questions.get(position);
					txt_option_1= option1.get(position);
					txt_option_2= option2.get(position);
					txt_option_3= option3.get(position);
					txt_option_4= option4.get(position);
					txt_option_5= option5.get(position);

					question_id= question_ids.get(position);

					Intent toDetails= new Intent(KnowTheFactsActivity.this, FactsDetailActivity.class);

					toDetails.putExtra("title", vote_question);
					toDetails.putExtra("option1", txt_option_1);
					toDetails.putExtra("option2", txt_option_2);
					toDetails.putExtra("option3", txt_option_3);
					toDetails.putExtra("option4", txt_option_4);
					toDetails.putExtra("option5", txt_option_5);
					toDetails.putExtra("id", question_id);
					startActivity(toDetails);
				}
			});

			return convertView;
		}

		class ViewHolder {
			ScaleImageView imageView;
			TextView txt_flip;
		}
	}
	/*	public class StaggeredAdapter extends ArrayAdapter<String> {

		private com.smart.bjpConnect.flipboard.loader.ImageLoader mLoader;

		public StaggeredAdapter(Context context, int textViewResourceId,
				String[] objects) {
			super(context, textViewResourceId, objects);
			mLoader = new ImageLoader(context);

		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {

			ViewHolder holder;

			if (convertView == null) {
				LayoutInflater layoutInflator = LayoutInflater.from(getContext());
				convertView = layoutInflator.inflate(R.layout.row_staggered_demo,
						null);
				holder = new ViewHolder();
				holder.imageView = (ScaleImageView) convertView .findViewById(R.id.imageView1);
				holder.txt_flip = (TextView) convertView .findViewById(R.id.txt_flip);
				holder.txt_flip.setText(questions.get(position));
				convertView.setTag(holder);
			}

			holder = (ViewHolder) convertView.getTag();
			Log.e("Url--"+position+getItem(position),"KKK"+questions.get(position));
			mLoader.DisplayImage(getItem(position), holder.imageView);

			return convertView;
		}
		 class ViewHolder {
			ScaleImageView imageView;
			TextView txt_flip;
		}
	}*/

	/*@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}*/
}