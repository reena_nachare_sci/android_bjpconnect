package com.smart.bjpConnect;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.smart.bjpConnect.quickaction.QuickAction;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;
import android.content.Context;
import android.content.Intent;

public class FAQActivity extends ActinBarActivity {

	private ListView listView ;
	private ArrayList<String> fAQList;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_faq);
	
		setupActionBar();
		TextView title = (TextView) actionBarLayout.findViewById(R.id.txt_title);
		title.setText("FAQ's");

		listView = (ListView) findViewById(R.id.list_faq);
		fAQList = new ArrayList<String>();
		fAQList.add("What are the documents I need to register as a voter?");
		fAQList.add("I don't have an address proof document on my name. Can I still register to vote?");
		fAQList.add("I submitted my registration form but was denied the receipt of application. Where should I complain?");
		fAQList.add("There is an upcoming election where I want to vote? What is the last date for registration?");

		StableArrayAdapter adapter1 = new StableArrayAdapter(FAQActivity.this,
				android.R.layout.simple_list_item_1, fAQList);

		// Assign adapter to ListView
		listView.setAdapter(adapter1); 

		// ListView Item Click Listener
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position,
					long id) {
				// ListView Clicked item index
				//int itemPosition     = position;

				// ListView Clicked item value
				String  itemValue    = (String) listView.getItemAtPosition(position);
				
				Intent toLoadImage = new Intent(FAQActivity.this,
						AndroidLoadActivity.class);
				toLoadImage.putExtra("display", itemValue);
				startActivity(toLoadImage);

				// Show Alert 
				/*Toast.makeText(getApplicationContext(),
						"Position :"+itemPosition+"  ListItem : " +itemValue , Toast.LENGTH_LONG)
						.show();*/
			}
		}); 
	}
	
	private class StableArrayAdapter extends ArrayAdapter<String> {

		HashMap<String, Integer> mIdMap = new HashMap<String, Integer>();

		public StableArrayAdapter(Context context, int textViewResourceId,
				List<String> objects) {
			super(context, textViewResourceId, objects);
			for (int i = 0; i < objects.size(); ++i) {
				mIdMap.put(objects.get(i), i);
			}
		}

		@Override
		public long getItemId(int position) {
			String item = getItem(position);
			return mIdMap.get(item);
		}

		@Override
		public boolean hasStableIds() {
			return true;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View view = convertView;
			if (view == null) {
				Log.e("View is null","KKK");
				LayoutInflater vi = (LayoutInflater)FAQActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				view = vi.inflate(R.layout.questions_list_row, null);
			}
			Log.e("getView--"+position,"KK");
			TextView txt_question= (TextView)view.findViewById(R.id.txt_question_name);
	
			txt_question.setText(fAQList.get(position));
			return view;
		}
	}
}