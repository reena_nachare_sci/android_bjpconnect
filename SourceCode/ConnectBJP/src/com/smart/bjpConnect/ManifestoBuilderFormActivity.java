package com.smart.bjpConnect;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.view.View.OnClickListener;

public class ManifestoBuilderFormActivity extends ActinBarActivity {

	private EditText mNameEditText;
	private EditText mEmailEditText;
	private EditText mContactNoEditText;
	private EditText mMessageEditText;
	
	private Button mSendMessageButton;
	
	private Spinner mIssueSpinner;
	
	ConnectionDetector cd;
	
	private ProgressDialog dlgProgress;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_manifesto_builder_form);

		setupActionBar();
		TextView title = (TextView) actionBarLayout.findViewById(R.id.txt_title);
		title.setText("PARTICIPATE NOW");
		
		cd= new ConnectionDetector(ManifestoBuilderFormActivity.this);
		
		mNameEditText = (EditText) findViewById(R.id.nameManifestoText);
		mEmailEditText = (EditText) findViewById(R.id.emailManifestiText);
		mContactNoEditText = (EditText) findViewById(R.id.telephoneMenifestoText);
		mMessageEditText = (EditText) findViewById(R.id.messageManifestoText);
		
		mSendMessageButton = (Button) findViewById(R.id.buttonManifestoBuilder);
		
		mIssueSpinner = (Spinner) findViewById(R.id.issue_Manifesto_spinner);
		
		mSendMessageButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(mNameEditText.getText().toString().trim().length() == 0){
					Toast toast = Toast.makeText(getApplicationContext(),
							"Please fill name field properly",
							Toast.LENGTH_SHORT);

					toast.setGravity(Gravity.CENTER, 0, 0);
					toast.show();
				}else if(mEmailEditText.getText().toString().trim().length() == 0){
					Toast toast = Toast.makeText(getApplicationContext(),
							"Please fill email field properly",
							Toast.LENGTH_SHORT);

					toast.setGravity(Gravity.CENTER, 0, 0);
					toast.show();
				}else if(mContactNoEditText.getText().toString().trim().length() == 0){
					Toast toast = Toast.makeText(getApplicationContext(),
							"Please fill contact no properly",
							Toast.LENGTH_SHORT);

					toast.setGravity(Gravity.CENTER, 0, 0);
					toast.show();
				}else if(mMessageEditText.getText().toString().trim().length() == 0){
					Toast toast = Toast.makeText(getApplicationContext(),
							"Please fill message field properly",
							Toast.LENGTH_SHORT);

					toast.setGravity(Gravity.CENTER, 0, 0);
					toast.show();
				}else if(mIssueSpinner.getSelectedItem().toString().trim().equals("Select Issue")){
					Toast toast = Toast.makeText(getApplicationContext(),
							"Please select issue",
							Toast.LENGTH_SHORT);

					toast.setGravity(Gravity.CENTER, 0, 0);
					toast.show();
				}else{
					if(cd.isConnectingToInternet()){
						ManifestoBuilder DT = new ManifestoBuilder();
						DT.execute();
					}else{
						Toast.makeText(ManifestoBuilderFormActivity.this, "NO Internet", Toast.LENGTH_SHORT).show();	
						//finish();
					}
				}
			}
		});
	}
	
	public class ManifestoBuilder extends AsyncTask<String, Integer, String>{
		boolean status=false;
		protected void onPreExecute() {
			Log.e("Pre","KK");
			dlgProgress = new ProgressDialog(ManifestoBuilderFormActivity.this);
			dlgProgress.setMessage("Please wait");
			dlgProgress.setCancelable(false);
			dlgProgress.show();
		}

		protected void onPostExecute(String result) {
			// Getting adapter of the listview
			Log.e("post","KK");
			dlgProgress.dismiss();
			
			if(status){
				Intent toVision= new Intent(ManifestoBuilderFormActivity.this, MainActivity1.class);
				toVision.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(toVision);
				
				Toast.makeText(ManifestoBuilderFormActivity.this,"Thanks For Participating",Toast.LENGTH_SHORT).show();
			}
		}
		@Override
		protected String doInBackground(String... aurl) {
			Log.e("background","KK");
			try {
				status = GetOptions();
				return null;

			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}
	}
	
	public boolean GetOptions() {
		//Bitmap decodedByte = null;
		boolean isList=false;
		HttpClient httpclient = new DefaultHttpClient();
		HttpPost httppost = new HttpPost(
				"http://www.smartcloudinfo.com/BJPConnect/index.php/logs/participateBJPManifesto");
		try {
			// Add your data

			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
			nameValuePairs.add(new BasicNameValuePair("name", mNameEditText.getText().toString().trim() ));
			nameValuePairs.add(new BasicNameValuePair("email", mEmailEditText.getText().toString().trim() ));
			nameValuePairs.add(new BasicNameValuePair("contact", mContactNoEditText.getText().toString().trim() ));
			nameValuePairs.add(new BasicNameValuePair("issues", mIssueSpinner.getSelectedItem().toString().trim() ));
			nameValuePairs.add(new BasicNameValuePair("message", mMessageEditText.getText().toString().trim() ));
			//nameValuePairs.add(new BasicNameValuePair("district", mDistrictSpinner.getSelectedItem().toString().trim()));

			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

			// Execute HTTP Post Request
			HttpResponse response = httpclient.execute(httppost);

			HttpEntity entity = response.getEntity();

			InputStream inStream = entity.getContent();
			String jsonresponse = Utils.convertStreamToString(inStream);
			Log.e("result==voting qs", jsonresponse);
			JSONObject obj1 = null;

			try {
				obj1 = new JSONObject(jsonresponse);

			} catch (JSONException e) {

				e.printStackTrace();
			}
			try {
				String status = obj1.getString("status");
				if (status.equals("false")) {
					isList=false;
				} else {
					isList=true;
					/*JSONArray medias= obj1.getJSONArray("aclist");
					mACArrayList = new ArrayList<String>();
					for(int i=0;i<medias.length();i++)
					{
						String ac=medias.getString(i);
						//JSONObject item= medias.getJSONObject(i);
						//filesMapMedia.put(item.getString("title"), item.getString("image"));
						Log.e("ac", ac);
						mACArrayList.add(ac);
					}*/
				}

			} catch (JSONException e) {
				e.printStackTrace();
			}
		} catch (ClientProtocolException ex) {
			ex.printStackTrace();
		} catch (IOException ez) {
			ez.printStackTrace();
		}
		return isList;
	}
}