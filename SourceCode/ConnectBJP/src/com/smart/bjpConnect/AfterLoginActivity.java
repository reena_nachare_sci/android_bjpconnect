package com.smart.bjpConnect;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import android.os.AsyncTask;
import android.os.Bundle;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;
import android.view.View.OnClickListener;

public class AfterLoginActivity extends ActinBarActivity {

	private ArrayList<String> optionsImages;
	private ArrayList<String> optionsAudio;
	private ArrayList<String> optionsVideo;

	private Map<String, String> filesMapImages;
	private Map<String, String> filesMapAudio;
	private Map<String, String> filesMapVideo;

	private Spinner categorySpinner;
	private Spinner optionsSpinner;
	private Button downloadButton;

	private ProgressDialog dlgProgress;
	private Context mContext;

	//private ArrayAdapter<String> dataAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_after_login);
		setupActionBar();
		TextView title = (TextView) actionBarLayout.findViewById(R.id.txt_title);
		title.setText("BJP VOLUNTEER LOGIN");
		mContext = this;

		filesMapImages = new HashMap<String, String>();
		filesMapAudio = new HashMap<String, String>();
		filesMapVideo = new HashMap<String, String>();

		optionsImages = new ArrayList<String>();
		optionsAudio =  new ArrayList<String>();
		optionsVideo = new ArrayList<String>();

		categorySpinner = (Spinner) findViewById(R.id.category_spinner);
		optionsSpinner = (Spinner) findViewById(R.id.files_spinner);
		downloadButton = (Button) findViewById(R.id.buttonDownload);

		if(CandidateLoginActivity.filesMapImages != null){
			Iterator<?> iter = CandidateLoginActivity.filesMapImages.entrySet().iterator();

			while (iter.hasNext()) {
				@SuppressWarnings("rawtypes")
				Map.Entry mEntry = (Map.Entry) iter.next();
				System.out.println(mEntry.getKey() + " : " + mEntry.getValue());
				filesMapImages.put((String)mEntry.getKey(), (String)mEntry.getValue());	

				optionsImages.add((String) mEntry.getKey());

			}
		}

		if(CandidateLoginActivity.filesMapAudio != null){
			Iterator<?> iter = CandidateLoginActivity.filesMapAudio.entrySet().iterator();

			while (iter.hasNext()) {
				@SuppressWarnings("rawtypes")
				Map.Entry mEntry = (Map.Entry) iter.next();
				System.out.println(mEntry.getKey() + " : " + mEntry.getValue());
				filesMapAudio.put((String)mEntry.getKey(), (String)mEntry.getValue());	

				optionsAudio.add((String) mEntry.getKey());
			}
		}

		if(CandidateLoginActivity.filesMapVideo != null){
			Iterator<?> iter = CandidateLoginActivity.filesMapVideo.entrySet().iterator();

			while (iter.hasNext()) {
				@SuppressWarnings("rawtypes")
				Map.Entry mEntry = (Map.Entry) iter.next();
				System.out.println(mEntry.getKey() + " : " + mEntry.getValue());
				filesMapVideo.put((String)mEntry.getKey(), (String)mEntry.getValue());	

				optionsVideo.add((String) mEntry.getKey());
			}
		}

		categorySpinner.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				if(categorySpinner.getSelectedItem().toString().trim().equals("image")){
					ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(mContext,android.R.layout.simple_spinner_item, optionsImages);
					dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
					optionsSpinner.setAdapter(dataAdapter);
				}else if(categorySpinner.getSelectedItem().toString().trim().equals("audio")){
					ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(mContext,android.R.layout.simple_spinner_item, optionsAudio);
					dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
					optionsSpinner.setAdapter(dataAdapter);

				}else if(categorySpinner.getSelectedItem().toString().trim().equals("video")){
					ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(mContext,android.R.layout.simple_spinner_item, optionsVideo);
					dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
					optionsSpinner.setAdapter(dataAdapter);
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {

			}
		});

		downloadButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				if(categorySpinner.getSelectedItem().toString().trim().equals("image")){

					Iterator<?> iter = CandidateLoginActivity.filesMapImages.entrySet().iterator();
					while (iter.hasNext()) {
						@SuppressWarnings("rawtypes")
						Map.Entry mEntry = (Map.Entry) iter.next();
						System.out.println(mEntry.getKey() + " : " + mEntry.getValue());
						if(optionsSpinner.getSelectedItem().toString().trim().equals(mEntry.getKey())){
							//downloadImagesToSdCard(mEntry.getValue().toString().trim());
							DownloadTask DT = new DownloadTask();
							DT.execute(mEntry.getValue().toString().trim());
							System.out.println("please come" + mEntry.getValue().toString().trim());
						}
					}
				}else if(categorySpinner.getSelectedItem().toString().trim().equals("audio")){


					Iterator<?> iter = CandidateLoginActivity.filesMapAudio.entrySet().iterator();
					while (iter.hasNext()) {
						@SuppressWarnings("rawtypes")
						Map.Entry mEntry = (Map.Entry) iter.next();
						System.out.println(mEntry.getKey() + " : " + mEntry.getValue());
						if(optionsSpinner.getSelectedItem().toString().trim().equals(mEntry.getKey())){
							//downloadImagesToSdCard(mEntry.getValue().toString().trim());

							DownloadTask DT = new DownloadTask();
							DT.execute(mEntry.getValue().toString().trim());
							System.out.println("please come" + mEntry.getValue().toString().trim());
						}
					}


				}else if(categorySpinner.getSelectedItem().toString().trim().equals("video")){

					Iterator<?> iter = CandidateLoginActivity.filesMapVideo.entrySet().iterator();
					while (iter.hasNext()) {
						@SuppressWarnings("rawtypes")
						Map.Entry mEntry = (Map.Entry) iter.next();
						System.out.println(mEntry.getKey() + " : " + mEntry.getValue());
						if(optionsSpinner.getSelectedItem().toString().trim().equals(mEntry.getKey())){
							//downloadImagesToSdCard(mEntry.getValue().toString().trim());

							DownloadTask DT = new DownloadTask();
							DT.execute(mEntry.getValue().toString().trim());
							System.out.println("please come" + mEntry.getValue().toString().trim());
						}
					}

				}
			}
		});
	}

	public class DownloadTask extends AsyncTask<String, Integer, String>{

		protected void onPreExecute() {
			Log.e("Pre","KK");
			dlgProgress = new ProgressDialog(AfterLoginActivity.this);
			dlgProgress.setMessage("Please wait");
			dlgProgress.setCancelable(false);
			dlgProgress.show();
		}

		protected void onPostExecute(String result) {
			// Getting adapter of the listview
			Log.e("post","KK");
			dlgProgress.dismiss();

			Toast.makeText(getApplicationContext(), "File Saved In BJPConnect Folder On SD CARD ", Toast.LENGTH_SHORT).show();

			/*if(status){
				Intent toAfterLogin = new Intent(CandidateLoginActivity.this,
						AfterLoginActivity.class);
				startActivity(toAfterLogin);
			}*/
		}

		@SuppressLint("SdCardPath")
		@Override
		protected String doInBackground(String... aurl) {
			int count;

			try {

				URL url = new URL(aurl[0]);
				URLConnection conexion = url.openConnection();
				conexion.connect();

				File root = android.os.Environment.getExternalStorageDirectory();

				int lenghtOfFile = conexion.getContentLength();
				Log.d("ANDRO_ASYNC", "Lenght of file: " + lenghtOfFile);



				// create a File object for the parent directory
				File Directory = new File("/sdcard/BJPConnect/");

				if(!Directory.exists()){
					// have the object build the directory structure, if needed.
					Directory.mkdirs();
				}

				InputStream input = new BufferedInputStream(url.openStream());
				//OutputStream output = new FileOutputStream("/sdcard/foldername/temp.zip");
				String a = root.getAbsolutePath() + "/BJPConnect/" + aurl[0].substring(aurl[0].lastIndexOf('/')+1);
				System.out.println("printing path : "+a);
				OutputStream output = new FileOutputStream(a);
				byte data[] = new byte[2048];

				long total = 0;

				while ((count = input.read(data)) != -1) {
					total += count;
					//publishProgress(""+(int)((total*100)/lenghtOfFile));
					Log.d("%Percentage%",""+(int)((total*100)/lenghtOfFile));
					onProgressUpdate((int)((total*100)/lenghtOfFile));
					output.write(data, 0, count);
				}

				output.flush();
				output.close();
				input.close();
			} catch (Exception e) {}
			return null;
		}
		protected void onProgressUpdate(Integer... progress) {
		}
		protected void onPostExecute(Void result)    {
		}
	}

}