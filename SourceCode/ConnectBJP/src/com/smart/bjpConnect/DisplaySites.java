package com.smart.bjpConnect;

import com.smart.bjpConnect.quickaction.ActionItem;
import com.smart.bjpConnect.quickaction.QuickAction;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.TextView;

public class DisplaySites extends ActinBarActivity{

	//private ConnectionDetector cd;
	ProgressDialog dlgProgress;
	WebView display;
	private String from;
	TextView title;


	private QuickAction quickAction;
	ImageButton credit;

	// action id
	private static final int ID_SHARE = 2;
	private static final int ID_LOGIN = 1;
	private static final int ID_NOTIFY = 3;
	private static final int ID_CALL = 5;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_display_site);
		setupActionBar();

	}
	@SuppressLint("SetJavaScriptEnabled")
	@Override
	protected void onResume() {
		super.onResume();
		setupActionBar();
		title = (TextView) actionBarLayout.findViewById(R.id.txt_title);

		credit = (ImageButton) findViewById(R.id.credit1);
		credit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if(from.equals("vision")){
					Intent toShare = new Intent(DisplaySites.this,
							ShareButtonActivity.class);
					toShare.putExtra("TOSHARE"," - Putting Poor First, - Economy Grows, India prospers, -War Within Enemies Within and Without, - Jai Jawan In Action, - Energy Saved, Energy Gained, - Women Empowered, Nation Strengthened, - Young India, National Pillar ");
					startActivity(toShare);
				}else if(from.equals("mediapress")){
					Intent toShare = new Intent(DisplaySites.this,
							ShareButtonActivity.class);
					if(getIntent().getExtras()!=null){
						String mediaUrl= getIntent().getExtras().getString("urlmediaandpress");
						toShare.putExtra("TOSHARE",mediaUrl);
						startActivity(toShare);
					}
				}else if(from.equals("join")){
					Intent toShare = new Intent(DisplaySites.this,
							ShareButtonActivity.class);
					toShare.putExtra("TOSHARE","http://www.bjpdelhi.org/join-delhi-bjp.php");
					startActivity(toShare);
				}else{
					Intent toShare = new Intent(DisplaySites.this,
							ShareButtonActivity.class);
					startActivity(toShare);
				}
			}
		});

		setupMenuOptions();

		//cd= new ConnectionDetector(DisplaySites.this);

		final AlertDialog alertDialog = new AlertDialog.Builder(this).create();

		dlgProgress=new ProgressDialog(DisplaySites.this);
		dlgProgress.setMessage("Loading..");
		dlgProgress.setCancelable(true);

		dlgProgress.show();

		display= (WebView)findViewById(R.id.web_site);
		display.getSettings().setSupportZoom(true);
		display.getSettings().setBuiltInZoomControls(true);
		display.getSettings().setJavaScriptEnabled(true);
		display.getSettings().setCacheMode(WebSettings.LOAD_DEFAULT);
		display.getSettings().setAppCacheMaxSize(1024*1024*8);
		/*display.getSettings().setLoadWithOverviewMode(true);		
		display.getSettings().setUseWideViewPort(true);*/

		if(getIntent().getExtras()!=null)
		{
			from= getIntent().getExtras().getString("display");
		}

		if(from.equals("bjp_india")){
			title.setText("BJP in India");
			display.loadUrl("file:///android_asset/2BJP_Delhi.html");
		}
		if(from.equals("join")){
			title.setText("Join BJP");
			display.loadUrl("http://www.bjpdelhi.org/join-delhi-bjp.php");
		}
		if(from.equals("media")){

			title.setText("Media and Press");
			display.loadUrl("http://www.bjpdelhi.org/inthenews.php");
		} 
		if(from.equalsIgnoreCase("facebook"))
		{
			Log.e("facebook","KKK");
			title.setText("FACEBOOK");
			display.loadUrl("https://www.facebook.com/BJPDelhi");
		}
		if(from.equalsIgnoreCase("twitter"))
		{
			Log.e("twitter","KKK");
			title.setText("TWITTER");
			display.loadUrl("https://twitter.com/BJPDelhiState");
		}
		if(from.equalsIgnoreCase("google"))
		{
			Log.e("google","KKK");
			title.setText("GOOGLE+");
			display.loadUrl("https://plus.google.com/112414740086721446028/about");
		}
		if(from.equalsIgnoreCase("youtube"))
		{
			Log.e("youtube","KKK");
			title.setText("BJP VIDEOS");
			display.loadUrl("http://m.youtube.com/user/BJPDelhiState");
		}
		if(from.equalsIgnoreCase("blogs"))
		{
			Log.e("blogs","KKK");
			title.setText("BLOGS");
			display.loadUrl("http://bjp-delhi.blogspot.in/");
		}
		if(from.equalsIgnoreCase("vision"))
		{
			Log.e("blogs","KKK");
			title.setText("BJP VISION");
			display.loadUrl("http://www.smartcloudinfo.com/BJPConnect/index.php/staticcontents/viewManifesto");
		}


		if(from.equalsIgnoreCase("manifestobuilder"))
		{
			Log.e("blogs","KKK");
			title.setText("MANIFESTO BUILDER");
			display.loadUrl("http://manifestobuilder.bjpdelhi.org/");
		}

		if(from.equalsIgnoreCase("articles"))
		{
			Log.e("blogs","KKK");
			title.setText("ARTICLES");
			display.loadUrl("http://www.smartcloudinfo.com/BJPConnect/index.php/staticcontents/viewManifesto");
		}
		if(from.equals("mediapress")){

			display.getSettings().setLoadWithOverviewMode(true);
			display.getSettings().setUseWideViewPort(true);
			title.setText("MEDIA AND PRESS");
			if(getIntent().getExtras()!=null){
				String mediaUrl= getIntent().getExtras().getString("urlmediaandpress");
				display.loadUrl(mediaUrl);
			}
		}

		if(from.equals("articles")){

			display.getSettings().setLoadWithOverviewMode(true);
			display.getSettings().setUseWideViewPort(true);
			title.setText("ARTICLES");
			if(getIntent().getExtras()!=null){
				String mediaUrl= getIntent().getExtras().getString("urlarticles");
				display.loadUrl(mediaUrl);
			}
		}

		display.setWebViewClient(new WebViewClient() {
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				Log.i("kk", "Processing..");
				view.getSettings().setBuiltInZoomControls(true);
				view.loadUrl(url);
				return true;
			}

			public void onPageFinished(WebView view, String url) {
				Log.i("KK", "Finished loading URL: " +url);
				//if (dlgProgress.isShowing()) {
				dlgProgress.dismiss();
				// }
			}

			@SuppressWarnings("deprecation")
			public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
				Log.e("KK", "Error: " + description);
				dlgProgress.dismiss();
				// Toast.makeText(activity, "Oh no! " + description, Toast.LENGTH_SHORT).show();
				alertDialog.setTitle("Error");
				alertDialog.setMessage(description);
				alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						return;
					}
				});
				alertDialog.show();
			}
		});
	}


	private void setupMenuOptions() {
		// TODO Auto-generated method stub
		ActionItem nextItem= new ActionItem(ID_NOTIFY, "", getResources()
				.getDrawable(R.drawable.notification));
		ActionItem prevItem = new ActionItem(ID_LOGIN, "", getResources()
				.getDrawable(R.drawable.login_button));
		ActionItem searchItem = new ActionItem(ID_SHARE, "", getResources()
				.getDrawable(R.drawable.i_sign));
		ActionItem callItem= new ActionItem(ID_CALL, "", getResources()
				.getDrawable(R.drawable.join_us_with_a_missed_call_01));
		/*ActionItem creditItem= new ActionItem(ID_CREDITS, "", getResources()
				.getDrawable(R.drawable.btn_credits));*/
		/*ActionItem creditItem= new ActionItem(ID_CREDITS, "", getResources()
				.getDrawable(R.drawable.credits_active));*/

		// use setSticky(true) to disable QuickAction dialog being dismissed
		// after an item is clicked
		prevItem.setSticky(true);
		nextItem.setSticky(true);
		callItem.setSticky(true);
		//creditItem.setSticky(true);

		// create QuickAction. Use QuickAction.VERTICAL or
		// QuickAction.HORIZONTAL param to define layout
		// orientation
		quickAction = new QuickAction(this,
				QuickAction.VERTICAL);

		// add action items into QuickAction
		quickAction.addActionItem(nextItem);
		quickAction.addActionItem(prevItem);
		quickAction.addActionItem(searchItem);
		quickAction.addActionItem(callItem);
		//quickAction.addActionItem(creditItem);

		// Set listener for action item clicked
		quickAction
		.setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {
			@Override
			public void onItemClick(QuickAction source, int pos,
					int actionId) {
				@SuppressWarnings("unused")
				ActionItem actionItem = quickAction.getActionItem(pos);

				// here we can filter which action item was clicked with
				// pos or actionId parameter
				if (actionId == ID_NOTIFY) {
					Intent toSettings = new Intent(DisplaySites.this,
							NotificationActivity.class);
					startActivity(toSettings);
				} else if (actionId == ID_SHARE) {
					Intent toCreditAct = new Intent(DisplaySites.this,
							CreditsActivity.class);
					startActivity(toCreditAct);
				} else if(actionId == ID_LOGIN){
					Intent toLoginAct = new Intent(DisplaySites.this,
							CandidateLoginActivity.class);
					startActivity(toLoginAct);
				}
				else if(actionId == ID_CALL){
					Intent toLoginAct = new Intent(DisplaySites.this,
							JoinWithCallActivity.class);
					startActivity(toLoginAct);
				}
			}
		});

		// set listnener for on dismiss event, this listener will be called only
		// if QuickAction dialog was dismissed
		// by clicking the area outside the dialog.
		quickAction.setOnDismissListener(new QuickAction.OnDismissListener() {
			@Override
			public void onDismiss() {
				// Toast.makeText(getApplicationContext(), "Dismissed",
				// Toast.LENGTH_SHORT).show();
			}
		});
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btn_menu:
			quickAction.show(v);
			break;

		default:
			break;
		}
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
	}
}