package com.smart.bjpConnect;

import com.smart.bjpConnect.quickaction.ActionItem;
import com.smart.bjpConnect.quickaction.QuickAction;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.ImageButton;
import android.widget.TextView;

public class AndroidLoadActivity extends ActinBarActivity {

	private WebView faq_text;
	private String from;

	private String one;
	private String two;
	private String three;
	private String four;

	private String mShareOne;
	private String mShareTwo;
	private String mShareThree;
	private String mShareFour;

	private QuickAction quickAction;
	ImageButton credit;

	// action id
	private static final int ID_SHARE = 2;
	private static final int ID_LOGIN = 1;
	private static final int ID_NOTIFY = 3;
	private static final int ID_CALL = 5;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_android_load);

		setupActionBar();

		credit = (ImageButton) findViewById(R.id.credit1);
		credit.setOnClickListener(new OnClickListener() {

			@Override public void onClick(View v) {
				if (from.trim()
						.equals("What are the documents I need to register as a voter?")) {
					Intent toShare = new Intent(
							AndroidLoadActivity.this,
							ShareButtonActivity.class);
					toShare.putExtra("TOSHARE", from + " Answer : "
							+ mShareOne);
					startActivity(toShare);
				} else if (from.trim()
						.equals("I don't have an address proof document on my name. Can I still register to vote?")) {
					Intent toShare = new Intent(
							AndroidLoadActivity.this,
							ShareButtonActivity.class);
					toShare.putExtra("TOSHARE", from + " Answer : "
							+ mShareTwo);
					startActivity(toShare);
				} else if (from
						.trim()
						.equals("I submitted my registration form but was denied the receipt of application. Where should I complain?")) {
					Intent toShare = new Intent(
							AndroidLoadActivity.this,
							ShareButtonActivity.class);
					toShare.putExtra("TOSHARE", from + " Answer : "
							+ mShareThree);
					startActivity(toShare);
				} else {
					Intent toShare = new Intent(
							AndroidLoadActivity.this,
							ShareButtonActivity.class);
					toShare.putExtra("TOSHARE", from + " Answer : "
							+ mShareFour);
					startActivity(toShare);
				}
			} 
		});


		setupMenuOptions();

		
		TextView title = (TextView) actionBarLayout
				.findViewById(R.id.txt_title);
		title.setText("ANSWERS");

		if (getIntent().getExtras() != null) {
			from = getIntent().getExtras().getString("display");
		}

		faq_text = (WebView) findViewById(R.id.faq_text);

		one = "<html><head>"

				+ "<body style=\"text-align: justify\">"
				+ "<p><h3>"
				+ ""
				+ "</h3></p>"

				+ "<p>"
				+ "Apart from your voter registration form, you will need to submit:"
				+ "</p>"
				+ "<p><li>"
				+ "An Address Proof document. This document is required for the BLO (Booth Level Officer) to come and verify your residence status. It is therefore not essential for the address proof document to have your name. It needs to have only the address of the place where you are staying."
				+ "</li></p>"
				+ "<hr>"
				+ "<p><li>"
				+ "If you are a first time voter, you will also need to submit an age proof document. You can submit the proof of Date of Birth from any government agency authorized document like birth certificate, passport, PAN Card, Driving Licence, state board exam certificate etc.� "
				+ "</li></p>"
				+ "<hr>"
				+ "<p><li>"
				+ "If you are a student residing in a hostel / mess, you might not have any of the above mentioned address proof documents. In this case, you can get a Student Declaration Form signed by your college dean/principal/registrar and submit it along with your voter registration form."
				+ "</li></p>"

				+ "<p><h3>" + "" + "</h3></p>"

				+ "</body>" + "</head></html>";

		mShareOne = "Apart from your voter registration form, you will need to submit: An Address Proof document. This document is required for the BLO (Booth Level Officer) to come and verify your residence status. It is therefore not essential for the address proof document to have your name. It needs to have only the address of the place where you are staying. If you are a first time voter, you will also need to submit an age proof document. You can submit the proof of Date of Birth from any government agency authorized document like birth certificate, passport, PAN Card, Driving Licence, state board exam certificate etc. If you are a student residing in a hostel / mess, you might not have any of the above mentioned address proof documents. In this case, you can get a Student Declaration Form signed by your college dean/principal/registrar and submit it along with your voter registration form.";

		// txt_about.setText(Html.fromHtml(textfir));

		two = "<html><head>"

		+ "<body style=\"text-align: justify\">" + "<p><h3>" + "" + "</h3></p>"

		+ "<p>" + "Yes, you can still register to vote." + "</p>"

		+ "<p><h3>" + "" + "</h3></p>"

		+ "</body>" + "</head></html>";

		mShareTwo = "Yes, you can still register to vote.";

		three = "<html><head>"

				+ "<body style=\"text-align: justify\">"
				+ "<p><h3>"
				+ ""
				+ "</h3></p>"

				+ "<p>"
				+ "If you don't get a satisfactory response from your ERO, you should complain with your state Chief Electoral Officer (CEO). You can find the contact details of your state CEO at the state CEO's website. If you are still not satisfied, you can directly complain to the Election Commission of India (ECI)."
				+ "</p>"

				+ "<p><h3>" + "" + "</h3></p>"

				+ "</body>" + "</head></html>";

		mShareThree = "If you don't get a satisfactory response from your ERO, you should complain with your state Chief Electoral Officer (CEO). You can find the contact details of your state CEO at the state CEO's website. If you are still not satisfied, you can directly complain to the Election Commission of India (ECI).";

		four = "<html><head>"

				+ "<body style=\"text-align: justify\">"
				+ "<p><h3>"
				+ ""
				+ "</h3></p>"

				+ "<p>"
				+ " If there is an upcoming election in your state, registration stops normally 10 days before the final date of accepting candidate nominations. Please check your local newspapers or the website of your state Chief Electoral Officer (CEO) for the exact final date for accepting voter registration forms."
				+ "</p>"

				+ "<p><h3>" + "" + "</h3></p>"

				+ "</body>" + "</head></html>";

		mShareFour = " If there is an upcoming election in your state, registration stops normally 10 days before the final date of accepting candidate nominations. Please check your local newspapers or the website of your state Chief Electoral Officer (CEO) for the exact final date for accepting voter registration forms.";

		if (from.trim().equals(
				"What are the documents I need to register as a voter?")) {
			faq_text.loadDataWithBaseURL(null, one, null, "utf-8", null);
			// faq_text.setText(Html.fromHtml(one));
		} else if (from
				.trim()
				.equals("I don't have an address proof document on my name. Can I still register to vote?")) {
			faq_text.loadDataWithBaseURL(null, two, null, "utf-8", null);
			// faq_text.setText(Html.fromHtml(two));
		} else if (from
				.trim()
				.equals("I submitted my registration form but was denied the receipt of application. Where should I complain?")) {
			faq_text.loadDataWithBaseURL(null, three, null, "utf-8", null);
			// faq_text.setText(Html.fromHtml(three));
		} else {
			faq_text.loadDataWithBaseURL(null, four, null, "utf-8", null);
			// faq_text.setText(Html.fromHtml(four));
		}
	}

	private void setupMenuOptions() {
		// TODO Auto-generated method stub
		ActionItem nextItem = new ActionItem(ID_NOTIFY, "", getResources()
				.getDrawable(R.drawable.notification));
		ActionItem prevItem = new ActionItem(ID_LOGIN, "", getResources()
				.getDrawable(R.drawable.login_button));
		ActionItem searchItem = new ActionItem(ID_SHARE, "", getResources()
				.getDrawable(R.drawable.share_01));
		ActionItem callItem = new ActionItem(ID_CALL, "", getResources()
				.getDrawable(R.drawable.join_us_with_a_missed_call_01));
		/*
		 * ActionItem creditItem= new ActionItem(ID_CREDITS, "", getResources()
		 * .getDrawable(R.drawable.btn_credits));
		 */
		/*
		 * ActionItem creditItem= new ActionItem(ID_CREDITS, "", getResources()
		 * .getDrawable(R.drawable.credits_active));
		 */

		// use setSticky(true) to disable QuickAction dialog being dismissed
		// after an item is clicked
		prevItem.setSticky(true);
		nextItem.setSticky(true);
		callItem.setSticky(true);
		// creditItem.setSticky(true);

		// create QuickAction. Use QuickAction.VERTICAL or
		// QuickAction.HORIZONTAL param to define layout
		// orientation
		quickAction = new QuickAction(this, QuickAction.VERTICAL);

		// add action items into QuickAction
		quickAction.addActionItem(nextItem);
		quickAction.addActionItem(prevItem);
		quickAction.addActionItem(searchItem);
		quickAction.addActionItem(callItem);
		// quickAction.addActionItem(creditItem);

		// Set listener for action item clicked
		quickAction
		.setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {
			@Override
			public void onItemClick(QuickAction source, int pos,
					int actionId) {
				@SuppressWarnings("unused")
				ActionItem actionItem = quickAction.getActionItem(pos);

				// here we can filter which action item was clicked with
				// pos or actionId parameter
				if (actionId == ID_NOTIFY) {
					Intent toSettings = new Intent(
							AndroidLoadActivity.this,
							NotificationActivity.class);
					startActivity(toSettings);

				} else if (actionId == ID_SHARE) {
					Intent toCreditAct = new Intent(AndroidLoadActivity.this, CreditsActivity.class);
					startActivity(toCreditAct);
				} else if (actionId == ID_LOGIN) {
					Intent toLoginAct = new Intent(
							AndroidLoadActivity.this,
							CandidateLoginActivity.class);
					startActivity(toLoginAct);
				} else if (actionId == ID_CALL) {
					Intent toLoginAct = new Intent(
							AndroidLoadActivity.this,
							JoinWithCallActivity.class);
					startActivity(toLoginAct);
				}
				/*
				 * else if(actionId == ID_CREDITS){ Intent tocreditAct =
				 * new Intent(MainActivity1.this,
				 * CreditsActivity.class); startActivity(toLoginAct); }
				 */
			}
		});

		// set listnener for on dismiss event, this listener will be called only
		// if QuickAction dialog was dismissed
		// by clicking the area outside the dialog.
		quickAction.setOnDismissListener(new QuickAction.OnDismissListener() {
			@Override
			public void onDismiss() {
				// Toast.makeText(getApplicationContext(), "Dismissed",
				// Toast.LENGTH_SHORT).show();
			}
		});
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btn_menu:
			quickAction.show(v);
			break;

		default:
			break;
		}
	}
}