package com.smart.bjpConnect;

import android.net.Uri;
import android.os.Bundle;
import android.view.View.OnClickListener;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class VisionActivity extends ActinBarActivity {

	private Button mManifestoBuilderButton;
	private Button mManifestoButton; 

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_vision);

		setupActionBar();
		TextView title = (TextView) actionBarLayout.findViewById(R.id.txt_title);
		title.setText("BJP VISION");

		mManifestoBuilderButton = (Button) findViewById(R.id.manifestoBuilderButton);

		mManifestoBuilderButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				/*Intent toBJPVision= new Intent(VisionActivity.this, DisplaySites.class);
				toBJPVision.putExtra("display", "manifestobuilder");
				toBJPVision.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(toBJPVision);*/

				//Uri registerUrl = Uri.parse("http://manifestobuilder.bjpdelhi.org/");  
				//Intent register = new Intent(Intent.ACTION_VIEW, registerUrl);  
				//startActivity(register);
				
				Intent toJoinBJP= new Intent(VisionActivity.this, ManifestoBuilderFormActivity.class);
				toJoinBJP.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(toJoinBJP);
				
			}
		});

		mManifestoButton = (Button) findViewById(R.id.manifestoButton);

		mManifestoButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent toBJPVision= new Intent(VisionActivity.this, DisplaySites.class);
				toBJPVision.putExtra("display", "vision");
				toBJPVision.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(toBJPVision);
			}
		});
	}
}
