package com.smart.bjpConnect;
import java.util.ArrayList;

import com.smart.bjpConnect.quickaction.ActionItem;
import com.smart.bjpConnect.quickaction.QuickAction;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class MainActivity1 extends ActinBarActivity implements OnItemSelectedListener, TextWatcher,OnItemClickListener {

	Singleton 				m_Inst 					= Singleton.getInstance();
	CarouselViewAdapter 	m_carouselAdapter		= null;	 
	//private final int		m_nFirstItem			= 1000;
	int selected=0;

	public static float topPixelCarousel;
	private QuickAction quickAction;
	ImageButton credit;

	// action id
	private static final int ID_SHARE = 2;
	private static final int ID_LOGIN = 1;
	private static final int ID_NOTIFY = 3;
	private static final int ID_CALL = 5;
	
	Utils util;
	
	Context mContext;

	/*@Override
	public void onBackPressed() {
		finish();
	}*/

	@SuppressWarnings("deprecation")
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setupActionBar();
		
		mContext = this;

		//to get phone number of user

		/*AccountManager am = AccountManager.get(this);
		Account[] accounts = am.getAccounts();

		//Account ac = null;
		//String actype = null;
		
		for (Account ac : accounts) {
		    String acname = ac.name;
		    String actype = ac.type;
		    // Take your time to look at all available accounts
		    System.out.println("Accounts : " + acname + ", " + actype);
		    
		    if(actype.equals("com.whatsapp")){
			    String phoneNumber = ac.name;
			    System.out.println(phoneNumber);
			}
		}*/
		
		DisplayMetrics metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);
		float dpi= metrics.densityDpi;
		float px=110;
		
		//userId = mSp.getString("UserId", "1");
		
		util= new Utils();
		util.GCMRegistration(mContext,"1");

		credit = (ImageButton) findViewById(R.id.credit1);
		credit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent toCreditAct = new Intent(MainActivity1.this,
						ShareButtonActivity.class);
				startActivity(toCreditAct);
			}
		});

		topPixelCarousel = px * ( dpi / 160);

		//no keyboard unless requested by user
		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN); 

		// compute screen size and scaling
		Singleton.getInstance().InitGUIFrame(this);

		//int padding = m_Inst.Scale(10);
		// create the interface : full screen container
		RelativeLayout panel  = new RelativeLayout(this);
		panel.setBackgroundResource(R.drawable.bg);
		panel.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.FILL_PARENT));
		//panel.setPadding(padding, padding, padding, padding);
		/*  panel.setBackgroundDrawable(new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, 
	    		new int[]{Color.WHITE, Color.GRAY}));*/
		setContentView(panel); 

		setupMenuOptions();
		// copy images from assets to sdcard
		//AppUtils.AssetFileCopy(this, "/mnt/sdcard/plasma1.png", "plasma1.png", false);
		//AppUtils.AssetFileCopy(this, "/mnt/sdcard/plasma2.png", "plasma2.png", false);
		//AppUtils.AssetFileCopy(this, "/mnt/sdcard/plasma3.png", "plasma3.png", false);
		//AppUtils.AssetFileCopy(this, "/mnt/sdcard/plasma4.png", "plasma4.png", false);

		//mBitArray[0] = getBitmapFromAssets("dem.png");
		//mBitArray[1] = getBitmapFromAssets("qwe.png");
		//mBitArray[2] = getBitmapFromAssets("e.png");
		//mBitArray[3] = getBitmapFromAssets("r.png");
		//mBitArray[4] = getBitmapFromAssets("nik.jpg");


		//Create carousel view documents
		ArrayList<CarouselDataItem> Docus = new ArrayList<CarouselDataItem>();
		for (int i=0;i<4;i++) {
			CarouselDataItem docu;
			if (i%4==0) docu = new CarouselDataItem("elections.png", 0, "");
			else if (i%4==1) docu = new CarouselDataItem("social.png", 0, "");
			else if (i%4==2) docu = new CarouselDataItem("issues.png", 0, "");
			else docu = new CarouselDataItem("bjp.png", 0, "");
			Docus.add(docu);
		} 

		// add the serach filter
		//EditText etSearch = new EditText(this);
		//etSearch.setHint("Search your documents");
		//etSearch.setSingleLine();
		//etSearch.setTextColor(Color.BLACK);
		//etSearch.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_menu_search, 0, 0, 0);
		/*AppUtils.AddView(panel, etSearch, LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, 
	    		new int[][]{new int[]{RelativeLayout.CENTER_HORIZONTAL}, new int[]{RelativeLayout.ALIGN_PARENT_TOP}}, -1,-1);
	    etSearch.addTextChangedListener((TextWatcher) this);*/ 

		// add logo
		TextView tv = new TextView(this);
		tv.setTextColor(Color.BLACK);
		tv.setText("");
		AppUtils.AddView(panel, tv, LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, 
				new int[][]{new int[]{RelativeLayout.CENTER_HORIZONTAL}, new int[]{RelativeLayout.ALIGN_PARENT_BOTTOM}}, -1,-1);

		// create the carousel
		CarouselView coverFlow = new CarouselView(this);

		// create adapter and specify device independent items size (scaling)
		// for more details see: http://www.pocketmagic.net/2013/04/how-to-scale-an-android-ui-on-multiple-screens/
		m_carouselAdapter =  new CarouselViewAdapter(this,Docus, m_Inst.Scale(400),m_Inst.Scale(300));
		coverFlow.setAdapter(m_carouselAdapter);
		coverFlow.setSpacing(-1*m_Inst.Scale(150));
		coverFlow.setSelection(Integer.MAX_VALUE / 2, true);
		coverFlow.setAnimationDuration(1000);
		coverFlow.setOnItemSelectedListener((OnItemSelectedListener) this);
		coverFlow.setOnItemClickListener(this);

		AppUtils.AddView(panel, coverFlow, LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT, 
				new int[][]{new int[]{RelativeLayout.CENTER_IN_PARENT}},
				-1, -1); 
	}

	/*public Bitmap getBitmapFromAssets (String filename) throws IOException{
		AssetManager assetManager = getAssets();
		InputStream istr = assetManager.open(filename);
		Bitmap bitmap = BitmapFactory.decodeStream(istr);
		return bitmap;  
	}*/

	private void setupMenuOptions() {
		// TODO Auto-generated method stub
		ActionItem nextItem= new ActionItem(ID_NOTIFY, "", getResources()
				.getDrawable(R.drawable.notification));
		ActionItem prevItem = new ActionItem(ID_LOGIN, "", getResources()
				.getDrawable(R.drawable.login_button));
		ActionItem searchItem = new ActionItem(ID_SHARE, "", getResources()
				.getDrawable(R.drawable.i_sign));
		ActionItem callItem= new ActionItem(ID_CALL, "", getResources()
				.getDrawable(R.drawable.join_us_with_a_missed_call_01));
		/*ActionItem creditItem= new ActionItem(ID_CREDITS, "", getResources()
				.getDrawable(R.drawable.btn_credits));*/
		/*ActionItem creditItem= new ActionItem(ID_CREDITS, "", getResources()
				.getDrawable(R.drawable.credits_active));*/

		// use setSticky(true) to disable QuickAction dialog being dismissed
		// after an item is clicked
		prevItem.setSticky(true);
		nextItem.setSticky(true);
		callItem.setSticky(true);
		//creditItem.setSticky(true);

		// create QuickAction. Use QuickAction.VERTICAL or
		// QuickAction.HORIZONTAL param to define layout
		// orientation
		quickAction = new QuickAction(this,
				QuickAction.VERTICAL);

		// add action items into QuickAction
		quickAction.addActionItem(nextItem);
		quickAction.addActionItem(prevItem);
		quickAction.addActionItem(searchItem);
		quickAction.addActionItem(callItem);
		//quickAction.addActionItem(creditItem);

		// Set listener for action item clicked
		quickAction
		.setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {
			@Override
			public void onItemClick(QuickAction source, int pos,
					int actionId) {
				@SuppressWarnings("unused")
				ActionItem actionItem = quickAction.getActionItem(pos);

				// here we can filter which action item was clicked with
				// pos or actionId parameter
				if (actionId == ID_NOTIFY) {
					Intent toSettings = new Intent(MainActivity1.this,
							NotificationActivity.class);
					startActivity(toSettings);

				} else if (actionId == ID_SHARE) {
					/* Intent fbPost=new Intent(MainActivity1.this, ShareOnFacebook.class);
					    fbPost.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK); //required to launch from non-activity
					    fbPost.putExtra("facebookMessage", "");
					    startActivity(fbPost);*/
					//shareIt();
					//share("BJPConnect","www.smartcloudinfotech.com");

					Intent toShare = new Intent(MainActivity1.this,
							CreditsActivity.class);
					startActivity(toShare);


				} else if(actionId == ID_LOGIN){
					Intent toLoginAct = new Intent(MainActivity1.this,
							CandidateLoginActivity.class);
					startActivity(toLoginAct);
				}
				else if(actionId == ID_CALL){
					Intent toLoginAct = new Intent(MainActivity1.this,
							JoinWithCallActivity.class);
					startActivity(toLoginAct);
				}
				/*else if(actionId == ID_CREDITS){
					Intent tocreditAct = new Intent(MainActivity1.this,
							CreditsActivity.class);
					startActivity(toLoginAct);
				}*/
			}
		});

		// set listnener for on dismiss event, this listener will be called only
		// if QuickAction dialog was dismissed
		// by clicking the area outside the dialog.
		quickAction.setOnDismissListener(new QuickAction.OnDismissListener() {
			@Override
			public void onDismiss() {
				// Toast.makeText(getApplicationContext(), "Dismissed",
				// Toast.LENGTH_SHORT).show();
			}
		});
	}
	/*private void shareIt() {
		Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
		sharingIntent.setType("text/plain");
		String shareBody = "Join BJP Connect";
		sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "BJP Connect");
		sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
		startActivity(Intent.createChooser(sharingIntent, "Share via"));

	}*/

	/*private void share(String nameApp, String imagePath) {
	    List<Intent> targetedShareIntents = new ArrayList<Intent>();
	    Intent share = new Intent(android.content.Intent.ACTION_SEND);
	    //share.setType("image/jpeg");
	    share.setType("text/plain");
	    List<ResolveInfo> resInfo = getPackageManager().queryIntentActivities(share, 0);
	    if (!resInfo.isEmpty()){
	        for (ResolveInfo info : resInfo) {
	            Intent targetedShare = new Intent(android.content.Intent.ACTION_SEND);
	            //targetedShare.setType("image/jpeg"); // put here your mime type
	            targetedShare.setType("text/plain"); // put here your mime type


	            if (info.activityInfo.packageName.toLowerCase().contains(nameApp) || 
	                    info.activityInfo.name.toLowerCase().contains(nameApp)) {
	                targetedShare.putExtra(Intent.EXTRA_TEXT,     "My body of post/email");
	                //targetedShare.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(new File(imagePath)) );
	                targetedShare.setPackage(info.activityInfo.packageName);
	                targetedShareIntents.add(targetedShare);
	            //}
	        }

	        Intent chooserIntent = Intent.createChooser(targetedShareIntents.remove(0), "Select app to share");
	        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, targetedShareIntents.toArray(new Parcelable[]{}));
	        startActivity(chooserIntent);
	    }
	}*/


	public void afterTextChanged(Editable arg0) {}

	public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

	public void onTextChanged(CharSequence s, int start, int before, int count) {
		m_carouselAdapter.getFilter().filter(s.toString()); 
	}

	public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		CarouselDataItem docu =  (CarouselDataItem) m_carouselAdapter.getItem((int) arg3);

		if (docu!=null){
			selected=(int)arg3;
			// Toast.makeText(this, "You've selected on:"+docu.getDocText()+selected, Toast.LENGTH_SHORT).show();
		}
	}

	public void onNothingSelected(AdapterView<?> arg0) {}

	/*public void onClick(View v) {
		// TODO Auto-generated method stub
		 CarouselDataItem docu =  (CarouselDataItem) m_carouselAdapter.getItem(selected);
		 if (docu!=null)
			 Toast.makeText(this, "You've clicked on:"+docu.getDocText(), Toast.LENGTH_SHORT).show();
	}*/

	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		// TODO Auto-generated method stub
		CarouselDataItem docu =  (CarouselDataItem) m_carouselAdapter.getItem((int) arg3);

		if (docu!=null){
			selected=(int)arg3;
			// Toast.makeText(this, "You've clicked on:"+docu.getDocText()+selected, Toast.LENGTH_SHORT).show();
			if(selected==0)
			{
				//go to about SocialActivity 
				Intent toBjp2= new Intent(MainActivity1.this,ElectionActivity.class);
				toBjp2.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(toBjp2);

			}
			if(selected==1)
			{
				// go to elections
				Intent toBjp1= new Intent(MainActivity1.this,SocialActivity.class);
				toBjp1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(toBjp1);

			}
			if(selected==2)
			{
				// go to social
				Intent toBjp3= new Intent(MainActivity1.this,IssuesAtHandActivity.class);
				toBjp3.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(toBjp3);

			}
			if(selected==3)
			{
				// go to issues at BjpActivity

				Intent toBjp4= new Intent(MainActivity1.this,BjpActivity.class);
				toBjp4.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(toBjp4);

			}
		}
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btn_menu:
			quickAction.show(v);
			break;

		default:
			break;
		}
	}
	
	@Override
	protected void onDestroy() {

		util.Unregister(mContext);

		super.onDestroy();

	}
}