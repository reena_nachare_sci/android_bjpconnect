package com.smart.bjpConnect;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.smart.bjpConnect.quickaction.ActionItem;
import com.smart.bjpConnect.quickaction.QuickAction;

public class ActinBarActivity extends SherlockFragmentActivity implements OnClickListener{
	public ViewGroup actionBarLayout, menulay;
	public ActionBar mActionBar;
	public ImageButton mBtn_MoreOptions;
	public ImageButton mBtn_Credit;

	private static final int ID_SHARE = 2;
	private static final int ID_LOGIN = 1;
	private static final int ID_NOTIFY = 3;
	//private static final int ID_CREDITS = 4;
	private static final int ID_CALL = 5;

	private QuickAction quickAction;
	private ImageButton credit;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//setupActionBar();
	}

	public void setupActionBar() {
		setupMenuOptions();
		// TODO Auto-generated method stub
		actionBarLayout = (ViewGroup) getLayoutInflater().inflate(
				R.layout.action_bar, null);
		mActionBar = getSupportActionBar();
		mActionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
		mActionBar.setCustomView(actionBarLayout);

		// menu ImageButton on action bar
		mBtn_MoreOptions = (ImageButton) actionBarLayout
				.findViewById(R.id.btn_menu);
		mBtn_MoreOptions.setOnClickListener(this);


		credit = (ImageButton) actionBarLayout.findViewById(R.id.credit1);
		credit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				Intent toSettings = new Intent(ActinBarActivity.this,
						ShareButtonActivity.class);
				startActivity(toSettings);
			}
		});
	}

	public void setupActionBarNoMenu() {
		setupMenuOptions();
		// TODO Auto-generated method stub
		actionBarLayout = (ViewGroup) getLayoutInflater().inflate(
				R.layout.action_bar, null);
		mActionBar = getSupportActionBar();
		mActionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
		mActionBar.setCustomView(actionBarLayout);

		// menu ImageButton on action bar
		mBtn_MoreOptions = (ImageButton) actionBarLayout
				.findViewById(R.id.btn_menu);
		mBtn_MoreOptions.setOnClickListener(this);

		credit = (ImageButton) actionBarLayout.findViewById(R.id.credit1);
		credit.setVisibility(View.INVISIBLE);
	}
	

	private void setupMenuOptions() {
		// TODO Auto-generated method stub


		ActionItem nextItem= new ActionItem(ID_NOTIFY, "", getResources()
				.getDrawable(R.drawable.notification));
		ActionItem prevItem = new ActionItem(ID_LOGIN, "", getResources()
				.getDrawable(R.drawable.login_button));
		ActionItem searchItem = new ActionItem(ID_SHARE, "", getResources()
				.getDrawable(R.drawable.i_sign));
		ActionItem callItem= new ActionItem(ID_CALL, "", getResources()
				.getDrawable(R.drawable.join_us_with_a_missed_call_01));
		/*ActionItem creditItem= new ActionItem(ID_CREDITS, "", getResources()
				.getDrawable(R.drawable.btn_credits));*/
		/*ActionItem creditItem= new ActionItem(ID_CREDITS, "", getResources()
				.getDrawable(R.drawable.credits_active));*/

		// use setSticky(true) to disable QuickAction dialog being dismissed
		// after an item is clicked
		prevItem.setSticky(true);
		nextItem.setSticky(true);
		callItem.setSticky(true);
		//creditItem.setSticky(true);

		// create QuickAction. Use QuickAction.VERTICAL or
		// QuickAction.HORIZONTAL param to define layout
		// orientation
		quickAction = new QuickAction(this,
				QuickAction.VERTICAL);

		// add action items into QuickAction
		quickAction.addActionItem(nextItem);
		quickAction.addActionItem(prevItem);
		quickAction.addActionItem(searchItem);
		quickAction.addActionItem(callItem);
		//quickAction.addActionItem(creditItem);

		// Set listener for action item clicked
		quickAction
		.setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {
			@Override
			public void onItemClick(QuickAction source, int pos,
					int actionId) {
				@SuppressWarnings("unused")
				ActionItem actionItem = quickAction.getActionItem(pos);

				// here we can filter which action item was clicked with
				// pos or actionId parameter
				if (actionId == ID_NOTIFY) {
					Intent toSettings = new Intent(ActinBarActivity.this,
							NotificationActivity.class);
					startActivity(toSettings);
				} else if (actionId == ID_SHARE) {
					Intent toCreditAct = new Intent(ActinBarActivity.this,
							CreditsActivity.class);
					startActivity(toCreditAct);
				} else if(actionId == ID_LOGIN){
					Intent toLoginAct = new Intent(ActinBarActivity.this,
							CandidateLoginActivity.class);
					startActivity(toLoginAct);
				}
				else if(actionId == ID_CALL){
					Intent toLoginAct = new Intent(ActinBarActivity.this,
							JoinWithCallActivity.class);
					startActivity(toLoginAct);
				}
			}
		});

		// set listnener for on dismiss event, this listener will be called only
		// if QuickAction dialog was dismissed
		// by clicking the area outside the dialog.
		quickAction.setOnDismissListener(new QuickAction.OnDismissListener() {
			@Override
			public void onDismiss() {
				// Toast.makeText(getApplicationContext(), "Dismissed",
				// Toast.LENGTH_SHORT).show();
			}
		});
	}

	/*private void shareIt() {
		Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
		sharingIntent.setType("text/plain");
		String shareBody = "Join BJP Connect";
		sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "BJP Connect");
		sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
		startActivity(Intent.createChooser(sharingIntent, "Share via"));

	}*/
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btn_menu:
			quickAction.show(v);
			break;

		default:
			break;
		}
	}
}
