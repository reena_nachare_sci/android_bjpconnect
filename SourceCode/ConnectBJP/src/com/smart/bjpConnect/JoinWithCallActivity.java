package com.smart.bjpConnect;

import android.net.Uri;
import android.os.Bundle;
import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.view.View.OnClickListener;
 

public class JoinWithCallActivity extends ActinBarActivity {
	
	private Button buttonJoinUs;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_join_with_call);
		setupActionBar();
		TextView title = (TextView) actionBarLayout.findViewById(R.id.txt_title);
		title.setText("JOIN US WITH CALL");
		buttonJoinUs = (Button) findViewById(R.id.buttonJoinUs);
		
		buttonJoinUs.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(Intent.ACTION_VIEW);
				intent.setData(Uri.parse("tel:01166575555"));
				startActivity(intent);
			}
		});
	}
}