package com.smart.bjpConnect;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.view.View.OnClickListener;

public class JoinBJPFormActivity extends ActinBarActivity {

	private EditText mNameEditText;
	private RadioGroup mSexRadioGroup;
	private Spinner mProfessionSpinner;
	private EditText mAddressEditText;
	private EditText mPincodeEditText;
	private EditText mTelephoneEditText;
	private EditText mMobileEditText;
	private EditText mEmailEditText;
	private Button mSubmitButton;
	private Button mJoinBJPButton;

	private String radioSexString;

	private Button mDateOfBirthButton;

	private DatePicker dpResult;
	private final int DATE_DIALOG_ID = 999;

	private int year; 
	private int month; 
	private int day;

	private int globalpYear;
	private int globalpMonth;
	private int globalpDay;

	ConnectionDetector cd;

	private ProgressDialog dlgProgress;

	//private Context mContext;

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_join_bjpform);

		//mContext = this;

		setupActionBar();
		TextView title = (TextView) actionBarLayout.findViewById(R.id.txt_title);
		title.setText("JOIN BJP");

		cd= new ConnectionDetector(JoinBJPFormActivity.this);

		mNameEditText = (EditText) findViewById(R.id.nameText);
		mAddressEditText = (EditText) findViewById(R.id.addressText);
		mPincodeEditText = (EditText) findViewById(R.id.pincodeText);
		mTelephoneEditText = (EditText) findViewById(R.id.telephoneText);
		mMobileEditText = (EditText) findViewById(R.id.mobileText);
		mEmailEditText = (EditText) findViewById(R.id.emailText);

		mProfessionSpinner = (Spinner) findViewById(R.id.profession_spinner);
		//mProfessionSpinner.setGravity(17);

		mSexRadioGroup = (RadioGroup) findViewById(R.id.radioSex);

		mDateOfBirthButton = (Button) findViewById(R.id.dobButton);
		mJoinBJPButton = (Button) findViewById(R.id.buttonjoinWithMissCall);

		mJoinBJPButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(Intent.ACTION_VIEW);
				intent.setData(Uri.parse("tel:01166575555"));
				startActivity(intent);
			}
		});

		mSubmitButton = (Button) findViewById(R.id.buttonjoinBJP);

		final Calendar cal = Calendar.getInstance();
		globalpYear = cal.get(Calendar.YEAR);
		globalpMonth = cal.get(Calendar.MONTH);
		globalpDay = cal.get(Calendar.DAY_OF_MONTH);

		setCurrentDateOnView();
		addListenerOnButton();

		mSubmitButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				if(mNameEditText.getText().toString().trim().length() == 0){
					Toast toast = Toast.makeText(getApplicationContext(),
							"Please fill name field properly",
							Toast.LENGTH_SHORT);

					toast.setGravity(Gravity.CENTER, 0, 0);
					toast.show();
				}else if(mAddressEditText.getText().toString().trim().length() == 0){
					Toast toast = Toast.makeText(getApplicationContext(),
							"Please fill address field properly",
							Toast.LENGTH_SHORT);

					toast.setGravity(Gravity.CENTER, 0, 0);
					toast.show();
				}else if(mPincodeEditText.getText().toString().trim().length() == 0){
					Toast toast = Toast.makeText(getApplicationContext(),
							"Please fill pincode field properly",
							Toast.LENGTH_SHORT);

					toast.setGravity(Gravity.CENTER, 0, 0);
					toast.show();
				}else if(mTelephoneEditText.getText().toString().trim().length() == 0){
					Toast toast = Toast.makeText(getApplicationContext(),
							"Please fill telephone field properly",
							Toast.LENGTH_SHORT);

					toast.setGravity(Gravity.CENTER, 0, 0);
					toast.show();
				}else if(mMobileEditText.getText().toString().trim().length() == 0){
					Toast toast = Toast.makeText(getApplicationContext(),
							"Please fill mobile field properly",
							Toast.LENGTH_SHORT);

					toast.setGravity(Gravity.CENTER, 0, 0);
					toast.show();
				}else if(mEmailEditText.getText().toString().trim().length() == 0){
					Toast toast = Toast.makeText(getApplicationContext(),
							"Please fill email field properly",
							Toast.LENGTH_SHORT);

					toast.setGravity(Gravity.CENTER, 0, 0);
					toast.show();
				}else if(mProfessionSpinner.getSelectedItem().toString().trim().equals("Select Profession")){
					Toast toast = Toast.makeText(getApplicationContext(),
							"Please select profession",
							Toast.LENGTH_SHORT);

					toast.setGravity(Gravity.CENTER, 0, 0);
					toast.show();
				}else{
					mSexRadioGroup = (RadioGroup) findViewById(R.id.radioSex);
					// get selected radio button from radioGroup
					int selectedId = mSexRadioGroup.getCheckedRadioButtonId();

					// find the radiobutton by returned id
					RadioButton radioSexButton = (RadioButton) findViewById(selectedId);

					radioSexString = String.valueOf(radioSexButton.getText());


					if(cd.isConnectingToInternet()){
						JoinBJP DT = new JoinBJP();
						DT.execute();
					}else{
						Toast.makeText(JoinBJPFormActivity.this, "NO Internet", Toast.LENGTH_SHORT).show();	
						//finish();
					}
				}
			}
		});
	}

	private void setCurrentDateOnView() {

		//dateOfBirthButton = (Button) findViewById(R.id.dateOfBirthText);
		mDateOfBirthButton = (Button) findViewById(R.id.dobButton);
		dpResult = new DatePicker(JoinBJPFormActivity.this);

		final Calendar c = Calendar.getInstance();
		year = c.get(Calendar.YEAR);
		month = c.get(Calendar.MONTH);
		day = c.get(Calendar.DAY_OF_MONTH);

		// set current date into textview
		mDateOfBirthButton.setText(new StringBuilder()
		// Month is 0 based, just add 1
		.append(day).append("-").append(month + 1).append("-")
		.append(year).append(" "));

		// set current date into datepicker
		dpResult.init(year, month, day, null);
	}

	private void addListenerOnButton() {

		mDateOfBirthButton = (Button) findViewById(R.id.dobButton);
		mDateOfBirthButton.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				showDialog(DATE_DIALOG_ID);
			}
		});
	}

	@SuppressLint("NewApi")
	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case DATE_DIALOG_ID:
			// set date picker as current date
			// return new DatePickerDialog(this, datePickerListener, year,
			// month,day);
			DatePickerDialog datePickerDialog = new DatePickerDialog(this,
					R.style.MyTheme, datePickerListenerDOB, year, month, day){
				@Override
				public void onDateChanged(DatePicker view, int year,
						int monthOfYear, int dayOfMonth) {

					if (year > globalpYear)
						view.updateDate(globalpYear, month, day);

					if (monthOfYear > globalpMonth && year == globalpYear)
						view.updateDate(globalpYear, globalpMonth, day);
					if (dayOfMonth > globalpDay && year == globalpYear
							&& monthOfYear == globalpMonth)
						view.updateDate(globalpYear, globalpMonth, globalpDay);
				}

			};
			int currentapiVersion = android.os.Build.VERSION.SDK_INT;
			if (currentapiVersion >= 11) {
				DatePicker datePicker = datePickerDialog.getDatePicker();
				datePicker.setCalendarViewShown(false);
			}

			return datePickerDialog;
		}
		return null;
	}


	DatePickerDialog.OnDateSetListener datePickerListenerDOB = new DatePickerDialog.OnDateSetListener() {

		// when dialog box is closed, below method will be called.
		public void onDateSet(DatePicker view, int selectedYear,
				int selectedMonth, int selectedDay) {

			year = selectedYear;
			month = selectedMonth;
			day = selectedDay;

			// set selected date into textview
			mDateOfBirthButton.setText(new StringBuilder().append(day)
					.append("-").append(month + 1).append("-").append(year)
					.append(" "));

			// set selected date into datepicker also
			dpResult.init(year, month, day, null);
		}
	};

	public class JoinBJP extends AsyncTask<String, Integer, String>{
		boolean status=false;
		protected void onPreExecute() {
			Log.e("Pre","KK");
			dlgProgress = new ProgressDialog(JoinBJPFormActivity.this);
			dlgProgress.setMessage("Please wait");
			dlgProgress.setCancelable(false);
			dlgProgress.show();
		}

		protected void onPostExecute(String result) {
			// Getting adapter of the listview
			Log.e("post","KK");
			dlgProgress.dismiss();

			if(status){
				Intent toVision= new Intent(JoinBJPFormActivity.this, MainActivity1.class);
				toVision.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(toVision);
				
				Toast.makeText(JoinBJPFormActivity.this,"Thanks For Your Interest",Toast.LENGTH_SHORT).show();
			}
		}
		@Override
		protected String doInBackground(String... aurl) {
			Log.e("background","KK");
			try {
				status = GetOptions();
				return null;

			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}
	}

	public boolean GetOptions() {
		// TODO Auto-generated method stub
		//Bitmap decodedByte = null;
		boolean isList=false;
		HttpClient httpclient = new DefaultHttpClient();
		HttpPost httppost = new HttpPost(
				"http://www.smartcloudinfo.com/BJPConnect/index.php/logs/joinBJP");
		try {
			// Add your data

			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
			nameValuePairs.add(new BasicNameValuePair("name", mNameEditText.getText().toString().trim() ));
			nameValuePairs.add(new BasicNameValuePair("gender",radioSexString.toString().trim()  ));
			nameValuePairs.add(new BasicNameValuePair("address",mAddressEditText.getText().toString().trim()));
			nameValuePairs.add(new BasicNameValuePair("pincode", mPincodeEditText.getText().toString().trim()));
			nameValuePairs.add(new BasicNameValuePair("date_of_birth", mDateOfBirthButton.getText().toString().trim() ));
			nameValuePairs.add(new BasicNameValuePair("profession", mProfessionSpinner.getSelectedItem().toString().trim() ));
			nameValuePairs.add(new BasicNameValuePair("telephone", mTelephoneEditText.getText().toString().trim() ));
			nameValuePairs.add(new BasicNameValuePair("mobile", mMobileEditText.getText().toString().trim() ));
			nameValuePairs.add(new BasicNameValuePair("email", mEmailEditText.getText().toString().trim() ));

			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

			// Execute HTTP Post Request
			HttpResponse response = httpclient.execute(httppost);

			HttpEntity entity = response.getEntity();

			InputStream inStream = entity.getContent();
			String jsonresponse = Utils.convertStreamToString(inStream);
			Log.e("result==voting qs", jsonresponse);
			JSONObject obj1 = null;

			try {
				obj1 = new JSONObject(jsonresponse);

			} catch (JSONException e) {

				e.printStackTrace();
			}
			try {
				String status = obj1.getString("status");
				if (status.equals("false")) {
					isList=false;
				} else {
					isList=true;
					/*JSONArray medias= obj1.getJSONArray("aclist");
					mACArrayList = new ArrayList<String>();
					for(int i=0;i<medias.length();i++)
					{
						String ac=medias.getString(i);
						//JSONObject item= medias.getJSONObject(i);
						//filesMapMedia.put(item.getString("title"), item.getString("image"));
						Log.e("ac", ac);
						mACArrayList.add(ac);
					}*/
				}

			} catch (JSONException e) {
				e.printStackTrace();
			}
		} catch (ClientProtocolException ex) {
			ex.printStackTrace();
		} catch (IOException ez) {
			ez.printStackTrace();
		}
		return isList;
	}
}