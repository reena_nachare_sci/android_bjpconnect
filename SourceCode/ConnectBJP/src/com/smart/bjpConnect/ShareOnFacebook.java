//package com.smart.bjpConnect;
//
//import java.security.MessageDigest;
//import java.security.NoSuchAlgorithmException;
//import java.util.Arrays;
//import java.util.List;
//
//import com.facebook.model.*;
//import com.facebook.widget.*;
//
//import com.facebook.FacebookAuthorizationException;
//import com.facebook.FacebookOperationCanceledException;
//import com.facebook.FacebookRequestError;
//import com.facebook.HttpMethod;
//import com.facebook.Request;
//import com.facebook.RequestAsyncTask;
//import com.facebook.Response;
//import com.facebook.Session;
//import com.facebook.SessionState;
//import com.facebook.UiLifecycleHelper;
//
//import android.app.Activity;
//import android.app.AlertDialog;
//
//import android.content.Context;
//import android.content.Intent;
//import android.content.pm.PackageInfo;
//import android.content.pm.PackageManager;
//import android.content.pm.PackageManager.NameNotFoundException;
//import android.content.pm.Signature;
//import android.os.Bundle;
//
//import android.util.Base64;
//import android.util.Log;
//import android.view.View;
//import android.widget.Button;
//import android.widget.TextView;
//import android.widget.Toast;
//
//
//public class ShareOnFacebook extends Activity{
//
//
//	public static final String ASTRO_ICON_URL = "http://smartcloudinfo.com/Pepsico/application/images/pep_help_logo.png";
//
//    private static final List<String> PERMISSIONS = Arrays.asList("publish_actions");
//
//
//    private final String PENDING_ACTION_BUNDLE_KEY = "com.smart.pep_help.facebook:PendingRequest";
//
//    private Button postStatusUpdateButton;
//    private LoginButton loginButton;
//    private ProfilePictureView profilePictureView;
//    private TextView greeting;
//    private PendingAction pendingAction = PendingAction.NONE;
//    private GraphUser user;
//    /**
//	 * Package Name
//	 */
//	static String packegeName = "com.smart.bjpConnect";
//
//    private enum PendingAction {
//        NONE,
//        POST_STATUS_UPDATE
//    }
//    private UiLifecycleHelper uiHelper;
//
//    private Session.StatusCallback callback = new Session.StatusCallback() {
//        @Override
//        public void call(Session session, SessionState state, Exception exception) {
//            onSessionStateChange(session, state, exception);
//        }
//    };
//
//	@Override
//    public void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//  	  
//
//        uiHelper = new UiLifecycleHelper(this, callback);
//        uiHelper.onCreate(savedInstanceState);
//
//        if (savedInstanceState != null) {
//            String name = savedInstanceState.getString(PENDING_ACTION_BUNDLE_KEY);
//            pendingAction = PendingAction.valueOf(name);
//        }
//
//        setContentView(R.layout.facebook_dialog);
//
//        PackageInfo info;
//		try {
//			info = getPackageManager().getPackageInfo(packegeName,
//					PackageManager.GET_SIGNATURES);
//			for (Signature signature : info.signatures) {
//				MessageDigest md;
//				md = MessageDigest.getInstance("SHA");
//				md.update(signature.toByteArray());
//				String something = new String(Base64.encode(md.digest(), 0));
//
//				Log.e("", "hash key " + something);
//
//			}
//		} catch (NameNotFoundException e1) {
//			Log.e("name not found", e1.toString());
//		} catch (NoSuchAlgorithmException e) {
//			Log.e("no such an algorithm", e.toString());
//		} catch (Exception e) {
//			Log.e("exception", e.toString());
//		}
//
//        
//        
//        getIntent().getExtras().getString("location");
//        loginButton = (LoginButton) findViewById(R.id.login_button);
//        loginButton.setUserInfoChangedCallback(new LoginButton.UserInfoChangedCallback() {
//            @Override
//            public void onUserInfoFetched(GraphUser user) {
//                ShareOnFacebook.this.user = user;
//                Log.e("User info changed--","KKKKKK");
//                updateUI();
//                // It's possible that we were waiting for this.user to be populated in order to post a
//                // status update.
//                handlePendingAction();
//                if(user!=null){
//                	postStatusUpdate();
//                }
//            }
//        });
//
//        profilePictureView = (ProfilePictureView) findViewById(R.id.profilePicture);
//        greeting = (TextView) findViewById(R.id.greeting);
//
//        postStatusUpdateButton = (Button) findViewById(R.id.postStatusUpdateButton);
//        postStatusUpdateButton.setOnClickListener(new View.OnClickListener() {
//            public void onClick(View view) {
//                onClickPostStatusUpdate();
//            }
//        });
//
//
//    }
//
//    @Override
//    protected void onResume() {
//        super.onResume();
//        uiHelper.onResume();
//
//        updateUI();
//    }
//
//    @Override
//    protected void onSaveInstanceState(Bundle outState) {
//        super.onSaveInstanceState(outState);
//        uiHelper.onSaveInstanceState(outState);
//
//        outState.putString(PENDING_ACTION_BUNDLE_KEY, pendingAction.name());
//    }
//
//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        uiHelper.onActivityResult(requestCode, resultCode, data);
//    }
//
//    @Override
//    public void onPause() {
//        super.onPause();
//        uiHelper.onPause();
//    }
//
//    @Override
//    public void onDestroy() {
//        super.onDestroy();
//        uiHelper.onDestroy();
//    }
//
//    private void onSessionStateChange(Session session, SessionState state, Exception exception) {
//        if (pendingAction != PendingAction.NONE &&
//                (exception instanceof FacebookOperationCanceledException ||
//                exception instanceof FacebookAuthorizationException)) {
//                new AlertDialog.Builder(ShareOnFacebook.this)
//                    .setTitle(R.string.cancelled)
//                    .setMessage(R.string.permission_not_granted)
//                    .setPositiveButton(R.string.ok, null)
//                    .show();
//            pendingAction = PendingAction.NONE;
//        } else if (state == SessionState.OPENED_TOKEN_UPDATED) {
//            handlePendingAction();
//        }
//        updateUI();
//    }
//
//    private void updateUI() {
//    	Log.e("In Update UI","KKKK");
//        Session session = Session.getActiveSession();
//        boolean enableButtons = (session != null && session.isOpened());
//        Log.e("enable buttons--"+enableButtons,"KKKK");
//        postStatusUpdateButton.setEnabled(enableButtons);
//        if (enableButtons && user != null) {
//            profilePictureView.setProfileId(user.getId());
//            greeting.setText(getString(R.string.hello_user, user.getFirstName()));
//        } else {
//            profilePictureView.setProfileId(null);
//            greeting.setText(null);
//        }
//    }
//
//    private void handlePendingAction() {
//        PendingAction previouslyPendingAction = pendingAction;
//        // These actions may re-set pendingAction if they are still pending, but we assume they
//        // will succeed.
//        pendingAction = PendingAction.NONE;
//
//        switch (previouslyPendingAction) {
//            case POST_STATUS_UPDATE:
//                postStatusUpdate();
//                break;
//        }
//    }
//
//
//    private void onClickPostStatusUpdate() {
//        performPublish(PendingAction.POST_STATUS_UPDATE);
//    }
//
//
//	/**
//	 * Post the status 
//	 * */
//    private void postStatusUpdate() {
//
//
//        if (user != null && hasPublishPermission()) {
//
//            Bundle postParams = new Bundle();
//            String postText = "BJP India: http://www.bjp.org/";
//          /*  
//            if(userLocation.length()<15){*/
//           // }
//
//            postParams.putString("message", postText);
//        //    postParams.putString("name", "Checkout the Pep Help App!");
//            postParams.putString("caption", "BJP");
//            
//            //postParams.putString("link", "http://www.getwemeet.com/");
//          //  postParams.putString("picture", ASTRO_ICON_URL);
//
//            
//            final Context context = this;
//            Request.Callback callback = new Request.Callback() {
//                public void onCompleted(Response response) {
//                    FacebookRequestError error = response.getError();
//                    if (error != null) {
//                        Toast.makeText(context, error.getErrorMessage(), Toast.LENGTH_LONG).show();
//                        finish();
//                    } else {
//                        Toast.makeText(context, "Successfully posted on your facebook wall!", 
//                        		Toast.LENGTH_LONG).show();
//                        finish();
//                    }
//                }
//            };
//
//            Request request = new Request(Session.getActiveSession(), "me/feed", postParams, HttpMethod.POST, callback);
//            RequestAsyncTask task = new RequestAsyncTask(request);
//            task.execute();
//           
//        } else {
//            pendingAction = PendingAction.POST_STATUS_UPDATE;
//        }
//    }
//
//
//    private boolean hasPublishPermission() {
//        Session session = Session.getActiveSession();
//        return session != null && session.getPermissions().contains("publish_actions");
//    }
//
//    private void performPublish(PendingAction action) {
//        Session session = Session.getActiveSession();
//        if (session != null) {
//            pendingAction = action;
//            if (hasPublishPermission()) {
//                // We can do the action right away.
//                handlePendingAction();
//            } else {
//                // We need to get new permissions, then complete the action when we get called back.
//                session.requestNewPublishPermissions(new Session.NewPermissionsRequest(this, PERMISSIONS));
//            }
//        }
//    }
//
//}//end of class
