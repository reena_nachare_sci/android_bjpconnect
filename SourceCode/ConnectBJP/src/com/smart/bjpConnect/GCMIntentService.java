/*
 * Copyright 2012 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.smart.bjpConnect;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.google.android.gcm.GCMBaseIntentService;
import com.google.android.gcm.GCMRegistrar;
import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import com.smart.bjpConnect.R;


import static com.smart.bjpConnect.Utils.displayMessage;
import static com.smart.bjpConnect.Utils.displayRecievedMessage;
import static com.smart.bjpConnect.Utils.SENDER_ID;

/**
 * Class that handles the GCM registration,sending and getting of notifications
 * @author ketkim
 *
 */
public class GCMIntentService extends GCMBaseIntentService {
	/**
	 * Application shared preferences name
	 */
	private static final String PREFS_NAME = "BjpConnectPrefs";
	/**
	 * User Id
	 */
	private String mId;
	/**
	 * Object to access the application shared preferences
	 */
	SharedPreferences mSp;
	SharedPreferences.Editor mSpEditor;


	/**
	 * Constructor of GCM Intent service class
	 */
	public GCMIntentService() {
		super(SENDER_ID);
	}

	@Override
	protected void onRegistered(Context context, String registrationId) {

		displayMessage(context, getString(R.string.gcm_registered));
		Log.e("GCM-OnREGISTERED----"+registrationId,"KKKK");
		//get the userid from shared preferences
		mSp = getSharedPreferences(PREFS_NAME, 0);
		mId= mSp.getString("UserId", "");
		mSpEditor= mSp.edit();
		mSpEditor.putString("DeviceToken", registrationId);
		mSpEditor.commit();

		//register the device on GCM
		ServerUtilities.register(context, registrationId,mId);
	}

	@Override
	protected void onUnregistered(Context context, String registrationId) {
		displayMessage(context, getString(R.string.gcm_unregistered));
		if (GCMRegistrar.isRegisteredOnServer(context)) {
			mSp = getSharedPreferences(PREFS_NAME, 0);
			String mId= mSp.getString("UserId", "");
			String deviceToken= mSp.getString("DeviceToken", "");
			ServerUtilities.unregister(context, mId,deviceToken);
		} 
	}

	/**Handle the notification incoming message
	 * 
	 */
	@SuppressLint("SimpleDateFormat")
	@Override
	protected void onMessage(Context context, Intent intent) {
		//String message = getString(R.string.gcm_message);
		String gcm_message= intent.getExtras().getString("message");
		Log.e("OnMessage-- notify"+gcm_message,"KKKKKK");
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		//get current date time with Date()
		Date date = new Date();
		System.out.println(dateFormat.format(date));
		gcm_message= "Dated:"+date+" - "+gcm_message;
		Connect.isNotification=true;
		mSp = getSharedPreferences(PREFS_NAME, 0);
		mSpEditor= mSp.edit();
		mSpEditor.putString("Message", gcm_message);
		mSpEditor.commit();

		String notSetting= mSp.getString("ISNOTIFICATION", "ON");

		Log.e("notification settng", notSetting);
		
		displayRecievedMessage(context, gcm_message);
		
		if(notSetting.equals("ON")){
			// notifies user with vibration and sound
			generateNotification(context, gcm_message);
		}
		/*String senderName=intent.getExtras().getString("sender");
		String status=intent.getExtras().getString("status");
		Log.e("status-- notify"+status,"KKKKKK");
		//If user is notified to logout then send a broadcast message to other activities to finish
		if(status.equals("logout"))
		{

			Intent broadcastIntent = new Intent();
			broadcastIntent.setAction("com.package.ACTION_LOGOUT");
			sendBroadcast(broadcastIntent);
			String deviceToken= mSp.getString("DeviceToken", "");
			 ServerUtilities.unregister(context, mId,deviceToken);

			 //clear the active session of facebook
			try{
				  Session session = Session.getActiveSession();
			         // run the closeAndClearTokenInformation which does the following
			         // DOCS : Closes the local in-memory Session object and clears any persistent 
			         // cache related to the Session.

			         session.closeAndClearTokenInformation();
				}
				catch(Exception e)
				{

				}
			message="You are logged out from your WeMeet session!";

		}
		else if(status.equals("sent")){
        	  message=senderName+" sent you WeMeet request";	 
        		Log.e("GCMIntentService",senderName);
        		displayRecievedMessage(context, message);

        		// notifies user with vibration and sound
        		generateNotification(context, message);
          }
          else if(status.equals("accept")){

        	  message=senderName+" accepted your WeMeet request";
        	  displayRecievedMessage(context, message);

      		// notifies user with vibration and sound
      		generateNotification(context, message);
          }*/
	}


	@Override
	protected void onDeletedMessages(Context context, int total) {
		String message = getString(R.string.gcm_deleted, total);
		displayMessage(context, message);
		// notifies user with vibration and sound
		generateNotification(context, message);
	}

	@Override
	public void onError(Context context, String errorId) {
		displayMessage(context, getString(R.string.gcm_error, errorId));
	}

	@Override
	protected boolean onRecoverableError(Context context, String errorId) {
		displayMessage(context,
				getString(R.string.gcm_recoverable_error, errorId));
		return super.onRecoverableError(context, errorId);
	}

	/**
	 * Issues a notification to inform the user that server has sent a message.
	 */
	@SuppressWarnings("deprecation")
	private void generateNotification(Context context, String message) {
		int icon = R.drawable.ic_launcher;
		long when = System.currentTimeMillis();
		NotificationManager notificationManager = (NotificationManager) context
				.getSystemService(Context.NOTIFICATION_SERVICE);
		Notification notification = new Notification(icon, message, when);
		String title = getResources().getString(R.string.app_name);
		Intent notificationIntent = new Intent(context, NotificationActivity.class);
		// set intent so it does not start a new activity
		notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
				| Intent.FLAG_ACTIVITY_SINGLE_TOP);
		PendingIntent intent = PendingIntent.getActivity(context, 0,
				notificationIntent, 0);
		notification.setLatestEventInfo(context, title, message, intent);
		notification.flags |= Notification.FLAG_AUTO_CANCEL;
		notificationManager.notify(0, notification);
	}
}