package com.smart.bjpConnect;

import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings.Secure;
import android.util.Log;
import android.webkit.WebView;
import android.widget.TextView;
import android.widget.Toast;

public class SurveyResultActivty extends ActinBarActivity{

	private ConnectionDetector cd;
	private ProgressDialog dlgProgress;
	private String android_id;
	private String option_yes,option_no;
	private static AlertDialogManager alert= new AlertDialogManager();
	WebView mResult;
	private Context mContext;
	private String id;

	private TextView mSurveyTextView; 

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_survey_result);
		setupActionBar();
		TextView title = (TextView) actionBarLayout.findViewById(R.id.txt_title);
		title.setText("RESULT");

		mContext= this;
		cd= new ConnectionDetector(SurveyResultActivty.this);
		dlgProgress= new ProgressDialog(SurveyResultActivty.this);

		if(getIntent().getExtras()!=null)
		{
			Log.e("Got intent","KK");
			id= getIntent().getExtras().getString("id");
			//}
		}
		android_id = Secure.getString(SurveyResultActivty.this.getContentResolver(),
				Secure.ANDROID_ID); 
		Log.e("android device id--"+android_id,"KKK");
		mResult= (WebView)findViewById(R.id.survey_result);

		mSurveyTextView = (TextView) findViewById(R.id.surveyResult);

		mResult.getSettings().setJavaScriptEnabled(true);
		if(cd.isConnectingToInternet())
		{
			GetSurveyResult facts= new GetSurveyResult();
			facts.execute();
		}
		else{
			alert.showAlertDialog(mContext, "Error", "Please connect to working Internet connection!", false);
		}

	}

	/** AsyncTask to download and load an image in ListView */
	private class GetSurveyResult extends AsyncTask<String, Void, String> {
		boolean status=false;
		@Override
		protected void onPreExecute() {
			Log.e("Pre","KK");
			dlgProgress.setMessage("Please wait");
			dlgProgress.setCancelable(false);
			dlgProgress.show();
		}

		@Override
		protected void onPostExecute(String result) {
			// Getting adapter of the listview
			Log.e("post","KK");
			dlgProgress.dismiss();
			//if(status){
			//String url =  "http://chart.apis.google.com/chart?cht=p3&chs=500x200&chd=e:TNTNTNGa&chts=000000,16&chtt=A+Better+Web&chl=Hello|Hi|anas|Explorer&chco=FF5533,237745,9011D3,335423&chdl=Apple|Mozilla|Google|Microsoft";
			//String url =  "http://chart.apis.google.com/chart?cht=p&chd=t:"+option_yes+","+option_no+"&chs=300x180&chl=YES|NO";

			double cant = 100 - ( RoundTo2Decimals(Double.parseDouble(option_yes)) +  RoundTo2Decimals(Double.parseDouble(option_no)));

			String cantString = String.valueOf(RoundTo2Decimals(cant)); 

			String url =  "http://chart.apis.google.com/chart?cht=p&chd=t:"+cantString+","+option_yes+","+option_no+"&chs=300x180&chl=YES|CAN'T SAY|NO";
			mResult.loadUrl(url);

			//Toast.makeText(mContext, "Yes % "+option_yes + "\n" + " No % "+option_no + "\n" + " Can't Say % " + cantString  , Toast.LENGTH_LONG).show();

			/*if(option_yes.length() > 3){
				String yes = option_yes.substring(0, 3);
				String no = option_no.substring(0, 3);
				String cantsy = cantString.substring(0, 3);

				mSurveyTextView.setText("Yes % "+yes + "\n" + " No % "+no + "\n" + " Can't Say % " + cantsy);
			}*/
			
			mSurveyTextView.setText("Yes - "+RoundTo2Decimals(Double.parseDouble(option_yes))+" %" + "\n" + " No - "+RoundTo2Decimals(Double.parseDouble(option_no))+" %" + "\n" + " Can't Say - " + cantString +" %");

			//}
		}

		double RoundTo2Decimals(double val) {
			DecimalFormat df2 = new DecimalFormat("###.##");
			return Double.valueOf(df2.format(val));
		}

		@Override
		protected String doInBackground(String... params) {
			Log.e("background","KK");
			try {
				status=getSurveyResult();
				return null;

			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

	}

	public boolean getSurveyResult() {
		// TODO Auto-generated method stub
		Bitmap decodedByte = null;
		boolean isList=false;
		HttpClient httpclient = new DefaultHttpClient();
		HttpPost httppost = new HttpPost(
				"http://www.smartcloudinfo.com/BJPConnect/index.php/polldatas/getPollDataAnalytics_REST");

		try {
			// Add your data

			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
			nameValuePairs.add(new BasicNameValuePair("device_id", android_id));

			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

			// Execute HTTP Post Request
			HttpResponse response = httpclient.execute(httppost);

			HttpEntity entity = response.getEntity();

			InputStream inStream = entity.getContent();
			String jsonresponse = Utils.convertStreamToString(inStream);
			Log.e("result==survey qs", jsonresponse);
			JSONObject obj1 = null;

			try {
				obj1 = new JSONObject(jsonresponse);

			} catch (JSONException e) {

				e.printStackTrace();
			}
			try {
				String status = obj1.getString("status");
				if (status.equals("false")) {
					isList=false;
				} else {
					isList=true;
					JSONArray result= obj1.getJSONArray("poll_analytics");
					for(int i=0;i<result.length();i++)
					{
						JSONObject item= result.getJSONObject(i);
						if(item.getString("id").equals(id)){
							option_yes = item.getString("option_1_overall");
							option_no = item.getString("option_2_overall");

							Log.e("Got---"+option_yes,"KKK-"+option_no);
							break;
						}
					}
				}

			} catch (JSONException e) {

			}
		} catch (ClientProtocolException ex) {

		} catch (IOException ez) {

		}
		return isList;
	}
}
