package com.smart.bjpConnect;

import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

public class SocialActivity extends ActinBarActivity{
	private ImageButton btn_social_facebook;
	private ImageButton btn_social_twitter;
	private ImageButton btn_social_google_plus;
	private ImageButton btn_social_youtube;
	private ImageButton btn_social_blogs;
	
	Intent displayURL;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_social);
		DisplayMetrics displaymetrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
		//int height = displaymetrics.heightPixels;
		int width = displaymetrics.widthPixels;
		if(width>480)
		{
			setContentView(R.layout.activity_social1);
		}
		setupActionBar();
		TextView title = (TextView) actionBarLayout.findViewById(R.id.txt_title);
		title.setText("SOCIAL");
		initialize();
		displayURL=new Intent(SocialActivity.this, DisplaySites.class);
	}
	
	private void initialize() {
		// TODO Auto-generated method stub
		btn_social_facebook=(ImageButton)findViewById(R.id.btn_social_facebook);
		btn_social_facebook.setOnClickListener(this);
		btn_social_twitter=(ImageButton)findViewById(R.id.btn_social_twitter);
		btn_social_twitter.setOnClickListener(this);
		btn_social_google_plus=(ImageButton)findViewById(R.id.btn_social_google_plus);
		btn_social_google_plus.setOnClickListener(this);
		
		btn_social_youtube=(ImageButton)findViewById(R.id.btn_social_youtube);
		btn_social_youtube.setOnClickListener(this);
		
		btn_social_blogs=(ImageButton)findViewById(R.id.btn_social_blog);
		btn_social_blogs.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btn_social_facebook:
			displayURL.putExtra("display", "facebook");
			displayURL.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(displayURL);
			break;
		case R.id.btn_social_twitter:
			displayURL.putExtra("display", "twitter");
			displayURL.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(displayURL);
			break;
		case R.id.btn_social_google_plus:
			displayURL.putExtra("display", "google");
			displayURL.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(displayURL);
			break;
		case R.id.btn_social_youtube:
			displayURL.putExtra("display", "youtube");
			displayURL.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(displayURL);
			break;
		case R.id.btn_social_blog:
			displayURL.putExtra("display", "blogs");
			displayURL.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(displayURL);
			break;

		default:
			break;
		}
		super.onClick(v);
	}
	
	@Override
	public void onBackPressed() {
		super.onBackPressed();
	/*
		Intent toAdd= new Intent(SocialActivity.this,MainActivity.class);
		startActivity(toAdd);
		finish();*/
	}
}
