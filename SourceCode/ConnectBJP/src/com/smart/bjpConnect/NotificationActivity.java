package com.smart.bjpConnect;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

public class NotificationActivity extends ActinBarActivity{
	private static final String URLPrivateSubmitAnswer = "http://www.smartcloudinfo.com/BJPConnect/index.php/notifications/getNotifications_REST";
	/**
	 * to check internet connection
	 */
	private ConnectivityManager mConnectivityManager;
	private NetworkInfo mNetworkInfo;
	private boolean isNetError = false;
	ListView listview;
	ArrayList<String> notificationList;
	ArrayList<String> notificationDetail;
	TextView no_notifn_txt;
	Button btn_notfnSettingON;
	Button btn_notfnSettingOFF;
	/**
	 * Application shared preferences name
	 */
	private static final String PREFS_NAME = "BjpConnectPrefs";
	/**
	/**
	 * Object to access the application shared preferences
	 */
	SharedPreferences mSp;
	SharedPreferences.Editor mSpEditor;
	
	AlertDialogManager alert = new AlertDialogManager();
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		Log.e("ONRESUME--","KKKK");
		if(Connect.isNotification)
		{
			DisplayNotification();
			Connect.isNotification=false;
		}
		super.onResume();
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_notification);
		mConnectivityManager = (ConnectivityManager)
				getSystemService(Context.CONNECTIVITY_SERVICE);
		mNetworkInfo = mConnectivityManager.getActiveNetworkInfo();
		no_notifn_txt=(TextView) findViewById(R.id.no_notifications_txt);
		listview=(ListView) findViewById(R.id.lst_notifications);
		
		

		setupActionBar();
		TextView title = (TextView) actionBarLayout.findViewById(R.id.txt_title);
		title.setText("NOTIFICATIONS");

		mSp = getSharedPreferences(PREFS_NAME, 0);

		mSpEditor= mSp.edit();


		btn_notfnSettingON=(Button) findViewById(R.id.btn_notifiactionSettingON);
		btn_notfnSettingOFF=(Button) findViewById(R.id.btn_notifiactionSettingOFF);

		btn_notfnSettingON.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				mSpEditor.putString("ISNOTIFICATION", "OFF");
				btn_notfnSettingON.setVisibility(View.INVISIBLE);
				btn_notfnSettingOFF.setVisibility(View.VISIBLE);

				mSpEditor.commit();
			}
		});


		btn_notfnSettingOFF.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				mSpEditor.putString("ISNOTIFICATION", "ON");
				btn_notfnSettingOFF.setVisibility(View.INVISIBLE);
				btn_notfnSettingON.setVisibility(View.VISIBLE);

				mSpEditor.commit();

			}
		});



		notificationList=new ArrayList<String>();
		notificationDetail=new ArrayList<String>();
		getNotifications();


	}
	
	protected void DisplayNotification() {
		// TODO Auto-generated method stub
		String message= mSp.getString("Message", "");
		if(message.equals("")){
			alert.showAlertDialog(
					NotificationActivity.this,
					"Notification",
					"You currently don't have any notifications!",
					false);
		}
		else{
			AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
					this);

			// set title
			alertDialogBuilder.setTitle("You have a notificaton!");

			// set dialog message
			alertDialogBuilder
			.setMessage(message)
			.setCancelable(false)
			.setPositiveButton("Ok",new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {
					// if this button is clicked, close
					// current activity
					dialog.cancel();
				}
			});

			// create alert dialog
			AlertDialog alertDialog = alertDialogBuilder.create();

			// show it
			alertDialog.show();
		}

	}
	
	private class StableArrayAdapter extends ArrayAdapter<String> {

		HashMap<String, Integer> mIdMap = new HashMap<String, Integer>();

		public StableArrayAdapter(Context context, int textViewResourceId,
				List<String> objects) {
			super(context, textViewResourceId, objects);
			for (int i = 0; i < objects.size(); ++i) {
				mIdMap.put(objects.get(i), i);
			}
		}

		@Override
		public long getItemId(int position) {
			String item = getItem(position);
			return mIdMap.get(item);
		}

		@Override
		public boolean hasStableIds() {
			return true;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View view = convertView;
			if (view == null) {
				Log.e("View is null","KKK");
				LayoutInflater vi = (LayoutInflater)NotificationActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				view = vi.inflate(R.layout.questions_list_row, null);
			}
			Log.e("getView--"+position,"KK");
			TextView txt_question= (TextView)view.findViewById(R.id.txt_question_name);
			txt_question.setText(notificationList.get(position));
			return view;
		}

	}

	private void getNotifications() {
		GetNotificationAsynch getN=new GetNotificationAsynch();
		getN.execute();

	}
	/**
	 * submit answer AsyncTask to submit answer to server which are added offline
	 */
	private class GetNotificationAsynch extends
	AsyncTask<String, Void, Void> {
		boolean status=false;


		ProgressDialog dlg;

		@Override
		protected void onPreExecute() {
			dlg = new ProgressDialog(NotificationActivity.this);
			dlg.setCancelable(true);
			dlg.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			dlg.setMessage("Downloading Notifications...");
			dlg.show();
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(String... params) {
			isNetError = false;

			if (mNetworkInfo != null && mNetworkInfo.isConnectedOrConnecting()) {

				status=HttpNotification();

			} else {
				isNetError = true;
			}
			return null;

		}

		@Override
		protected void onPostExecute(Void result) {
			if(dlg.isShowing()){
				dlg.dismiss();
			}
			if (isNetError) {

			} else {
				if(!status){
					Log.e("status is false", status+"");
					listview.setVisibility(View.GONE);
					no_notifn_txt.setVisibility(View.VISIBLE);
					no_notifn_txt.setText("No Notifications");
				}
				else{
					StableArrayAdapter adapter = new StableArrayAdapter(NotificationActivity.this,
							android.R.layout.simple_list_item_1, notificationList);
					listview.setAdapter(adapter);
					listview.setOnItemClickListener(new OnItemClickListener() {

						@Override
						public void onItemClick(AdapterView<?> parent,
								View v, int position, long arg3) {
							AlertDialogManager al=new AlertDialogManager();
							al.showAlertDialog(NotificationActivity.this, notificationList.get(position), notificationDetail.get(position), false);

						}
					});
				}

			}
			super.onPostExecute(result);
		}
	}// end of AsyncPostData

	/**
	 * submit all the answers to server
	 */
	public boolean HttpNotification() {

		HttpClient httpclient = new DefaultHttpClient();
		HttpPost httppost = new HttpPost(URLPrivateSubmitAnswer);
		boolean status=false;

		try {
			// Add your data
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(
					2);

			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

			// Execute HTTP Post Request
			HttpResponse response = httpclient.execute(httppost);

			HttpEntity entity = response.getEntity();

			InputStream inStream = entity.getContent();
			String jsonresponse =Utils.convertStreamToString(inStream);
			Log.e("res", jsonresponse);
			try {
				JSONObject jResponse = new JSONObject(jsonresponse);
				String status1=jResponse.getString("status");
				if(status1.equals("true")){
					JSONArray noteArray=jResponse.getJSONArray("notifications");
					notificationList.clear();
					for (int i = 0; i < noteArray.length(); i++) {
						JSONObject obj=noteArray.getJSONObject(i);
						String title=obj.getString("title");
						notificationList.add(title);

						String detail=obj.getString("text");
						if(detail.equals("")){
							detail="No description";
						}

						notificationDetail.add(detail);
					}
					status=true;
				}
				else{
					status=false;
				}	
			} catch (Exception e) {
				e.printStackTrace();
				status=false;
			}

		} catch (ClientProtocolException ex) {
			status=false;
			ex.printStackTrace();
			//Log.e(TAG, ex.toString());
		} catch (IOException ez) {
			ez.printStackTrace();
			status=false;
			//Log.e(TAG, ez.toString());
		}
		return status;
	}
	
	
}