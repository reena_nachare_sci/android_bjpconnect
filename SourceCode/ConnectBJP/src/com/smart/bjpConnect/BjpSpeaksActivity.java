package com.smart.bjpConnect;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings.Secure;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.origamilabs.library.views.StaggeredGridView;

import com.smart.bjpConnect.flipboard.loader.ImageLoader;
import com.smart.bjpConnect.flipboard.view.ScaleImageView;
import com.smart.bjpConnect.quickaction.ActionItem;
import com.smart.bjpConnect.quickaction.QuickAction;

public class BjpSpeaksActivity extends ActinBarActivity{
	private ProgressDialog dlgProgress;
	private ArrayList<String> title;
	private ArrayList<String> image_urls;
	private ArrayList<String> feed_ids;
	private ArrayList<String> link_Array;

	//private QuickAction quickAction;
	//ImageButton credit;

	// action id
	//private static final int ID_SHARE = 2;
	//private static final int ID_LOGIN = 1;
	//private static final int ID_NOTIFY = 3;
	//private static final int ID_CALL = 5;

	static String linkSelected="";

	static String textTitle="";
	String display="";

	private String urls[] = { 
			"http://farm9.staticflickr.com/8335/8144074340_38a4c622ab.jpg",
	};

	private String android_id;
	private ConnectionDetector cd;
	StaggeredGridView gridView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_polls);
		setupActionBar();

		/*credit = (ImageButton) findViewById(R.id.credit1);
		credit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent toCreditAct = new Intent(BjpSpeaksActivity.this,
						CreditsActivity.class);
				startActivity(toCreditAct);
			}
		});*/

		//setupMenuOptions();

		android_id = Secure.getString(BjpSpeaksActivity.this.getContentResolver(),
				Secure.ANDROID_ID); 
		Log.e("android device id--"+android_id,"KKK");
		cd= new ConnectionDetector(BjpSpeaksActivity.this);
		dlgProgress= new ProgressDialog(BjpSpeaksActivity.this);
		title= new ArrayList<String>();
		image_urls= new ArrayList<String>();
		feed_ids= new ArrayList<String>();
		link_Array= new ArrayList<String>();

		TextView title = (TextView) actionBarLayout.findViewById(R.id.txt_title);
		title.setText("BJP SPEAKS");
		gridView = (StaggeredGridView) this.findViewById(R.id.staggeredGridView1);


		int margin = getResources().getDimensionPixelSize(R.dimen.margin);

		gridView.setItemMargin(margin); // set the GridView margin

		gridView.setPadding(margin, 0, margin, 0); // have the margin on the sides as well 

		if(cd.isConnectingToInternet()){
			GetVotingQs qs= new GetVotingQs();
			qs.execute();
		}
		else{
			Toast.makeText(BjpSpeaksActivity.this, "NO Internet", Toast.LENGTH_SHORT).show();	
			finish();

		}



		/*StaggeredAdapter adapter = new StaggeredAdapter(PollsActivity.this, R.id.imageView1, urls);

		gridView.setAdapter(adapter);
		adapter.notifyDataSetChanged();*/
	}

	/*private void setupMenuOptions() {
		// TODO Auto-generated method stub
		ActionItem nextItem= new ActionItem(ID_NOTIFY, "", getResources()
				.getDrawable(R.drawable.notification));
		ActionItem prevItem = new ActionItem(ID_LOGIN, "", getResources()
				.getDrawable(R.drawable.login_button));
		ActionItem searchItem = new ActionItem(ID_SHARE, "", getResources()
				.getDrawable(R.drawable.share_01));
		ActionItem callItem= new ActionItem(ID_CALL, "", getResources()
				.getDrawable(R.drawable.join_us_with_a_missed_call_01));
		ActionItem creditItem= new ActionItem(ID_CREDITS, "", getResources()
				.getDrawable(R.drawable.btn_credits));
		ActionItem creditItem= new ActionItem(ID_CREDITS, "", getResources()
				.getDrawable(R.drawable.credits_active));

		// use setSticky(true) to disable QuickAction dialog being dismissed
		// after an item is clicked
		prevItem.setSticky(true);
		nextItem.setSticky(true);
		callItem.setSticky(true);
		//creditItem.setSticky(true);

		// create QuickAction. Use QuickAction.VERTICAL or
		// QuickAction.HORIZONTAL param to define layout
		// orientation
		quickAction = new QuickAction(this,
				QuickAction.VERTICAL);

		// add action items into QuickAction
		quickAction.addActionItem(nextItem);
		quickAction.addActionItem(prevItem);
		quickAction.addActionItem(searchItem);
		quickAction.addActionItem(callItem);
		//quickAction.addActionItem(creditItem);

		// Set listener for action item clicked
		quickAction
		.setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {
			@Override
			public void onItemClick(QuickAction source, int pos,
					int actionId) {
				@SuppressWarnings("unused")
				ActionItem actionItem = quickAction.getActionItem(pos);

				// here we can filter which action item was clicked with
				// pos or actionId parameter
				if (actionId == ID_NOTIFY) {
					Intent toSettings = new Intent(BjpSpeaksActivity.this,
							NotificationActivity.class);
					startActivity(toSettings);

				} else if (actionId == ID_SHARE) {
				
					Intent toShare = new Intent(BjpSpeaksActivity.this,
							ShareButtonActivity.class);
					startActivity(toShare);
				} else if(actionId == ID_LOGIN){
					Intent toLoginAct = new Intent(BjpSpeaksActivity.this,
							CandidateLoginActivity.class);
					startActivity(toLoginAct);
				}
				else if(actionId == ID_CALL){
					Intent toLoginAct = new Intent(BjpSpeaksActivity.this,
							JoinWithCallActivity.class);
					startActivity(toLoginAct);
				}
				else if(actionId == ID_CREDITS){
					Intent tocreditAct = new Intent(MainActivity1.this,
							CreditsActivity.class);
					startActivity(toLoginAct);
				}
			}
		});

		// set listnener for on dismiss event, this listener will be called only
		// if QuickAction dialog was dismissed
		// by clicking the area outside the dialog.
		quickAction.setOnDismissListener(new QuickAction.OnDismissListener() {
			@Override
			public void onDismiss() {
				// Toast.makeText(getApplicationContext(), "Dismissed",
				// Toast.LENGTH_SHORT).show();
			}
		});
	}*/

	/** AsyncTask to download and load an image in ListView */
	private class GetVotingQs extends AsyncTask<String, Void, String> {
		boolean status=false;
		@Override
		protected void onPreExecute() {
			Log.e("Pre","KK");
			dlgProgress.setMessage("Please wait");
			dlgProgress.setCancelable(false);
			dlgProgress.show();
		}

		@Override
		protected void onPostExecute(String result) {
			// Getting adapter of the listview
			Log.e("post","KK");
			dlgProgress.dismiss();
			if(status){
				Log.e("size;"+title.size(),"KKK");
				if(title.size()>0)
				{
					if(image_urls.size()>0){
						for(int i=0;i<image_urls.size();i++)
						{
							Log.e("URL is--"+urls[i],"KK");
							urls= new String[image_urls.size()];
							urls[i]=image_urls.get(i);
							//Log.e("URL is--"+urls[i],"KK");
						}
						StaggeredAdapter adapter = new StaggeredAdapter(BjpSpeaksActivity.this, R.id.imageView1, urls);

						gridView.setAdapter(adapter);
						adapter.notifyDataSetChanged();
					}
					/*VotesAdapter adapter= new VotesAdapter(PollsActivity.this,android.R.layout.simple_list_item_1, questions);
				//ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, questions);
				lst_questions.setAdapter(adapter);*/
				}
			}
			else{
				Toast.makeText(BjpSpeaksActivity.this, "Oops error occured..", Toast.LENGTH_SHORT).show();
			}
		}

		@Override
		protected String doInBackground(String... params) {
			Log.e("background","KK");
			try {
				status=getVotingQs();
				return null;

			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

	}

	public boolean getVotingQs() {
		// TODO Auto-generated method stub
		//Bitmap decodedByte = null;
		boolean isList=false;
		HttpClient httpclient = new DefaultHttpClient();
		HttpPost httppost = new HttpPost(
				"http://www.smartcloudinfo.com/BJPConnect/index.php/feeds/getFeeds_REST");

		try {
			// Add your data

			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
			nameValuePairs.add(new BasicNameValuePair("device_id", android_id));

			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

			// Execute HTTP Post Request
			HttpResponse response = httpclient.execute(httppost);

			HttpEntity entity = response.getEntity();

			InputStream inStream = entity.getContent();
			String jsonresponse = Utils.convertStreamToString(inStream);
			Log.e("result==voting qs", jsonresponse);
			JSONObject obj1 = null;

			try {
				obj1 = new JSONObject(jsonresponse);

			} catch (JSONException e) {
				isList=false;
				e.printStackTrace();
			}
			try {
				String status = obj1.getString("status");
				if (status.equals("false")) {
					isList=false;
				} else {
					feed_ids.clear();
					title.clear();
					image_urls.clear();
					link_Array.clear();
					isList=true;
					JSONArray polls= obj1.getJSONArray("feeds");
					for(int i=0;i<polls.length();i++)
					{
						Log.e("Polls length--"+polls.length(),"KKK--"+i);
						JSONObject item= polls.getJSONObject(i);
						title.add(item.getString("title"));
						feed_ids.add(item.getString("id"));
						image_urls.add(item.getString("image"));
						link_Array.add(item.getString("link"));

					}
				}

			} catch (JSONException e) {
				isList=false;
				e.printStackTrace();
			}
		} catch (ClientProtocolException ex) {
			isList=false;
			ex.printStackTrace();
		} catch (IOException ez) {
			isList=false;
			ez.printStackTrace();
		}
		return isList;
	}

	public class StaggeredAdapter extends ArrayAdapter<String> {

		private com.smart.bjpConnect.flipboard.loader.ImageLoader mLoader;
		String[] imgs;
		public StaggeredAdapter(Context context, int textViewResourceId,
				String[] objects) {
			super(context, textViewResourceId, objects);
			mLoader = new ImageLoader(context);
			imgs=objects;
		}

		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {

			ViewHolder holder;
			Log.e("Got image view url--"+image_urls.get(position),"KKK");
			if (convertView == null) {
				LayoutInflater layoutInflator = LayoutInflater.from(getContext());
				convertView = layoutInflator.inflate(R.layout.row_staggered_demo,
						null);
				holder = new ViewHolder();
				holder.imageView = (ScaleImageView) convertView .findViewById(R.id.imageView1);
				holder.txt_flip = (TextView) convertView .findViewById(R.id.txt_flip);
				holder.txt_flip.setText(title.get(position));
				convertView.setTag(holder);
			}

			holder = (ViewHolder) convertView.getTag();
			//Log.e("Url to load--"+urls[position],"KKK"+getItem(position));
			mLoader.DisplayImage(image_urls.get(position), holder.imageView);
			mLoader.DisplayText(title.get(position), holder.txt_flip, image_urls.get(position));
			convertView.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

					textTitle= title.get(position);
					linkSelected= link_Array.get(position);
					int currentapiVersion = android.os.Build.VERSION.SDK_INT;
					if (currentapiVersion >= 17) {

						Log.e("link selected", linkSelected);
						String [] temp = null;
						temp= linkSelected.split("=");
						String VideoId = temp[1];
						Log.e("videoid", VideoId);
						Intent toYouTube= new Intent(BjpSpeaksActivity.this, YouTubeActivity.class);
						toYouTube.putExtra("VideoId" , VideoId);
						startActivity(toYouTube);
					}else{
						Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(linkSelected));
						startActivity(intent);
					}

				}
			});

			return convertView;
		}

		class ViewHolder {
			ScaleImageView imageView;
			TextView txt_flip;
		}
	}
	
	/*@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btn_menu:
			quickAction.show(v);
			break;

		default:
			break;
		}
	}*/
}