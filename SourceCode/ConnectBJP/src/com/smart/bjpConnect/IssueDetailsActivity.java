package com.smart.bjpConnect;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Base64;
import android.util.Log;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

public class IssueDetailsActivity extends ActinBarActivity{

	private String issue_title="";
	private WebView txt_issue_desc;
	private String issue_id="";
	private ConnectionDetector cd;
	private ProgressDialog dlgProgress;
	private String issue_desc="";

	Bitmap issue_icon;
	private ImageView img_issue_icon;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_issue_detail);
		setupActionBar();
		TextView title = (TextView) actionBarLayout.findViewById(R.id.txt_title);
		

		cd= new ConnectionDetector(IssueDetailsActivity.this);
		dlgProgress= new ProgressDialog(IssueDetailsActivity.this);
		//setupMenu();
		txt_issue_desc=(WebView)findViewById(R.id.txt_issue_desc);
		
		img_issue_icon= (ImageView)findViewById(R.id.img_issue);
		if(getIntent().getExtras()!=null)
		{
			issue_title= getIntent().getExtras().getString("title");
			title.setText(issue_title);
			issue_id=getIntent().getExtras().getString("id");

			if(cd.isConnectingToInternet())
			{
				GetIssueDetails issues= new GetIssueDetails();
				issues.execute();
			}

		}

	}


	/** AsyncTask to download and load an image in ListView */
	private class GetIssueDetails extends AsyncTask<String, Void, String> {
		boolean status=false;
		@Override
		protected void onPreExecute() {
			Log.e("Pre","KK");
			dlgProgress.setMessage("Loading the details..");
			dlgProgress.setCancelable(false);
			dlgProgress.show();
		}

		@Override
		protected void onPostExecute(String result) {
			// Getting adapter of the listview
			Log.e("post"+status,"KK"+issue_desc);
			dlgProgress.dismiss();
			if(status){
				String textfir = "<html><head>"
					+ "<body style=\"text-align: justify\">"
	              +"<p><h2>"
	              + issue_title
	              +"</h2></p>"
	              + issue_desc
	              + "</body>"
	              + "</head></html>";
				
				txt_issue_desc.loadDataWithBaseURL(null, textfir, null, "utf-8", null);
				img_issue_icon.setImageBitmap(issue_icon);
			}
		}

		@Override
		protected String doInBackground(String... params) {
			Log.e("background","KK");
			try {
				status=getIssueDetails();
				return null;

			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

	}

	public boolean getIssueDetails() {
		// TODO Auto-generated method stub
		//Bitmap decodedByte = null;
		boolean isList=false;
		HttpClient httpclient = new DefaultHttpClient();
		HttpPost httppost = new HttpPost(
				"http://www.smartcloudinfo.com/BJPConnect/index.php/issues/getIssueImage_REST");

		try {
			// Add your data

			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
			nameValuePairs.add(new BasicNameValuePair("id", issue_id));
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

			// Execute HTTP Post Request
			HttpResponse response = httpclient.execute(httppost);

			HttpEntity entity = response.getEntity();

			InputStream inStream = entity.getContent();
			String jsonresponse = Utils.convertStreamToString(inStream);
			Log.e("result==list issues", jsonresponse);
			JSONObject obj1 = null;

			try {
				obj1 = new JSONObject(jsonresponse);

			} catch (JSONException e) {

				e.printStackTrace();
			}
			try {
				String status = obj1.getString("status");
				if (status.equals("false")) {
					isList=false;
				} else {
					isList=true;
					issue_desc=obj1.getString("issue");
					String image=obj1.getString("image");
					Log.e("desc--", issue_desc);
			
						byte[] decodedString = Base64.decode(image, Base64.DEFAULT);
						issue_icon = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
					//}
				}

			} catch (JSONException e) {

			}
		} catch (ClientProtocolException ex) {

		} catch (IOException ez) {

		}
		return isList;

	}
}
