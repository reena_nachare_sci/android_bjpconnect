package com.smart.bjpConnect;

import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ImageButton;

import android.widget.TextView;

public class BjpActivity extends ActinBarActivity{
	private ImageButton btn_bjp_india;
	private ImageButton btn_history;
	private ImageButton btn_bjp_delhi;
	private ImageButton btn_national_leaders;
	private ImageButton btn_state_leaders;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_bjp1);
		setupActionBar();
		DisplayMetrics displaymetrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
		//int height = displaymetrics.heightPixels;
		int width = displaymetrics.widthPixels;
		if(width>480)
		{
			setContentView(R.layout.activity_bjp);
		}
		/*mBtn_MoreOptions = (ImageButton) actionBarLayout
				.findViewById(R.id.btn_menu);
		mBtn_MoreOptions.setVisibility(0);*/
		TextView title = (TextView) actionBarLayout.findViewById(R.id.txt_title);
		title.setText("BJP");
		initialize();
	}
	
	private void initialize() {
		// TODO Auto-generated method stub
		btn_bjp_india=(ImageButton)findViewById(R.id.btn_bjp_india);
		btn_bjp_india.setOnClickListener(this);
		btn_history=(ImageButton)findViewById(R.id.btn_history);
		btn_history.setOnClickListener(this);
		btn_bjp_delhi=(ImageButton)findViewById(R.id.btn_bjp_delhi);
		btn_bjp_delhi.setOnClickListener(this);
		
		btn_national_leaders=(ImageButton)findViewById(R.id.btn_leaders_national);
		btn_national_leaders.setOnClickListener(this);
		
		btn_state_leaders=(ImageButton)findViewById(R.id.btn_leaders_state);
		btn_state_leaders.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btn_bjp_india:
			/*Intent toPhilosophy= new Intent(BjpActivity.this, AboutDetailActivity.class);
			toPhilosophy.putExtra("display", "bjp_india");
			startActivity(toPhilosophy);*/
			Intent toDisplayActivity= new Intent(BjpActivity.this, DisplaySites.class);
			toDisplayActivity.putExtra("display", "bjp_india");
			startActivity(toDisplayActivity);
			break;
		case R.id.btn_history:
			Intent tohistory= new Intent(BjpActivity.this, AboutDetailActivity.class);
			tohistory.putExtra("display", "history");
			startActivity(tohistory);
			break;
		case R.id.btn_bjp_delhi:
			Intent toCon= new Intent(BjpActivity.this, AboutDetailActivity.class);
			toCon.putExtra("display", "faq");
			startActivity(toCon);
			
			/*Intent toCon= new Intent(BjpActivity.this, AboutDetailActivity.class);
			toCon.putExtra("display", "bjp_delhi");
			startActivity(toCon);*/
			break;
		case R.id.btn_leaders_national:
			Intent toLeader= new Intent(BjpActivity.this, NationalLeaderActivity.class);
			//toLeader.putExtra("display", "State_leadership");
			toLeader.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(toLeader);
			break;
		case R.id.btn_leaders_state:
			Intent toLeaders= new Intent(BjpActivity.this, StateLeaderActivity.class);
			//toLeaders.putExtra("display", "National_leadership");
			toLeaders.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(toLeaders);
			break;

		default:
			break;
		}
		super.onClick(v);
	}
	
	@Override
	public void onBackPressed() {
		super.onBackPressed();
	/*
		Intent toAdd= new Intent(BjpActivity.this,MainActivity.class);
		startActivity(toAdd);
		finish();*/
	}
}
