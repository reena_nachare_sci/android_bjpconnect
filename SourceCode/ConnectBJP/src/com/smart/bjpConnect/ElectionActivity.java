package com.smart.bjpConnect;


import android.net.Uri;
import android.os.Bundle;
import android.content.Intent;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

public class ElectionActivity extends ActinBarActivity {

	private ImageButton register_to_vote;
	private ImageButton BJP_vision;
	private ImageButton Poll_Booth_Info;
	private ImageButton FAQ;
	private ImageButton Join_BJP;

	private ConnectionDetector cd;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_election1);
		DisplayMetrics displaymetrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
		//int height = displaymetrics.heightPixels;
		int width = displaymetrics.widthPixels;
		if(width>480)
		{
			setContentView(R.layout.activity_election);
		}
		setupActionBar();
		TextView title = (TextView) actionBarLayout.findViewById(R.id.txt_title);
		title.setText("ELECTIONS");
		initialize();
	}

	private void initialize() {
		register_to_vote=(ImageButton)findViewById(R.id.btn_register_to_vote);
		register_to_vote.setOnClickListener(this);
		BJP_vision=(ImageButton)findViewById(R.id.btn_BJP_vision);
		BJP_vision.setOnClickListener(this);
		Poll_Booth_Info=(ImageButton)findViewById(R.id.btn_Poll_Booth_Info);
		Poll_Booth_Info.setOnClickListener(this);
		FAQ=(ImageButton)findViewById(R.id.btn_FAQ);
		FAQ.setOnClickListener(this);
		Join_BJP=(ImageButton)findViewById(R.id.btn_Join_BJP);
		Join_BJP.setOnClickListener(this);
		cd= new ConnectionDetector(ElectionActivity.this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_register_to_vote:
			Uri registerUrl = Uri.parse("http://www.ceodelhi.gov.in/OnlineErms/login.aspx");  
			Intent register = new Intent(Intent.ACTION_VIEW, registerUrl);  
			startActivity(register);  
			break;
		case R.id.btn_BJP_vision:
			
			Intent toVision= new Intent(ElectionActivity.this, VisionActivity.class);
			toVision.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(toVision);
			
			
			break;
		case R.id.btn_Poll_Booth_Info:
			/*Intent launchBrowser = new Intent(Intent.ACTION_VIEW);
			launchBrowser.setData(Uri.parse("http://www.eci-polldaymonitoring.nic.in/psl/default.aspx"));
			startActivity(launchBrowser);*/
			if(cd.isConnectingToInternet()){
				Intent launchBrowser= new Intent(ElectionActivity.this, PollBoothActivity.class);
				launchBrowser.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(launchBrowser);
			}
			else{
				Toast.makeText(ElectionActivity.this, "NO Internet", Toast.LENGTH_SHORT).show();	
				finish();
			}

			break;
		case R.id.btn_FAQ:
			Intent toFAQ= new Intent(ElectionActivity.this, FAQActivity.class);
			toFAQ.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(toFAQ);
			break;
		case R.id.btn_Join_BJP:
			/*Intent toJoinBJP= new Intent(ElectionActivity.this, DisplaySites.class);
			toJoinBJP.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			toJoinBJP.putExtra("display", "join");
			startActivity(toJoinBJP);*/
			
			Intent toJoinBJP= new Intent(ElectionActivity.this, JoinBJPFormActivity.class);
			toJoinBJP.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(toJoinBJP);
			
			break;
		default:
			break;
		}
		super.onClick(v);
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		/*Intent toAdd= new Intent(BjpActivity.this,MainActivity.class);
		startActivity(toAdd);
		finish();*/
	}
}
