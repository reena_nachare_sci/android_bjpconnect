package com.smart.bjpConnect;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.widget.AdapterView.OnItemClickListener;;

public class MediaPressActivity extends ActinBarActivity {

	//private static final String TAG = null;
	private ProgressDialog dlgProgress;
	private Map<String, String> filesMapMedia;
	private ArrayList<String> titleArrayList ;
	private ListView listView ;
	//private Context mContext;
	//private ImageView listImage;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_media_press);

		setupActionBar();
		TextView title = (TextView) actionBarLayout.findViewById(R.id.txt_title);
		title.setText("MEDIA AND PRESS");

		//mContext = this;

		filesMapMedia = new HashMap<String, String>();
		titleArrayList = new ArrayList<String>();

		GetMediaAndPress GMAP = new GetMediaAndPress();
		GMAP.execute();

		listView = (ListView) findViewById(R.id.list);
		//listImage = (ImageView) findViewById(R.id.listImage);

	}

	/** AsyncTask to download and load an image in ListView */
	private class GetMediaAndPress extends AsyncTask<String, Void, String> {
		boolean status=false;
		@Override
		protected void onPreExecute() {
			Log.e("Pre","KK");
			dlgProgress = new ProgressDialog(MediaPressActivity.this);
			dlgProgress.setMessage("Please wait");
			dlgProgress.setCancelable(false);
			dlgProgress.show();
		}

		@Override
		protected void onPostExecute(String result) {
			// Getting adapter of the listview
			Log.e("post","KK");
			dlgProgress.dismiss();
			if(status){
				/*Intent toAfterLogin = new Intent(MediaPressActivity.this,
						AfterLoginActivity.class);
				startActivity(toAfterLogin);*/


				/*ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext,
						android.R.layout.simple_list_item_1, android.R.id.text1, titleArrayList);*/
				StableArrayAdapter adapter1 = new StableArrayAdapter(MediaPressActivity.this,
						android.R.layout.simple_list_item_1, titleArrayList);

				// Assign adapter to ListView
				listView.setAdapter(adapter1); 

				// ListView Item Click Listener
				listView.setOnItemClickListener(new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> parent, View view, int position,
							long id) {
						// ListView Clicked item index
						//int itemPosition     = position;

						// ListView Clicked item value
						String  itemValue    = (String) listView.getItemAtPosition(position);

						// Show Alert 
						/*Toast.makeText(getApplicationContext(),
								"Position :"+itemPosition+"  ListItem : " +itemValue , Toast.LENGTH_LONG)
								.show();*/




						Iterator<?> iter = filesMapMedia.entrySet().iterator();
						while (iter.hasNext()) {
							@SuppressWarnings("rawtypes")
							Map.Entry mEntry = (Map.Entry) iter.next();
							System.out.println(mEntry.getKey() + " : " + mEntry.getValue());
							if(itemValue.trim().equals(mEntry.getKey())){
								Intent toLoadImage = new Intent(MediaPressActivity.this,
										DisplaySites.class);
								toLoadImage.putExtra("display", "mediapress" );
								toLoadImage.putExtra("urlmediaandpress", mEntry.getValue().toString() );
								startActivity(toLoadImage);
							}
						}
					}
				}); 
			}
		}

		@Override
		protected String doInBackground(String... params) {
			Log.e("background","KK");
			try {
				status=GetOptions();
				return null;

			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}
	}
	private class StableArrayAdapter extends ArrayAdapter<String> {

		HashMap<String, Integer> mIdMap = new HashMap<String, Integer>();

		public StableArrayAdapter(Context context, int textViewResourceId,
				List<String> objects) {
			super(context, textViewResourceId, objects);
			for (int i = 0; i < objects.size(); ++i) {
				mIdMap.put(objects.get(i), i);
			}
		}

		@Override
		public long getItemId(int position) {
			String item = getItem(position);
			return mIdMap.get(item);
		}

		@Override
		public boolean hasStableIds() {
			return true;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View view = convertView;
			if (view == null) {
				Log.e("View is null","KKK");
				LayoutInflater vi = (LayoutInflater)MediaPressActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				view = vi.inflate(R.layout.questions_list_row, null);
			}
			Log.e("getView--"+position,"KK");
			TextView txt_question= (TextView)view.findViewById(R.id.txt_question_name);
			txt_question.setText(titleArrayList.get(position));
			return view;
		}

	}

	public boolean GetOptions() {
		// TODO Auto-generated method stub
		//Bitmap decodedByte = null;
		boolean isList=false;
		HttpClient httpclient = new DefaultHttpClient();
		HttpPost httppost = new HttpPost(
				"http://www.smartcloudinfo.com/BJPConnect/index.php/medias/getMedias_REST");
		try {
			// Add your data

			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
			//nameValuePairs.add(new BasicNameValuePair("device_id", android_id));

			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

			// Execute HTTP Post Request
			HttpResponse response = httpclient.execute(httppost);

			HttpEntity entity = response.getEntity();

			InputStream inStream = entity.getContent();
			String jsonresponse = Utils.convertStreamToString(inStream);
			Log.e("result==voting qs", jsonresponse);
			JSONObject obj1 = null;

			try {
				obj1 = new JSONObject(jsonresponse);

			} catch (JSONException e) {

				e.printStackTrace();
			}
			try {
				String status = obj1.getString("status");
				if (status.equals("false")) {
					isList=false;
				} else {
					isList=true;
					JSONArray medias= obj1.getJSONArray("medias");
					for(int i=0;i<medias.length();i++)
					{
						JSONObject item= medias.getJSONObject(i);
						filesMapMedia.put(item.getString("title"), item.getString("image"));
						titleArrayList.add(item.getString("title"));
					}
				}

			} catch (JSONException e) {
				e.printStackTrace();
			}
		} catch (ClientProtocolException ex) {
			ex.printStackTrace();
		} catch (IOException ez) {
			ez.printStackTrace();
		}
		return isList;
	}
}