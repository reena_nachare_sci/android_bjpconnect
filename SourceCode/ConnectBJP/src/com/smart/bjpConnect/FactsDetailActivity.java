package com.smart.bjpConnect;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import com.smart.bjpConnect.quickaction.ActionItem;
import com.smart.bjpConnect.quickaction.QuickAction;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings.Secure;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;


public class FactsDetailActivity extends ActinBarActivity{
	
	private static String confrim_answer="";

	TextView txt_question;
	private RadioButton option1;
	private RadioButton option2;
	private RadioButton option3;
	private RadioButton option4;
	private RadioButton option5;
	private Button btn_vote;
	private Button btn_cancel;
	private ProgressDialog dlgProgress;
	private String android_id;
	private RadioGroup option_grp;
	private String selected_option;
	private String question_id;
	private String option1_txt;
	private String option2_txt;
	private String option3_txt;
	private String option4_txt;
	private String option5_txt;
	private String question_title;

	ImageButton btn_left;
	ImageButton btn_right;

	AlertDialogManager alert= new AlertDialogManager();
	ConnectionDetector cd;
	//private String display="";
	private TextView txt_question_num;

	private QuickAction quickAction;
	ImageButton credit;

	// action id
	private static final int ID_SHARE = 2;
	private static final int ID_LOGIN = 1;
	private static final int ID_NOTIFY = 3;
	private static final int ID_CALL = 5;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.detail_fragment);

		android_id = Secure.getString(FactsDetailActivity.this.getContentResolver(),
				Secure.ANDROID_ID);

		setupActionBar();
		TextView title = (TextView) actionBarLayout.findViewById(R.id.txt_title);
		title.setText("Know The Facts");

		credit = (ImageButton) findViewById(R.id.credit1);
		credit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				
				if(confrim_answer.equalsIgnoreCase("-1") || confrim_answer.equalsIgnoreCase("")){
					Toast.makeText(FactsDetailActivity.this, "Please confirm Your Answer to Share", Toast.LENGTH_SHORT).show();
				}else {
					Intent toShare = new Intent(FactsDetailActivity.this,
							ShareButtonActivity.class);
					
					String ans="";
					if(confrim_answer.equalsIgnoreCase("1")){
						ans= option1_txt;
					}
					else if(confrim_answer.equalsIgnoreCase("2")){
						ans= option2_txt;
					}
					else if(confrim_answer.equalsIgnoreCase("3")){
						ans= option3_txt;
					}
					else if(confrim_answer.equalsIgnoreCase("4")){
						ans= option4_txt;
					}
					else if(confrim_answer.equalsIgnoreCase("5")){
						ans= option5_txt;
					}
					
					toShare.putExtra("TOSHARE", question_title + " Answer : "+ ans );
					startActivity(toShare);
				}
			}
		});

		setupMenuOptions();

		cd= new ConnectionDetector(FactsDetailActivity.this);
		dlgProgress= new ProgressDialog(FactsDetailActivity.this);
		txt_question = (TextView)findViewById(R.id.txt_vote_question);

		option_grp=(RadioGroup)findViewById(R.id.radioGroup1);

		option1=(RadioButton)findViewById(R.id.option1);
		option2=(RadioButton)findViewById(R.id.option2);
		option3=(RadioButton)findViewById(R.id.option3);
		option4=(RadioButton)findViewById(R.id.option4);
		option5=(RadioButton)findViewById(R.id.option5);

		btn_left=(ImageButton)findViewById(R.id.btn_left);
		btn_right=(ImageButton)findViewById(R.id.btn_right);

		txt_question_num = (TextView)findViewById(R.id.question_num);
		txt_question_num.setVisibility(View.GONE);

		btn_left.setVisibility(View.GONE);
		btn_right.setVisibility(View.GONE);

		btn_vote=(Button)findViewById(R.id.btn_vote);
		btn_vote.setOnClickListener(this);
		btn_cancel=(Button)findViewById(R.id.btn_cancel);
		btn_cancel.setOnClickListener(this);

		if(getIntent().getExtras()!=null)
		{
			Log.e("Got intent","KK");
			question_id= getIntent().getExtras().getString("id");
			option1_txt= getIntent().getExtras().getString("option1");
			option2_txt= getIntent().getExtras().getString("option2");
			option3_txt= getIntent().getExtras().getString("option3");
			option4_txt= getIntent().getExtras().getString("option4");
			option5_txt= getIntent().getExtras().getString("option5");
			question_title=getIntent().getExtras().getString("title");
			//display= getIntent().getExtras().getString("display");
			Log.e("Title--"+question_title,"KK"+option1_txt);

			btn_vote.setText("Confirm");
			title.setText("Know The Facts");
			//}
		}
		Log.e("Tiele--"+question_title,"KK"+option1_txt);
		setVotingTitle(question_title);
		setOptions();
	}

	public void setOptions() {
		// TODO Auto-generated method stub
		Log.e("Setting--"+option1_txt+"--","KK"+option1);
		if(!option1_txt.equals(""))
			option1.setText(option1_txt);
		else
			option1.setVisibility(View.INVISIBLE);

		if(option2_txt.equals("")){
			option2.setVisibility(View.INVISIBLE);
		}
		else{
			option2.setVisibility(View.VISIBLE);
			Log.e("Setting option--"+option2_txt,"KK");
			option2.setText(option2_txt);
		}


		if(!option3_txt.equals("")){
			option3.setVisibility(View.VISIBLE);
			option3.setText(option3_txt);
		}
		else{
			option3.setVisibility(View.GONE);
		}

		if(!option4_txt.equals(""))
			option4.setText(option4_txt);
		else
			option4.setVisibility(View.GONE);

		if(!option5_txt.equals(""))
			option5.setText(option5_txt);
		else
			option5.setVisibility(View.GONE);

	}

	public void setVotingTitle(String capt) {
		// TODO Auto-generated method stub
		txt_question.setText(question_title);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btn_vote:

			sendFact();

			break;
		case R.id.btn_cancel:
			//VotingActivity.PopFragments();
			finish();
			break;
			
		case R.id.btn_menu:
			quickAction.show(v);
			break;

		default:
			break;
		}

	}

	private void sendFact() {
		// TODO Auto-generated method stub
		String option=getSelectedOption();
		if (option.equalsIgnoreCase("-1")){

			Toast.makeText(FactsDetailActivity.this, "No option selected!", Toast.LENGTH_SHORT).show();
		}
		else{
			Log.e("U selected--"+selected_option,"KKK"+question_id);
			SendFact send_fact= new SendFact();
			String[] params= {question_id,selected_option};
			send_fact.execute(params);
		}
	}

	/** AsyncTask to download and load an image in ListView */
	private class SendFact extends AsyncTask<String, Void, String> {
		
		String user_option="";
		@Override
		protected void onPreExecute() {
			Log.e("Pre","KK");

			dlgProgress.setMessage("Confirming your answer!");
			dlgProgress.show();
		}

		@Override
		protected void onPostExecute(String result) {
			// Getting adapter of the listview
			Log.e("post","KK");
			dlgProgress.dismiss();

			final AlertDialog alertDialog = new AlertDialog.Builder(FactsDetailActivity.this).create();
			if(confrim_answer.equalsIgnoreCase("-1")){

			}
			else{
				if(confrim_answer.equalsIgnoreCase(user_option)){
					alertDialog.setTitle("Congratulations!");
					alertDialog.setMessage("You have given the correct answer!");
					alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
							new DialogInterface.OnClickListener() {

						public void onClick(DialogInterface dialog, int which) {

							dialog.dismiss();
							finish();
						}
					});
					alertDialog.setCancelable(false);
					alertDialog.setCanceledOnTouchOutside(false);
					alertDialog.show();
				}
				else{
					String ans="";
					if(confrim_answer.equalsIgnoreCase("1")){
						ans= option1_txt;
					}
					else if(confrim_answer.equalsIgnoreCase("2")){
						ans= option2_txt;
					}
					else if(confrim_answer.equalsIgnoreCase("3")){
						ans= option3_txt;
					}
					else if(confrim_answer.equalsIgnoreCase("4")){
						ans= option4_txt;
					}
					else if(confrim_answer.equalsIgnoreCase("5")){
						ans= option5_txt;
					}
					alertDialog.setTitle("Oops!");


					alertDialog.setMessage("Incorrect answer! Correct answer is:-"+"\n"+ans);
					alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
							new DialogInterface.OnClickListener() {

						public void onClick(DialogInterface dialog, int which) {

							dialog.dismiss();
							finish();
						}
					});
					alertDialog.setCancelable(false);
					alertDialog.setCanceledOnTouchOutside(false);
					alertDialog.show();
				}
			}
		}

		@Override
		protected String doInBackground(String... params) {
			Log.e("background"+question_id,"KK");
			try {
				user_option=params[1];
				confrim_answer=confirmFact(question_id, params[1]);
				return null;

			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

	}

	public String confirmFact(String poll_id,String option_id) {
		// TODO Auto-generated method stub
		String answer="";
		HttpClient httpclient = new DefaultHttpClient();
		HttpPost httppost = new HttpPost(
				"http://www.smartcloudinfo.com/BJPConnect/index.php/knowfacts/answerKnowFact_REST");

		try {
			// Add your data

			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
			nameValuePairs.add(new BasicNameValuePair("device_id", android_id));
			nameValuePairs.add(new BasicNameValuePair("knowfact_id", poll_id));
			nameValuePairs.add(new BasicNameValuePair("option", option_id));


			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

			// Execute HTTP Post Request
			HttpResponse response = httpclient.execute(httppost);

			HttpEntity entity = response.getEntity();

			InputStream inStream = entity.getContent();
			String jsonresponse = Utils.convertStreamToString(inStream);
			Log.e("result send facts qs", jsonresponse);
			JSONObject obj1 = null;

			try {
				obj1 = new JSONObject(jsonresponse);

			} catch (JSONException e) {

				e.printStackTrace();
			}
			try {
				String status = obj1.getString("status");
				if (status.equals("false")) {
					answer="-1";
				} else {
					answer= obj1.getString("correctanswer");

				}

			} catch (JSONException e) {

			}
		} catch (ClientProtocolException ex) {

		} catch (IOException ez) {

		}
		return answer;
	}


	private  String getSelectedOption() {
		int selected= option_grp.getCheckedRadioButtonId();
		if (selected == -1){
			//no item selected
			selected_option="-1";
			Toast.makeText(FactsDetailActivity.this, "No option selected!", Toast.LENGTH_SHORT).show();
		}
		else{
			if (selected == R.id.option1){
				//Do something with the button
				selected_option="1";
			}
			else if(selected == R.id.option2){
				//Do something with the button
				selected_option="2";
			}
			else if(selected == R.id.option3){
				//Do something with the button
				selected_option="3";
			}
			else if(selected == R.id.option4){
				//Do something with the button
				selected_option="4";
			}
			else if(selected == R.id.option5){
				//Do something with the button
				selected_option="5";
			}

		}
		return selected_option;
	}

	private void setupMenuOptions() {
		// TODO Auto-generated method stub
		ActionItem nextItem= new ActionItem(ID_NOTIFY, "", getResources()
				.getDrawable(R.drawable.notification));
		ActionItem prevItem = new ActionItem(ID_LOGIN, "", getResources()
				.getDrawable(R.drawable.login_button));
		ActionItem searchItem = new ActionItem(ID_SHARE, "", getResources()
				.getDrawable(R.drawable.i_sign));
		ActionItem callItem= new ActionItem(ID_CALL, "", getResources()
				.getDrawable(R.drawable.join_us_with_a_missed_call_01));
		/*ActionItem creditItem= new ActionItem(ID_CREDITS, "", getResources()
				.getDrawable(R.drawable.btn_credits));*/
		/*ActionItem creditItem= new ActionItem(ID_CREDITS, "", getResources()
				.getDrawable(R.drawable.credits_active));*/

		// use setSticky(true) to disable QuickAction dialog being dismissed
		// after an item is clicked
		prevItem.setSticky(true);
		nextItem.setSticky(true);
		callItem.setSticky(true);
		//creditItem.setSticky(true);

		// create QuickAction. Use QuickAction.VERTICAL or
		// QuickAction.HORIZONTAL param to define layout
		// orientation
		quickAction = new QuickAction(this,
				QuickAction.VERTICAL);

		// add action items into QuickAction
		quickAction.addActionItem(nextItem);
		quickAction.addActionItem(prevItem);
		quickAction.addActionItem(searchItem);
		quickAction.addActionItem(callItem);
		//quickAction.addActionItem(creditItem);

		// Set listener for action item clicked
		quickAction
		.setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {
			@Override
			public void onItemClick(QuickAction source, int pos,
					int actionId) {
				@SuppressWarnings("unused")
				ActionItem actionItem = quickAction.getActionItem(pos);

				// here we can filter which action item was clicked with
				// pos or actionId parameter
				if (actionId == ID_NOTIFY) {
					Intent toSettings = new Intent(FactsDetailActivity.this,
							NotificationActivity.class);
					startActivity(toSettings);
				} else if (actionId == ID_SHARE) {
					Intent toCreditAct = new Intent(FactsDetailActivity.this,
							CreditsActivity.class);
					startActivity(toCreditAct);
				} else if(actionId == ID_LOGIN){
					Intent toLoginAct = new Intent(FactsDetailActivity.this,
							CandidateLoginActivity.class);
					startActivity(toLoginAct);
				}
				else if(actionId == ID_CALL){
					Intent toLoginAct = new Intent(FactsDetailActivity.this,
							JoinWithCallActivity.class);
					startActivity(toLoginAct);
				}
				/*else if(actionId == ID_CREDITS){
					Intent tocreditAct = new Intent(MainActivity1.this,
							CreditsActivity.class);
					startActivity(toLoginAct);
				}*/
			}
		});

		// set listnener for on dismiss event, this listener will be called only
		// if QuickAction dialog was dismissed
		// by clicking the area outside the dialog.
		quickAction.setOnDismissListener(new QuickAction.OnDismissListener() {
			@Override
			public void onDismiss() {
				// Toast.makeText(getApplicationContext(), "Dismissed",
				// Toast.LENGTH_SHORT).show();
			}
		});
	}
}