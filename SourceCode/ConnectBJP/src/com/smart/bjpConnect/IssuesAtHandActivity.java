package com.smart.bjpConnect;

import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

public class IssuesAtHandActivity extends ActinBarActivity{
	private ImageButton btn_bjp_speaks;
	private ImageButton btn_opinion;
	private ImageButton btn_matters_delhi;
	private ImageButton btn_media;
	private ImageButton btn_know_facts;
	
	Intent displayURL;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_issues_hand1);
		DisplayMetrics displaymetrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
		//int height = displaymetrics.heightPixels;
		int width = displaymetrics.widthPixels;
		if(width>480)
		{
			setContentView(R.layout.activity_issues_hand);
		}
		setupActionBar();
		TextView title = (TextView) actionBarLayout.findViewById(R.id.txt_title);
		title.setText("ISSUES THAT MATTER");
		initialize();
		displayURL = new Intent(IssuesAtHandActivity.this, DisplaySites.class);
	}
	
	private void initialize() {
		// TODO Auto-generated method stub
		btn_bjp_speaks=(ImageButton)findViewById(R.id.btn_bjp_speaks);
		btn_bjp_speaks.setOnClickListener(this);
		btn_opinion=(ImageButton)findViewById(R.id.btn_opinion);
		btn_opinion.setOnClickListener(this);
		btn_matters_delhi=(ImageButton)findViewById(R.id.btn_matters_delhi);
		btn_matters_delhi.setOnClickListener(this);
		
		btn_media=(ImageButton)findViewById(R.id.btn_media);
		btn_media.setOnClickListener(this);
		
		btn_know_facts=(ImageButton)findViewById(R.id.btn_know_the_facts);
		btn_know_facts.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btn_bjp_speaks:
			Intent BjpSpeaks= new Intent(IssuesAtHandActivity.this,BjpSpeaksActivity.class);
			BjpSpeaks.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			   startActivity(BjpSpeaks);
			 
			break;
		case R.id.btn_opinion:
			Intent survey= new Intent(IssuesAtHandActivity.this,PollsActivity.class);
			survey.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(survey);
			break;
		case R.id.btn_matters_delhi:
			Intent issues= new Intent(IssuesAtHandActivity.this,IssuesActivity.class);
			issues.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(issues);
			break;
		case R.id.btn_media:
			Intent media= new Intent(IssuesAtHandActivity.this,MediaActivity.class);
			media.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);   
			startActivity(media);
			break;
		case R.id.btn_know_the_facts:
			Intent facts= new Intent(IssuesAtHandActivity.this,KnowTheFactsActivity.class);
			facts.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(facts);
			break;

		default:
			break;
		}
		super.onClick(v);
	}
	
	@Override
	public void onBackPressed() {
		super.onBackPressed();
	}
}