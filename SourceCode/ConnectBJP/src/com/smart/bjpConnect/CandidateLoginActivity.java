package com.smart.bjpConnect;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class CandidateLoginActivity extends ActinBarActivity {

	EditText useridEditText;
	Button loginButton;
	private ProgressDialog dlgProgress;
	
	//ArrayList<String> titleArrayList;
	//ArrayList<String> urlArrayList;
	
	public static Map<String, String> filesMapImages;
	public static Map<String, String> filesMapAudio;
	public static Map<String, String> filesMapVideo;

   ConnectionDetector cd;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_candidate_login);
		setupActionBar();
		TextView title = (TextView) actionBarLayout.findViewById(R.id.txt_title);
		title.setText("BJP VOLUNTEER LOGIN");
		initialize();
		cd=new ConnectionDetector(CandidateLoginActivity.this);
		
	}

	private void initialize() {
		// TODO Auto-generated method stub
		useridEditText=(EditText)findViewById(R.id.userId);
		loginButton = (Button) findViewById(R.id.buttonLogin);
		loginButton.setOnClickListener(this);
		
		//titleArrayList = new ArrayList<String>();
		//urlArrayList =  new ArrayList<String>();
		
		filesMapImages = new HashMap<String, String>();
		filesMapAudio = new HashMap<String, String>();
		filesMapVideo = new HashMap<String, String>();
	}
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.buttonLogin:
			if(useridEditText.getText().toString().trim().equals("abcd")){
				GetOptions options= new GetOptions();
				options.execute();
			}else{
				Toast.makeText(getApplicationContext(),"Please Enter Valid Id", Toast.LENGTH_SHORT).show();
			}
			break;

		default:
			break;
		}
		super.onClick(v);
	}
	
	/** AsyncTask to download and load an image in ListView */
	private class GetOptions extends AsyncTask<String, Void, String> {
		boolean status=false;
		boolean isNetError=false;
		@Override
		protected void onPreExecute() {
			Log.e("Pre","KK");
			dlgProgress = new ProgressDialog(CandidateLoginActivity.this);
			dlgProgress.setMessage("Please wait");
			dlgProgress.setCancelable(false);
			dlgProgress.show();
		}

		@Override
		protected void onPostExecute(String result) {
			// Getting adapter of the listview
			Log.e("post","KK");
			dlgProgress.dismiss();
			if(isNetError){
			Toast.makeText(CandidateLoginActivity.this, "NO Internet", Toast.LENGTH_SHORT).show();	
			}
			else{
			if(status){
				Intent toAfterLogin = new Intent(CandidateLoginActivity.this,
						AfterLoginActivity.class);
				startActivity(toAfterLogin);
			}
			}
		}

		@Override
		protected String doInBackground(String... params) {
			Log.e("background","KK");
			
			try {
				if(cd.isConnectingToInternet()){
				status=GetOptions();
				return null;
				}
				else{
					isNetError=true;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}
	}
	
	public boolean GetOptions() {
		// TODO Auto-generated method stub
		//Bitmap decodedByte = null;
		boolean isList=false;
		HttpClient httpclient = new DefaultHttpClient();
		HttpPost httppost = new HttpPost(
				"http://www.smartcloudinfo.com/BJPConnect/index.php/downloads/getDownloads_REST");
		try {
			// Add your data

			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
			//nameValuePairs.add(new BasicNameValuePair("device_id", android_id));

			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

			// Execute HTTP Post Request
			HttpResponse response = httpclient.execute(httppost);

			HttpEntity entity = response.getEntity();

			InputStream inStream = entity.getContent();
			String jsonresponse = Utils.convertStreamToString(inStream);
			Log.e("result==voting qs", jsonresponse);
			JSONObject obj1 = null;

			try {
				obj1 = new JSONObject(jsonresponse);

			} catch (JSONException e) {

				e.printStackTrace();
			}
			try {
				String status = obj1.getString("status");
				if (status.equals("false")) {
					isList=false;
				} else {
					isList=true;
					JSONArray images= obj1.getJSONArray("images");
					for(int i=0;i<images.length();i++)
					{
						JSONObject item= images.getJSONObject(i);
						filesMapImages.put(item.getString("title"), item.getString("url"));					
					}
					JSONArray audios= obj1.getJSONArray("audios");
					for(int i=0;i<audios.length();i++)
					{
						JSONObject item= audios.getJSONObject(i);
						filesMapAudio.put(item.getString("title"), item.getString("url"));					
					}
					JSONArray videos= obj1.getJSONArray("videos");
					for(int i=0;i<videos.length();i++)
					{
						JSONObject item= videos.getJSONObject(i);
						filesMapVideo.put(item.getString("title"), item.getString("url"));					
					}
				}

			} catch (JSONException e) {
				e.printStackTrace();
			}
		} catch (ClientProtocolException ex) {
			ex.printStackTrace();
		} catch (IOException ez) {
			ez.printStackTrace();
		}
		return isList;
	}
}
