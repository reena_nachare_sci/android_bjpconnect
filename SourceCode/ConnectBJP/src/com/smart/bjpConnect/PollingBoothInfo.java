package com.smart.bjpConnect;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.StringTokenizer;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.maps.GeoPoint;

import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class PollingBoothInfo extends ActinBarActivity {

	GoogleMap googleMap;
	MarkerOptions markerOptions;
	LatLng latLng,delhilatlong;
	StringTokenizer tokens;
	int i = 0;
	String[] seperated;
	String address;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.test_pollbooth);
		SupportMapFragment supportMapFragment = (SupportMapFragment) getSupportFragmentManager()
				.findFragmentById(R.id.map);
		
		setupActionBar();
		TextView title = (TextView) actionBarLayout.findViewById(R.id.txt_title);
		title.setText("POLL BOOTH INFO");

		// Getting a reference to the map
		googleMap = supportMapFragment.getMap();

		delhilatlong=new LatLng(28.635308,
				77.22496);
		/*googleMap.animateCamera(CameraUpdateFactory
				.newLatLng(delhilatlong));*/
	
		googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(28.635308, 77.22496), 10.0f));
		address=getIntent().getExtras().getString("address")+" Delhi";
		Log.e("address", address);
		new GeocoderTask().execute(address);
		googleMap.setInfoWindowAdapter(new InfoWindowAdapter() {

	        @Override
	        public View getInfoWindow(Marker arg0) {
	            return null;
	        }

	        @Override
	        public View getInfoContents(Marker marker) {

	            View v = getLayoutInflater().inflate(R.layout.test, null);

	            TextView info= (TextView) v.findViewById(R.id.info);

	            info.setText(marker.getTitle());

	            return v;
	        }
	    });
		/*// Getting reference to btn_find of the layout activity_main
		Button btn_find = (Button) findViewById(R.id.btn_find);

		// Defining button click event listener for the find button
		OnClickListener findClickListener = new OnClickListener() {
			@Override
			public void onClick(View v) {
				// Getting reference to EditText to get the user input location
				EditText etLocation = (EditText) findViewById(R.id.et_location);

				// Getting user input location
				String location = etLocation.getText().toString();

				tokens = new StringTokenizer(location, ",");
				seperated = location.split(",");
				Log.e("seperated size", "" + seperated.length);

				
				 * if(location!=null && !location.equals("")){ new
				 * GeocoderTask().execute(tokens.nextToken()); }
				 
				i = 0;
				if (seperated.length > 0) {
					showMarker=true;
					new GeocoderTask().execute(seperated[i]);

				}

				GeoPoint srcGeoPoint =getGeoPoint(getLocationInfo(location.replace("\n"," ").replace(" ", "%20")));
				Log.e("lat"+srcGeoPoint.getLatitudeE6(),"longi"+srcGeoPoint.getLongitudeE6());
			}
		};

		// Setting button click event listener for the find button
		btn_find.setOnClickListener(findClickListener);*/

	}

	// An AsyncTask class for accessing the GeoCoding Web Service
	private class GeocoderTask extends AsyncTask<String, Void, List<Address>> {

		boolean isError = false;
		GeoPoint srcGeoPoint=null;
		@Override
		protected List<Address> doInBackground(String... locationName) {
			// Creating an instance of Geocoder class
			Geocoder geocoder = new Geocoder(getBaseContext());
			List<Address> addresses = null;
			
			
			try {
				 srcGeoPoint =getGeoPoint(getLocationInfo(locationName[0].replace("\n"," ").replace(" ", "%20")));
				Log.e("lat"+srcGeoPoint.getLatitudeE6(),"longi"+srcGeoPoint.getLongitudeE6());
				// Getting a maximum of 3 Address that matches the input text
				//addresses = geocoder.getFromLocationName(locationName[0], 3);
			} catch (Exception e) {
				isError = true;
				e.printStackTrace();
			}
			return addresses;
		}

		@Override
		protected void onPostExecute(List<Address> addresses) {

			if (isError) {
			
				Toast.makeText(getBaseContext(), "Error finding location",
						Toast.LENGTH_SHORT).show();
			} else {
				/*if (addresses == null || addresses.size() == 0)*/ 
				if(srcGeoPoint.getLatitudeE6()==0 && srcGeoPoint.getLongitudeE6()==0){
					googleMap.clear();
					String location = "Raja vihar";

					Toast.makeText(getBaseContext(), "No Location found",
							Toast.LENGTH_SHORT).show();
					i++;
					
				} else {
					// Clears all the existing markers on the map
					googleMap.clear();
					
					//googleMap.clear();

					// Adding Markers on Google Map for each matching address

					/*Log.e("address size", "" + addresses.size());
					for (int i = 0; i < addresses.size(); i++) {

						Address address = (Address) addresses.get(i);

*/						// Creating an instance of GeoPoint, to display in
						// Google Map
						latLng = new LatLng(srcGeoPoint.getLatitudeE6()/1e6,
								srcGeoPoint.getLongitudeE6()/1e6);

						/*String addressText = String.format(
								"%s, %s",
								address.getMaxAddressLineIndex() > 0 ? address
										.getAddressLine(0) : "", address
										.getCountryName());*/

						markerOptions = new MarkerOptions();
						markerOptions.position(latLng);
						markerOptions.title(address);
						//markerOptions.snippet(address);

						googleMap.addMarker(markerOptions);

						// Locate the first location
						googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 10.0f));
							/*googleMap.animateCamera(CameraUpdateFactory
									.newLatLng(latLng));*/
				
				}
			}
		}
	}
	
	public static JSONObject getLocationInfo(String address) {

		HttpGet httpGet = new HttpGet("http://maps.google.com/maps/api/geocode/json?address=" +address+"&ka&sensor=false");
		HttpClient client = new DefaultHttpClient();
		HttpResponse response;
		StringBuilder stringBuilder = new StringBuilder();

		try {
			response = client.execute(httpGet);
			HttpEntity entity = response.getEntity();
			InputStream stream = entity.getContent();
			int b;
			while ((b = stream.read()) != -1) {
				stringBuilder.append((char) b);
			}
		} catch (ClientProtocolException e) {
		} catch (IOException e) {
		}

		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject = new JSONObject(stringBuilder.toString());
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return jsonObject;
	}
	
	public static GeoPoint getGeoPoint(JSONObject jsonObject) {

		Double lon = new Double(0);
		Double lat = new Double(0);

		try {

			lon = ((JSONArray)jsonObject.get("results")).getJSONObject(0)
				.getJSONObject("geometry").getJSONObject("location")
				.getDouble("lng");

			lat = ((JSONArray)jsonObject.get("results")).getJSONObject(0)
				.getJSONObject("geometry").getJSONObject("location")
				.getDouble("lat");

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return new GeoPoint((int) (lat * 1E6), (int) (lon * 1E6));

	}

	
	
}
