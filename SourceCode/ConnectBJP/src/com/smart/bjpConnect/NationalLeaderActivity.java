package com.smart.bjpConnect;

import java.util.ArrayList;

import android.os.Bundle;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.Html;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class NationalLeaderActivity extends ActinBarActivity {

	ListView mHotels;
	ArrayList<String> leader_details;
	ArrayList<Bitmap> leader_pics;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_leaders_state);

		setupActionBar();
		TextView title = (TextView) actionBarLayout.findViewById(R.id.txt_title);
		title.setText("NATIONAL LEADERS");

		// Get the view from fragmenttab1.xml
		//View view = inflater.inflate(R.layout.activity_leaders_state, container, false);
		mHotels = (ListView) findViewById(R.id.list_leaders_state);

		leader_details= new ArrayList<String>();
		leader_details.add("<p><h4>Shri Atal Bihari Vajpayee</h4></p><p>Was Prime Minister of India from May 16-31, 1996 and from 1998 - 2004.</p>");
		leader_details.add("<p><h4>Shri L K Advani</h4></p><p>The current Chairman of the BJP Parliamentary Party.</p>");
		leader_details.add("<p><h4>Shri Rajnath Singh</h4></p><p>President(Bharatiya Janata Party - National).</p>");
		leader_details.add("<p><h4>Sushma Swaraj</h4></p><p>The leader of opposition in the Lok Sabha</p>");
		leader_details.add("<p><h4>Arun Jaitley</h4></p><p>Indian politician & Leader of Opposition in the Rajya Sabha.</p>");
		leader_details.add("<p><h4>Shri Narendra Modi</h4></p><p>The 14th and current Chief Minister of Gujarat.</p>");
		leader_details.add("<p><h4>Shri Nitin Gadkari</h4></p><p>Former BJP President & Delhi Election-Incharge.</p>");

		leader_pics= new ArrayList<Bitmap>();
		
		Bitmap icon2 = BitmapFactory.decodeResource(NationalLeaderActivity.this.getResources(),
				R.drawable.vajpaie);
		leader_pics.add(icon2);
		
		Bitmap icon3 = BitmapFactory.decodeResource(NationalLeaderActivity.this.getResources(),
				R.drawable.advani);
		leader_pics.add(icon3);
		
		Bitmap icon4 = BitmapFactory.decodeResource(NationalLeaderActivity.this.getResources(),
				R.drawable.rajnath);
		leader_pics.add(icon4);

		Bitmap icon5 = BitmapFactory.decodeResource(NationalLeaderActivity.this.getResources(),
				R.drawable.suhma);
		leader_pics.add(icon5);

		Bitmap icon6 = BitmapFactory.decodeResource(NationalLeaderActivity.this.getResources(),
				R.drawable.jetli);
		leader_pics.add(icon6);
		
		Bitmap icon7 = BitmapFactory.decodeResource(NationalLeaderActivity.this.getResources(),
				R.drawable.modi);
		leader_pics.add(icon7);

		Bitmap icon1 = BitmapFactory.decodeResource(NationalLeaderActivity.this.getResources(),
				R.drawable.gadkari);
		leader_pics.add(icon1);

		//Context ctx= getActivity();
		NationalAdapter adapter= new NationalAdapter(NationalLeaderActivity.this, leader_details, leader_pics);
		mHotels.setAdapter(adapter);

	}

	// Adapter to populate the listview 
	public class NationalAdapter extends ArrayAdapter<String> {

		/**
		 * Arraylist that contains the notifications data
		 */
		private ArrayList<String> Mdetails;
		/**
		 * Arraylist of bitmaps that contains all images of notification list
		 */
		public ArrayList<Bitmap> mImagevalues;
		/**
		 * Device display metrics
		 */
		DisplayMetrics mDisplaymetrics;

		/**
		 * Application context
		 */
		private final Context context;
		public NationalAdapter(Context context, ArrayList<String> details,
				ArrayList<Bitmap> imageArray) {
			super(context, R.layout.stream_list_row, details);
			this.context = context;

			Mdetails= new ArrayList<String>();
			Mdetails.clear();
			for (int i = 0; i < details.size(); i++) {
				Mdetails.add(details.get(i));
			}
			mImagevalues = new ArrayList<Bitmap>();
			mImagevalues.clear();
			for (int i = 0; i < imageArray.size(); i++) {
				mImagevalues.add(imageArray.get(i));
			}
			mDisplaymetrics = new DisplayMetrics();

		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {

			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			if (convertView == null) {
				// inflate the notification view list
				convertView = inflater.inflate(R.layout.stream_list_row,
						parent, false);
			}

			Log.e("POsition--"+position,"KKK" +Mdetails.get(position));

			TextView textView1 = (TextView) convertView
					.findViewById(R.id.txt_leader_txt);
			ImageView imageView = (ImageView) convertView
					.findViewById(R.id.img_leader);
			//imageView.setLayoutParams(new RelativeLayout.LayoutParams(70, 70));

			textView1.setText(Html.fromHtml(Mdetails.get(position)));

			Bitmap candidate_image= Utils.getCroppedBitmap(mImagevalues.get(position));
			imageView.setImageBitmap(candidate_image);


			return convertView;
		}
	}

	/*@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.national_leader, menu);
		return true;
	}*/

}
