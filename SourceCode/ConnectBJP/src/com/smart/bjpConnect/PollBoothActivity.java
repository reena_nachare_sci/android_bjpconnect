package com.smart.bjpConnect;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.AsyncTask;
import android.os.Bundle;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;
import android.view.View.OnClickListener;

public class PollBoothActivity extends ActinBarActivity {

	private Spinner mDistrictSpinner;
	private Spinner mACSpinner;
	private Spinner mPollBoothSpinner;

	private Button mClickHereButton;

	private ProgressDialog dlgProgress;
	private Context mContext;

	ConnectionDetector cd;

	private ArrayList<String> mACArrayList;
	private ArrayList<String> mPollBoothArrayList;
	String address;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_poll_booth);

		setupActionBar();
		TextView title = (TextView) actionBarLayout.findViewById(R.id.txt_title);
		title.setText("POLL BOOTH INFO");

		mContext = this;

		cd= new ConnectionDetector(PollBoothActivity.this);

		mDistrictSpinner = (Spinner) findViewById(R.id.district_spinner);
		mACSpinner = (Spinner) findViewById(R.id.ac_spinner);
		mPollBoothSpinner = (Spinner) findViewById(R.id.pollingStations_spinner);

		mACArrayList = new ArrayList<String>();
		mPollBoothArrayList = new ArrayList<String>();

		mClickHereButton = (Button) findViewById(R.id.buttonClickHere);

		mDistrictSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {

				if(!mDistrictSpinner.getSelectedItem().toString().equals("--Select--")){
					if(cd.isConnectingToInternet()){
						DownloadAC DT = new DownloadAC();
						DT.execute();
					}else{
						Toast.makeText(PollBoothActivity.this, "NO Internet", Toast.LENGTH_SHORT).show();	
						//finish();
					}
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {

			}
		});

		mACSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				if(!mACSpinner.getSelectedItem().toString().equals("--Select--")){
					if(cd.isConnectingToInternet()){
						DownloadPollBooth pollBooth = new DownloadPollBooth();
						pollBooth.execute();
					}else{
						Toast.makeText(PollBoothActivity.this, "NO Internet", Toast.LENGTH_SHORT).show();
					}
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {

			}
		});

		mClickHereButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				if(!mACSpinner.getSelectedItem().toString().equals("--Select--") &&
						!mDistrictSpinner.getSelectedItem().toString().equals("--Select--")&&
						!mPollBoothSpinner.getSelectedItem().toString().equals("--Select--")){
					DownloadPollBoothAddress pollBoothAdd = new DownloadPollBoothAddress();
					pollBoothAdd.execute();
				}else{
					Toast.makeText(mContext, "Please fill all fields", Toast.LENGTH_SHORT).show();
				}
			}
		});
	}

	public class DownloadAC extends AsyncTask<String, Integer, String>{
		boolean status=false;
		protected void onPreExecute() {
			Log.e("Pre","KK");
			dlgProgress = new ProgressDialog(PollBoothActivity.this);
			dlgProgress.setMessage("Please wait");
			dlgProgress.setCancelable(false);
			dlgProgress.show();
		}

		protected void onPostExecute(String result) {
			// Getting adapter of the listview
			Log.e("post","KK");
			dlgProgress.dismiss();

			/*ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext,
					android.R.layout.simple_list_item_1, android.R.id.text1, mACArrayList);*/

			SpinnerAdapter adapter = new ArrayAdapter<String>(mContext,
					android.R.layout.simple_list_item_1, android.R.id.text1, mACArrayList);

			mACSpinner.setAdapter(adapter);
		}
		@Override
		protected String doInBackground(String... aurl) {
			Log.e("background","KK");
			try {
				status = GetOptions();
				return null;

			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}
	}

	public boolean GetOptions() {
		// TODO Auto-generated method stub
		//Bitmap decodedByte = null;
		boolean isList=false;
		HttpClient httpclient = new DefaultHttpClient();
		HttpPost httppost = new HttpPost(
				"http://www.smartcloudinfo.com/BJPConnect/index.php/pollbooths/getACList_REST");
		try {
			// Add your data

			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
			//nameValuePairs.add(new BasicNameValuePair("state", "Delhi"));
			nameValuePairs.add(new BasicNameValuePair("district", mDistrictSpinner.getSelectedItem().toString().trim()));

			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

			// Execute HTTP Post Request
			HttpResponse response = httpclient.execute(httppost);

			HttpEntity entity = response.getEntity();

			InputStream inStream = entity.getContent();
			String jsonresponse = Utils.convertStreamToString(inStream);
			Log.e("result==voting qs", jsonresponse);
			JSONObject obj1 = null;

			try {
				obj1 = new JSONObject(jsonresponse);

			} catch (JSONException e) {

				e.printStackTrace();
			}
			try {
				String status = obj1.getString("status");
				if (status.equals("false")) {
					isList=false;
				} else {
					isList=true;
					JSONArray medias= obj1.getJSONArray("aclist");
					mACArrayList = new ArrayList<String>();
					for(int i=0;i<medias.length();i++)
					{
						String ac=medias.getString(i);
						//JSONObject item= medias.getJSONObject(i);
						//filesMapMedia.put(item.getString("title"), item.getString("image"));
						Log.e("ac", ac);
						mACArrayList.add(ac);
					}
				}

			} catch (JSONException e) {
				e.printStackTrace();
			}
		} catch (ClientProtocolException ex) {
			ex.printStackTrace();
		} catch (IOException ez) {
			ez.printStackTrace();
		}
		return isList;
	}

	public class DownloadPollBooth extends AsyncTask<String, Integer, String>{
		boolean status=false;
		protected void onPreExecute() {
			Log.e("Pre","KK");
			dlgProgress = new ProgressDialog(PollBoothActivity.this);
			dlgProgress.setMessage("Please wait");
			dlgProgress.setCancelable(false);
			dlgProgress.show();
		}

		protected void onPostExecute(String result) {
			// Getting adapter of the listview
			Log.e("post","KK");
			dlgProgress.dismiss();

			/*ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext,
					android.R.layout.simple_list_item_1, android.R.id.text1, mACArrayList);*/

			SpinnerAdapter adapter = new ArrayAdapter<String>(mContext,
					android.R.layout.simple_list_item_1, android.R.id.text1, mPollBoothArrayList);

			mPollBoothSpinner.setAdapter(adapter);
		}
		@Override
		protected String doInBackground(String... aurl) {
			Log.e("background","KK");
			try {
				status = GetPollBooths();
				return null;

			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}
	}

	public boolean GetPollBooths() {
		// TODO Auto-generated method stub
		//Bitmap decodedByte = null;
		boolean isList=false;
		HttpClient httpclient = new DefaultHttpClient();
		HttpPost httppost = new HttpPost(
				"http://www.smartcloudinfo.com/BJPConnect/index.php/pollbooths/getLocalities_REST");
		try {
			// Add your data

			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
			nameValuePairs.add(new BasicNameValuePair("ac", mACSpinner.getSelectedItem().toString().trim()));

			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

			// Execute HTTP Post Request
			HttpResponse response = httpclient.execute(httppost);

			HttpEntity entity = response.getEntity();

			InputStream inStream = entity.getContent();
			String jsonresponse = Utils.convertStreamToString(inStream);
			Log.e("result==voting qs", jsonresponse);
			JSONObject obj1 = null;

			try {
				obj1 = new JSONObject(jsonresponse);

			} catch (JSONException e) {

				e.printStackTrace();
			}
			try {
				String status = obj1.getString("status");
				if (status.equals("false")) {
					isList=false;
				} else {
					isList=true;
					JSONArray medias= obj1.getJSONArray("localities");
					mPollBoothArrayList = new ArrayList<String>();
					for(int i=0;i<medias.length();i++)
					{
						String ac=medias.getString(i);
						//JSONObject item= medias.getJSONObject(i);
						//filesMapMedia.put(item.getString("title"), item.getString("image"));
						mPollBoothArrayList.add(ac);
					}
				}

			} catch (JSONException e) {
				e.printStackTrace();
			}
		} catch (ClientProtocolException ex) {
			ex.printStackTrace();
		} catch (IOException ez) {
			ez.printStackTrace();
		}
		return isList;
	}

	public class DownloadPollBoothAddress extends AsyncTask<String, Integer, String>{
		boolean status=false;
		protected void onPreExecute() {
			Log.e("Pre","KK");
			dlgProgress = new ProgressDialog(PollBoothActivity.this);
			dlgProgress.setMessage("Please wait");
			dlgProgress.setCancelable(false);
			dlgProgress.show();
		}

		protected void onPostExecute(String result) {
			// Getting adapter of the listview
			Log.e("post","KK");
			dlgProgress.dismiss();

			/*ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext,
					android.R.layout.simple_list_item_1, android.R.id.text1, mACArrayList);*/

			Intent toMap= new Intent(PollBoothActivity.this, PollingBoothInfo.class);
			toMap.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			toMap.putExtra("address", address);
			startActivity(toMap);
			//Toast.makeText(getApplicationContext(), "Saved", Toast.LENGTH_SHORT).show();
		}
		@Override
		protected String doInBackground(String... aurl) {
			Log.e("background","KK");
			try {
				status = GetPollBoothsAddress();
				return null;

			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}
	}

	public boolean GetPollBoothsAddress() {
		// TODO Auto-generated method stub
		//Bitmap decodedByte = null;
		boolean isList=false;
		HttpClient httpclient = new DefaultHttpClient();
		HttpPost httppost = new HttpPost(
				"http://www.smartcloudinfo.com/BJPConnect/index.php/pollbooths/getPollbooths_REST");
		try {
			// Add your data

			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
			nameValuePairs.add(new BasicNameValuePair("locality", mPollBoothSpinner.getSelectedItem().toString().trim()));

			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

			// Execute HTTP Post Request
			HttpResponse response = httpclient.execute(httppost);

			HttpEntity entity = response.getEntity();

			InputStream inStream = entity.getContent();
			String jsonresponse = Utils.convertStreamToString(inStream);
			Log.e("result==voting qs", jsonresponse);
			JSONObject obj1 = null;

			try {
				obj1 = new JSONObject(jsonresponse);

			} catch (JSONException e) {

				e.printStackTrace();
			}
			try {
				String status = obj1.getString("status");
				if (status.equals("false")) {
					isList=false;
				} else {
					isList=true;
					JSONArray medias= obj1.getJSONArray("pollbooths");
					for(int i=0;i<medias.length();i++)
					{
						address=medias.getString(i);
						//String ac=medias.getString(i);
						//JSONObject item= medias.getJSONObject(i);
						//filesMapMedia.put(item.getString("title"), item.getString("image"));
						//mPollBoothArrayList.add(ac);
					}
				}

			} catch (JSONException e) {
				e.printStackTrace();
			}
		} catch (ClientProtocolException ex) {
			ex.printStackTrace();
		} catch (IOException ez) {
			ez.printStackTrace();
		}
		return isList;
	}
}