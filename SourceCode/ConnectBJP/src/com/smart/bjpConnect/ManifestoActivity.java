package com.smart.bjpConnect;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;


import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.widget.TextView;

public class ManifestoActivity extends ActinBarActivity{

	private TextView txt_manifesto;
	private String manifesto_desc="";
	private String manifesto_title="";
	private ConnectionDetector cd;
	AlertDialogManager alert = new AlertDialogManager();
	private ProgressDialog dlgProgress;

	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		Connect.isManifesto=true;
		setContentView(R.layout.activity_issue_detail);
		setupActionBar();
		dlgProgress= new ProgressDialog(ManifestoActivity.this);
		TextView title = (TextView) actionBarLayout.findViewById(R.id.txt_title);
		title.setText("BJP VISION");
		//setupMenu();
		cd=new ConnectionDetector(getApplicationContext());
		getManifesto();
		//displayManifesto();
	}

	private void getManifesto() {
		// TODO Auto-generated method stub
		if(cd.isConnectingToInternet()){
			GetManifesto getManifest= new GetManifesto();
			getManifest.execute();
		}
		else{
			alert.showAlertDialog(
					ManifestoActivity.this,
					"Internet Connection Error",
					"Please connect to working Internet connection for loading Manifesto!",
					false);
			finish();
		}
	}

	private void displayManifesto() {
		// TODO Auto-generated method stub

		txt_manifesto=(TextView)findViewById(R.id.txt_issue_desc);

		String text_manifesto = "<html><head>"

	              + "<body style=\"text-align: center\">"
	              +"<p><h1>"
	              + manifesto_title
	              +"</h1></p>"
	              +"<p><h3>"
	              + ""
	              +"</h></p>"
	              +"<p><h2>"
	              + manifesto_desc
	              +"</h2></p>"
	              + "</body>"
	              + "</head></html>";
		txt_manifesto.setText(Html.fromHtml(text_manifesto));

	}

	/** AsyncTask to download and load an image in ListView */
	private class GetManifesto extends AsyncTask<String, Void, String> {
		@Override
		protected void onPostExecute(String result) {
			// Getting adapter of the listview
			dlgProgress.dismiss();
			displayManifesto();
		}

		@Override
		protected String doInBackground(String... params) {

			try {
				getManifestoData();
				return null;

			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPreExecute() {
			dlgProgress.setMessage("Please wait");
			dlgProgress.setCancelable(false);
			dlgProgress.show();
		}

	}

	public void getManifestoData() {
		// TODO Auto-generated method stub
		//Bitmap decodedByte = null;
		HttpClient httpclient = new DefaultHttpClient();
		HttpPost httppost = new HttpPost(
				"http://www.smartcloudinfo.com/BJPConnect/index.php/staticcontents/getManifestoContent_REST");

		try {
			// Add your data

			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);


			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

			// Execute HTTP Post Request
			HttpResponse response = httpclient.execute(httppost);

			HttpEntity entity = response.getEntity();

			InputStream inStream = entity.getContent();
			String jsonresponse = Utils.convertStreamToString(inStream);
			Log.e("result==manifesto", jsonresponse);
			JSONObject obj1 = null;

			try {
				obj1 = new JSONObject(jsonresponse);

			} catch (JSONException e) {

				e.printStackTrace();
			}
			try {
				String status = obj1.getString("status");
				if (status.equals("false")) {

				} else {
					manifesto_title = obj1.getString("title");
					// decode the byte array
					manifesto_desc= obj1.getString("manifesto");
				}

			} catch (JSONException e) {

			}
		} catch (ClientProtocolException ex) {

		} catch (IOException ez) {

		}

	}

	public void onBackPressed() {
		super.onBackPressed();
		Connect.isManifesto=false;
	}
}
