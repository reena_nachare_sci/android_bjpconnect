package com.smart.bjpConnect;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.Gravity;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.actionbarsherlock.app.ActionBar.Tab;



public class AboutDetailActivity extends ActinBarActivity{
	//ActionBar mActionBar;
	//ViewGroup actionBarLayout,menulay;
	WebView mWebView;
	TextView txt_about;
	private String from;
	ViewPager mPager;
	Tab tab;
	ScrollView scroll_txt;
	ImageView img_about;
	WebView webview;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_about);
		//setupActionBar();
		/*TextView title = (TextView) actionBarLayout.findViewById(R.id.txt_title);
		title.setText("ABOUT BJP");
		setupMenu();*/
		setupActionBar();
		txt_about= (TextView)findViewById(R.id.txt_about);
		txt_about.setMovementMethod(LinkMovementMethod.getInstance());
		scroll_txt=(ScrollView)findViewById(R.id.about_scroll);
		img_about= (ImageView)findViewById(R.id.img_about);
		webview=(WebView) findViewById(R.id.webview);
		mPager = (ViewPager) findViewById(R.id.pager);

		if(getIntent().getExtras()!=null)
		{
			from= getIntent().getExtras().getString("display");

		}

		if(from.equals("bjp_india"))
		{
			/*setTitleBar("BJP INDIA");
			//scroll_txt.setVisibility(View.VISIBLE);
			//mPager.setVisibility(View.GONE);
			//displayPhilosophy();
			Intent toDisplayActivity= new Intent(AboutDetailActivity.this, DisplaySites.class);
			toDisplayActivity.putExtra("display", "bjp_india");
			startActivity(toDisplayActivity);*/

		}
		else if(from.equals("history"))
		{
			setTitleBar("HISTORY");
			scroll_txt.setVisibility(View.VISIBLE);
			mPager.setVisibility(View.GONE);
			displayHistory();
		}
		else if(from.equals("bjp_delhi"))
		{
			setTitleBar("FAQ's");
			scroll_txt.setVisibility(View.VISIBLE);
			mPager.setVisibility(View.GONE);
			
			displayConstituent();

		}else if(from.equals("faq"))
		{
			setTitleBar("BJP in Delhi");
			scroll_txt.setVisibility(View.VISIBLE);
			mPager.setVisibility(View.GONE);
			/*setupActionBarNoMenu();
			img_about.setVisibility(View.GONE);*/
			displayFAQ();

		}
		/*else if(from.equals("National_leadership"))
		{
			scroll_txt.setVisibility(View.GONE);
			mPager.setVisibility(View.VISIBLE);
			setActionBar();
			//displayLeaders();
		}*/
		//	Initialize();

	}

	

	private void setTitleBar(String title_name) {
		// TODO Auto-generated method stub
		/*actionBarLayout = (ViewGroup) getLayoutInflater().inflate(
				R.layout.action_bar,
				null);
		mActionBar= getSupportActionBar();
		mActionBar.setDisplayOptions(
				ActionBar.DISPLAY_SHOW_CUSTOM 
				);
		mActionBar.setCustomView(actionBarLayout);
*/
		TextView title = (TextView) actionBarLayout.findViewById(R.id.txt_title);
		title.setText(title_name);
	}


	/*private void displayLeaders() {
		// TODO Auto-generated method stub
		setTabs();
	}
*/
	/*private void setTabs() {
		// TODO Auto-generated method stub
		// Locate ViewPager in activity_main.xml



		// Activate Fragment Manager
		FragmentManager fm = getSupportFragmentManager();

		// Capture ViewPager page swipes
		ViewPager.SimpleOnPageChangeListener ViewPagerListener = new ViewPager.SimpleOnPageChangeListener() {
			@Override
			public void onPageSelected(int position) {
				super.onPageSelected(position);
				// Find the ViewPager Position
				mActionBar.setSelectedNavigationItem(position);
			}
		};

		mPager.setOnPageChangeListener(ViewPagerListener);
		// Locate the adapter class called ViewPagerAdapter.java
		//ViewPagerAdapter viewpageradapter = new ViewPagerAdapter(fm);
		// Set the View Pager Adapter into ViewPager
		//mPager.setAdapter(viewpageradapter);

		// Capture tab button clicks
		ActionBar.TabListener tabListener = new ActionBar.TabListener() {

			@Override
			public void onTabSelected(Tab tab, FragmentTransaction ft) {
				// Pass the position on tab click to ViewPager
				mPager.setCurrentItem(tab.getPosition());

			}

			@Override
			public void onTabUnselected(Tab tab, FragmentTransaction ft) {
				// TODO Auto-generated method stub
			}

			@Override
			public void onTabReselected(Tab tab, FragmentTransaction ft) {
				// TODO Auto-generated method stub
			}


		};

		// Create first Tab
		tab = mActionBar.newTab().setText("State").setTabListener(tabListener);
		mActionBar.addTab(tab);

		// Create second Tab
		tab = mActionBar.newTab().setText("National").setTabListener(tabListener);
		mActionBar.addTab(tab);
		if(from.equals("State_leadership"))
		{
			mPager.setCurrentItem(0);
		}
		else{
			mPager.setCurrentItem(1);
		}

	}
*/

	private void displayConstituent() {
		// TODO Auto-generated method stub
		String textfir = "<html><head>"

	              + "<body style=\"text-align: center\">"
	              +"<p><h3>"
	              + ""
	              +"</h3></p>"

	              

	              +"<p><h3>"+ "What are the documents I need to register as a voter?"+"</h3></p>"

+"<p><h3>"
+ ""
+"</h3></p>"
					+"<p><h3>"
					+ "Answer :-"
					+"</h3></p>"

	              +"<p>"+ "Apart from your voter registration form, you will need to submit:"+"</p>"
	              +"<p>"+ "� An Address Proof document. This document is required for the BLO (Booth Level Officer) to come and verify your residence status. It is therefore not essential for the address proof document to have your name. It needs to have only the address of the place where you are staying."+"</p>"
	              +"<p>"+ "� If you are a first time voter, you will also need to submit an age proof document. You can submit the proof of Date of Birth from any government agency authorized document like birth certificate, passport, PAN Card, Driving Licence, state board exam certificate etc.� "+"</p>"
	              +"<p>"+ "� If you are a student residing in a hostel / mess, you might not have any of the above mentioned address proof documents. In this case, you can get a Student Declaration Form signed by your college dean/principal/registrar and submit it along with your voter registration form."+"</p>"

					+"<p><h3>"
					+ ""
					+"</h3></p>"


+"<p><h3>"+ "I don't have an address proof document on my name. Can I still register to vote?"+"</h3></p>"
+"<p><h3>"
+ ""
+"</h3></p>"
	

	+"<p><h3>"
	+ "Answer :-"
	+"</h3></p>"

+"<p>"+ "Yes, you can still register to vote."+"</p>"

	+"<p><h3>"
	+ ""
	+"</h3></p>"


+"<p><h3>"+ "I submitted my registration form but was denied the receipt of application. Where should I complain?"+"</h3></p>"
+"<p><h3>"
+ ""
+"</h3></p>"
	
	+"<p><h3>"
	+ "Answer :-"
	+"</h3></p>"

+"<p>"+ "If you don't get a satisfactory response from your ERO, you should complain with your state Chief Electoral Officer (CEO). You can find the contact details of your state CEO at the state CEO's website. If you are still not satisfied, you can directly complain to the Election Commission of India (ECI)."+"</p>"

	+"<p><h3>"
	+ ""
	+"</h3></p>"


+"<p><h3>"+ "There is an upcoming election where I want to vote? What is the last date for registration?"+"</h3></p>"

	+"<p><h3>"
	+ "Answer :-"
	+"</h3></p>"

+"<p>"+ "If there is an upcoming election in your state, registration stops normally 10 days before the final date of accepting candidate nominations. Please check your local newspapers or the website of your state Chief Electoral Officer (CEO) for the exact final date for accepting voter registration forms."+"</p>"

	+"<p><h3>"
	+ ""
	+"</h3></p>"


	              + "</body>"
	              + "</head></html>";
		txt_about.setText(Html.fromHtml(textfir));
	}

	private void displayFAQ() {
		// TODO Auto-generated method stub
		String textfir = "<html><head>"
	              + "<body style=\"text-align: justify\">"
	              +"<p><li>"+ "Brave, bold men and women, these are what we want. What we want is vigor in the blood, strength in the nerves, iron muscles and nerves of steel, not softening namby-pamby ideas."+"</li></p>"
	              +"<hr>"
	              +"<p><li>"+ "The first and the foremost thing is that any government in Delhi must be capable enough to share and connect with the aspirations and dreams of the citizens of Delhi.  Delhi needs to have innovative solutions  for the problems which are being projected insurmountable. No problem is insurmountable, if we have the intent to resolve it. Only a weak mind would  take up lame excuses and shift the blame."+"</li></p>"
	              +"<hr>"
	              +"<p><li>"+ "The party aims at establishing a democratic state which guarantees to all citizens irrespective of caste, creed or sex, political, social and economic justice, equality of opportunity and liberty of faith and expression."+"</li></p>"
	              +"<hr>"
	              +"<p><li>"+ "BJP�s vision for Delhi is that it should be a city where people can feel safe, dignity of  women is taken care of, children and senior citizens don�t feel vulnerable and all sections of the society get equal opportunities."+"</li></p>"
	              +"<hr>"
	              +"<p><li>"+ "A model city that is environment friendly and built keeping the disabled in mind. "+"</li></p>"
	              +"<hr>"
	              +"<p><li>"+ "BJP will play an important role in setting up, nurturing, supporting and promoting world class art and cultural organizations as well as supporting skill development, and support ecosystems for young entrepreneurs."+"</li></p>"
	              +"<hr>"
	              +"<p><li>"+ "Be not afraid, for all great power throughout the history of humanity has been with the people. From out of their ranks have come all the greatest geniuses of the world, and history can only repeat itself. Be not afraid of anything. You will do marvelous work."+"</li></p>"
	              + "</body>"
	              + "</head></html>";
		webview.setVisibility(View.VISIBLE);
		txt_about.setVisibility(View.INVISIBLE);
		webview.loadDataWithBaseURL(null, textfir, null, "utf-8", null);
		/*txt_about.setText(Html.fromHtml(textfir));
		txt_about.setGravity(Gravity.CENTER_HORIZONTAL);*/
	}

	private void displayHistory() {
		// TODO Auto-generated method stub
		String textfir = "<html><head>"

	              + "<body style=\"text-align: center\">"
	              +"<p><h3>"
	              + "<b>\"HISTORY\"</b>"
	              +"</h3></p>"
	              +"<p><h3>"
	              + ""
	              +"</h3></p>"
	              +"<p><h3>"
	              +" Founder :: Syama Prasad Mookerjee (1901-1953) " +"</h3></p>"
	              +"<p>"+ "Founder of the Bharatiya Jana Sangh"+"</p>"
	              +"<p>"+ "The BJP is the successor party of the BJS, which merged itself into the Janata Party in 1977. The BJP was formed as a separate party in 1980 after internal differences in the Janata Party resulted in the collapse of it's government in 1979."+"</p>"
	              +"<p>"+"<a href=\"http://www.bjpdelhi.org/beta/bjp-founder.php\">"+"Read More</a>"
	              + "</body>"
	              + "</head></html>";
		txt_about.setText(Html.fromHtml(textfir));
	}

	/*private void displayPhilosophy() {
		// TODO Auto-generated method stub
		String textfir = "<html><head>"

	              + "<body style=\"text-align: center\">"
	    		  +"<p><h3>"
	              + "<b>\"PHILOSOPHY\"</b>"
	              +"</h3></p>"
	              +"<p><h3>"
	              + ""
	              +"</h3></p>"
	              +"<p><h3>"
	              +" BJP PHILOSOPHY :: Integral Humanism " +"</h3></p>"
	              +"<p>" + "Integral Humanism, the guiding philosophy of the Bharatiya Janata Party, was first presented by Pandit Deendayal Upadhyaya in the form of four lectures delivered in Bombay on April 22-25, 1965."+"</p>"
	              +"<p>"+ "We have to Undertake the task of awakening our nation's 'Virat'. Let us go forward in this task with a sense of pride for our heritage, with a realistic assessment of the present and a great ambition for the future. We wish neither to make this country a shadow of some distant past nor an imitation of Russia or America. "+"</p>"
	              +"<p>"+ "With the support of Universal knowledge and our heritage, we shall create a Bharat which will excel all its past glories, and will enable every citizen in its fold to steadily progress in the development of his manifold latent possibilities and to achieve through a sense of unity with the entire creation, a state even higher than that of a complete human being; to become Narayan from 'Nar'. This is the external divine from of our culture. This is our message to humanity to cross roads. May God give us strength to succeed in this task."+"</p>"
	              +"<p>"+ "! BHARAT MATA KI JAI !"+"</p>"
	              +"<p>"+"<a href=\"http://www.bjpdelhi.org/beta/delhi_bjp_philosph.php\">"+"Read More</a>"
	              + "</body>"
	              + "</head></html>";
		 txt_about.setText(Html.fromHtml(textfir));
	}*/




	/*
	 * Method to handle Back press click of phone
	 */
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		/*
		Intent toAdd= new Intent(AboutDetailActivity.this,AboutBjpActivity.class);
		startActivity(toAdd);
		finish();*/
	}
}
