package com.smart.bjpConnect;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import com.google.android.gcm.GCMRegistrar;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Vibrator;
import android.util.Log;
import android.widget.Toast;

public class Utils {

	/**
	 * Google API project id registered to use GCM.
	 */
	public static final String SENDER_ID = "632141680080";	

	private String mRegistrationId;   
	/**
	 * Intent used to display a message in the screen.
	 */
	public static final String DISPLAY_MESSAGE_ACTION = "com.google.android.gcm.demo.app.DISPLAY_MESSAGE";

	/**
	 * Intent's extra that contains the message to be displayed.
	 */
	public static final String EXTRA_MESSAGE = "message";

	AlertDialogManager alert = new AlertDialogManager();
	public static final String Client_ID = "632141680080.apps.googleusercontent.com";//The client ID 
	/**
	 * Start Async task to register user for GCM and notifications. We start
	 * this task after user login to profile.
	 * */
	private AsyncTask<Void, Void, Void> mRegisterTask;
	private static final String ACTION = "android.net.conn.CONNECTIVITY_CHANGE";
	/**
	 * Base URL of the Linode Server
	 */
	public static final String SERVER_URL = "http://www.smartcloudinfo.com/BJPConnect/index.php/notifications/saveDeviceId_REST";	//get this from Rohit

	/**
	 * Application shared preferences name
	 */
	private static final String PREFS_NAME = "BjpConnectPrefs";
	/**
	 * Object to access the application shared preferences
	 */
	SharedPreferences mSp;
	SharedPreferences.Editor mSpEditor;
	
	
	/*
	   * convert server response
	   */
	public static String convertStreamToString(InputStream is) {
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		StringBuilder sb = new StringBuilder();

		String line = null;
		try {
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				is.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return sb.toString();
	}
	
	public static void displayRecievedMessage(Context context, String message) {
		Log.e("Utils", "display Message");
		Intent intent = new Intent(DISPLAY_MESSAGE_ACTION);
		intent.putExtra(EXTRA_MESSAGE, message);
		intent.putExtra("IsOtherMessage", "true");
		context.sendBroadcast(intent);
		Connect.isNotification=true;
		
	}
	
	 /* @Method : method will take the arg input string and will convert to
	 *         string
	 * @param ists
	 *            - input stream
	 * @return String value
	 * @Exception IOException
	 */
	public static String convertinputStreamToString(InputStream ists)
			throws IOException {
		if (ists != null) {
			StringBuilder sb = new StringBuilder();
			String line;

			try {
				BufferedReader r1 = new BufferedReader(new InputStreamReader(
						ists, "UTF-8"));
				while ((line = r1.readLine()) != null) {
					sb.append(line);
				}
			} finally {
				ists.close();
			}
			return sb.toString();
		} else {
			return "";
		}
	}
	
	/**
	  * Get circular cropped
	  * @param bitmap
	  * @return
	  */
	 public static Bitmap getCroppedBitmap(Bitmap bitmap) {
		 Log.e("Received bitmap--"+bitmap,"KKK");
		 System.gc();
		    Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
		            bitmap.getHeight(), Config.ARGB_8888);
		    Canvas canvas = new Canvas(output);

		    final int color = 0xff424242;
		    final Paint paint = new Paint();
		    final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());

		    paint.setAntiAlias(true);
		    canvas.drawARGB(0, 0, 0, 0);
		    paint.setColor(color);
		    // canvas.drawRoundRect(rectF, roundPx, roundPx, paint);
		    canvas.drawCircle(bitmap.getWidth() / 2, bitmap.getHeight() / 2,
		            bitmap.getWidth() / 2, paint);
		    paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
		    canvas.drawBitmap(bitmap, rect, rect, paint);
		    //Bitmap _bmp = Bitmap.createScaledBitmap(output, 60, 60, false);
		    //return _bmp;
		    return output;
		}
	 
	 public void GCMRegistration(final Context mCtx,final String userId) {
		 mSp = mCtx.getSharedPreferences(PREFS_NAME, 0);
		// Check if device is already registered or not.
		checkNotNull(SERVER_URL, SERVER_URL);
		checkNotNull(SENDER_ID, SENDER_ID);

		// Make sure the device has the proper dependencies.
		GCMRegistrar.checkDevice(mCtx);

		// Make sure the manifest was properly set - comment out this line
		// while developing the app, then uncomment it when it's ready.
		GCMRegistrar.checkManifest(mCtx);

		IntentFilter filter = new IntentFilter(ACTION);
		mCtx.registerReceiver(mConnectivityCheckReceiver, filter);
		/*mCtx.registerReceiver(mHandleMessageReceiver, new IntentFilter(
				DISPLAY_MESSAGE_ACTION));*/
		

		mRegistrationId = GCMRegistrar.getRegistrationId(mCtx);
		Log.e("GOT GCM REG ID:---"+mRegistrationId,"KKKKKKKKK");
		mSpEditor= mSp.edit();
		 mSpEditor.putString("DeviceToken", mRegistrationId);
		 mSpEditor.commit();
		if (mRegistrationId.equals("")) {
			// Automatically registers application on startup.

			GCMRegistrar.register(mCtx, SENDER_ID);
		} else {
			// Device is already egistered on GCM, check server.
			if (GCMRegistrar.isRegisteredOnServer(mCtx)) {
				// Skips registration.

			} else {
				// Try to register again, but not in the UI thread.
				// It's also necessary to cancel the thread onDestroy(),
				// hence the use of AsyncTask instead of a raw thread.

			}
		}
		
		mRegisterTask = new AsyncTask<Void, Void, Void>() {

			@Override
			protected Void doInBackground(Void... params) {
				String mId=userId;
				boolean registered = ServerUtilities.register(mCtx, mId,
						mRegistrationId);
				// At this point all attempts to register with the app
				// server failed, so we need to unregister the device
				// from GCM - the app will try to register again when
				// it is restarted. Note that GCM will send an
				// unregistered callback upon completion, but
				// GCMIntentService.onUnregistered() will ignore it.
				if (!registered) {
					GCMRegistrar.unregister(mCtx);
				}
				return null;
			}

			@Override
			protected void onPostExecute(Void result) {
				mRegisterTask = null;
			}

		};
		mRegisterTask.execute(null, null, null);
	}

	private void checkNotNull(Object reference, String name) {
		if (reference == null) {
			throw new NullPointerException("Error");
		}
	}
	
	/**
	 * Related to GCM:</p>
	 * Notifies UI to display a message.
	 * </p>
	 * This method is defined in the common helper because it's used both by the
	 * UI and the background service.</p>
	 * 
	 * @param context
	 *            application's context.
	 * @param message
	 *            message to be displayed.
	 */
	public static void displayMessage(Context context, String message) {
		Log.e("Utils", "display Message");
		Intent intent = new Intent(DISPLAY_MESSAGE_ACTION);
		intent.putExtra(EXTRA_MESSAGE, message);
		intent.putExtra("IsOtherMessage", "false");
		context.sendBroadcast(intent);
		
	}

	/**
	 * Broadcast receiver to notify user about the incoming message with
	 * ringtone
	 */
	public final BroadcastReceiver mHandleMessageReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {

			String msg = intent.getExtras().getString("IsOtherMessage");

			if (msg.equals("true")) {
				try {
					Uri notification = RingtoneManager
							.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
					Ringtone r = RingtoneManager.getRingtone(
							context, notification);
					r.play();

					// Get instance of Vibrator from current Context
					Vibrator mVibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);

					// This code snippet will cause the phone to vibrate "SOS"
					// in Morse Code
					// In Morse Code, "s" = "dot-dot-dot", "o" =
					// "dash-dash-dash"
					// There are pauses to separate dots/dashes, letters, and
					// words
					// The following numbers represent millisecond lengths
					int dot = 200; // Length of a Morse Code "dot" in
					// milliseconds
					int dash = 500; // Length of a Morse Code "dash" in
					// milliseconds
					int short_gap = 200; // Length of Gap Between dots/dashes
					int medium_gap = 500; // Length of Gap Between Letters
					int long_gap = 1000; // Length of Gap Between Words
					long[] pattern = { 0, // Start immediately
							dot, short_gap, dot, short_gap, dot, // s
							medium_gap, dash, short_gap, dash, short_gap, dash, // o
							medium_gap, dot, short_gap, dot, short_gap, dot, // s
							long_gap };
					// Only perform this pattern one time (-1 means
					// "do not repeat")
					mVibrator.vibrate(pattern, -1);

				} catch (Exception e) {
				}
			}
		}
	};

	public final BroadcastReceiver mConnectivityCheckReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {

			boolean noConnectivity = intent.getBooleanExtra(
					ConnectivityManager.EXTRA_NO_CONNECTIVITY, false);

			if (noConnectivity) {
				Toast.makeText(context,
						"No internet connection", Toast.LENGTH_LONG).show();
			} else {

			}
		}
	};

	public void Unregister(Context mCtx) {
		// TODO Auto-generated method stub
		if (mRegisterTask != null) {
			mRegisterTask.cancel(true);
		}
		//mCtx.unregisterReceiver(mHandleMessageReceiver);
		/*
		 * UnregisterAsynch urd = new UnregisterAsynch();
		 * 
		 * urd.execute();
		 */
		mCtx.unregisterReceiver(mConnectivityCheckReceiver);
		GCMRegistrar.onDestroy(mCtx);
	}
	 
	
	
}
