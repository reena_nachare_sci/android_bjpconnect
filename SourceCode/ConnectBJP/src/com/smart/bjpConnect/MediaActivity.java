package com.smart.bjpConnect;

import android.os.Bundle;
import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.view.View.OnClickListener;

public class MediaActivity extends ActinBarActivity {
	
	private Button mArticlesButton;
	private Button mPressMentionsButton;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_media);
		
		setupActionBar();
		TextView title = (TextView) actionBarLayout.findViewById(R.id.txt_title);
		title.setText("MEDIA AND PRESS");
		
		mArticlesButton = (Button) findViewById(R.id.articles);
		mPressMentionsButton = (Button) findViewById(R.id.mediaAndPress);
		
		mArticlesButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				//Intent toBJPVision= new Intent(MediaActivity.this, DisplaySites.class);
				//toBJPVision.putExtra("display", "articles");
				//toBJPVision.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				//startActivity(toBJPVision);
				Intent media= new Intent(MediaActivity.this,ArticleActivity.class);
				media.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);   
				startActivity(media);
			}
		});
		
		mPressMentionsButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent media= new Intent(MediaActivity.this,MediaPressActivity.class);
				media.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);   
				startActivity(media);
			}
		});
	}
}
