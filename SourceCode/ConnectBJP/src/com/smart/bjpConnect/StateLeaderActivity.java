package com.smart.bjpConnect;

import java.util.ArrayList;

import android.os.Bundle;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.Html;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class StateLeaderActivity extends ActinBarActivity {

	ListView mHotels;
	ArrayList<String> leader_names;
	ArrayList<String> leader_details;
	ArrayList<Bitmap> leader_pics;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_leaders_state);
		setupActionBar();
		TextView title = (TextView) actionBarLayout.findViewById(R.id.txt_title);
		title.setText("STATE LEADERS");
		// Get the view from fragmenttab1.xml
		//View view = inflater.inflate(R.layout.activity_leaders_state, container, false);
		mHotels = (ListView) findViewById(R.id.list_leaders_state);

		leader_names= new ArrayList<String>();
		leader_details= new ArrayList<String>();
		leader_pics= new ArrayList<Bitmap>();

		leader_names.add("Shri Vijay Goel");
		leader_names.add("Shri Harsh Vardhan");
		leader_names.add("Shri Vijay Kumar Malhotra");
		leader_names.add("Smt Arti Mehra");
		leader_names.add("Shri Vijendra Gupta");


		leader_details.add("<p><h4>Shri Vijay Goel</h4></p><p>The current President of BJP,Delhi Pradesh.</p>");
		leader_details.add("<p><h4>Shri Vijay Kumar Malhotra</h4></p><p>The Leader of Opposition (Delhi Legislative Assembly).</p>");
		leader_details.add("<p><h4>Shri Harsh Vardhan</h4></p><p>Was the former President of BJP,Delhi Pradesh in 1993.</p>");
		leader_details.add("<p><h4>Smt Arti Mehra</h4></p><p>Was the mayor of Municipal Corporation of Delhi(MCD) from 2007 to 2009.</p>");
		leader_details.add("<p><h4>Shri Vijendra Gupta</h4></p><p>Was the President of BJP Delhi Pradesh from Feb 2010 to Feb 2013.</p>");

		Bitmap icon1 = BitmapFactory.decodeResource(StateLeaderActivity.this.getResources(),
				R.drawable.goel);
		leader_pics.add(icon1);

		Bitmap icon3 = BitmapFactory.decodeResource(StateLeaderActivity.this.getResources(),
				R.drawable.malhotra);
		leader_pics.add(icon3);

		Bitmap icon2 = BitmapFactory.decodeResource(StateLeaderActivity.this.getResources(),
				R.drawable.harshvrdhan);
		leader_pics.add(icon2);

		Bitmap icon4 = BitmapFactory.decodeResource(StateLeaderActivity.this.getResources(),
				R.drawable.artimehra);
		leader_pics.add(icon4);

		Bitmap icon5 = BitmapFactory.decodeResource(StateLeaderActivity.this.getResources(),
				R.drawable.gupta);
		leader_pics.add(icon5);

		//Context ctx= getActivity();
		StatesAdapter adapter= new StatesAdapter(StateLeaderActivity.this, leader_details, leader_pics);
		mHotels.setAdapter(adapter);
	}

	// Adapter to populate the listview 
	public class StatesAdapter extends ArrayAdapter<String> {

		/**
		 * Arraylist that contains the notifications data
		 */
		//private ArrayList<String> Mvalues;
		/**
		 * Arraylist that contains the notifications data
		 */
		private ArrayList<String> Mdetails;
		/**
		 * Arraylist of bitmaps that contains all images of notification list
		 */
		public ArrayList<Bitmap> mImagevalues;
		/**
		 * Device display metrics
		 */
		DisplayMetrics mDisplaymetrics;

		/**
		 * Application context
		 */
		private final Context context;
		public StatesAdapter(Context context, ArrayList<String> details,
				ArrayList<Bitmap> imageArray) {
			super(context, R.layout.stream_list_row, details);
			this.context = context;

			Mdetails= new ArrayList<String>();
			Mdetails.clear();
			for (int i = 0; i < details.size(); i++) {
				Mdetails.add(details.get(i));
			}
			mImagevalues = new ArrayList<Bitmap>();
			mImagevalues.clear();
			for (int i = 0; i < imageArray.size(); i++) {
				mImagevalues.add(imageArray.get(i));
			}
			mDisplaymetrics = new DisplayMetrics();

		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {

			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			if (convertView == null) {
				// inflate the notification view list
				convertView = inflater.inflate(R.layout.stream_list_row,
						parent, false);
			}

			Log.e("POsition--"+position,"KKK" +Mdetails.get(position));

			TextView textView1 = (TextView) convertView
					.findViewById(R.id.txt_leader_txt);
			ImageView imageView = (ImageView) convertView
					.findViewById(R.id.img_leader);
			//imageView.setLayoutParams(new RelativeLayout.LayoutParams(70, 70));

			textView1.setText(Html.fromHtml(Mdetails.get(position)));

			Bitmap candidate_image= Utils.getCroppedBitmap(mImagevalues.get(position));
			imageView.setImageBitmap(candidate_image);


			return convertView;
		}
	}

}
